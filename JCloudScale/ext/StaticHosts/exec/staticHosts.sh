#!/bin/bash
set -x 

if [ "$#" == '0' ]; then
        mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main"
        exit 0;
fi

if [ "$#" == '1' ]; then
        mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main $1"
        exit 0;
fi

if [ "$#" == '2' ]; then
        mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main $1 $2"
        exit 0;
fi

if [ "$#" == '3' ]; then
        mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main $1 $2 $3"
        exit 0;
fi

if [ "$#" == '4' ]; then
        mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main $1 $2 $3 $4"
        exit 0;
fi

if [ "$#" == '5' ]; then
        mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main $1 $2 $3 $4 $5"
        exit 0;
fi

echo "Too much parameters."
