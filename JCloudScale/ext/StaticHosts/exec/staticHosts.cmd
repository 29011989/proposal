@ECHO OFF
IF "%~1" == "" GOTO NOPARAM

IF "%~2" == "" GOTO ONEPARAM

IF "%~3" == "" GOTO TWOPARAM

IF "%~4" == "" GOTO THREEPARAM

IF "%~5" == "" GOTO FOURPARAM

IF "%~6" == "" GOTO FIVEPARAM

:NOPARAM
mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %%classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main"
exit

:ONEPARAM
mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %%classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main %1"
exit

:TWOPARAM
mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %%classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main %1 %2"
exit

:THREEPARAM
mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %%classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main %1 %2 %3"
exit

:FOURPARAM
mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %%classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main %1 %2 %3 %4"
exit

:FIVEPARAM
mvn exec:exec -DskipTests -Dexec.executable="java" -Dexec.args="-cp %%classpath at.ac.tuwien.infosys.jcloudscale.shmt.Main %1 %2 %3 %4 %5"
exit