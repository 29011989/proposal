package jcloudscale.staticHostTool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.logging.Level;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.messaging.ActiveMQHelper;
import at.ac.tuwien.infosys.jcloudscale.shmt.StaticHostToolClient;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;


public class StaticHostToolTest {

	private JCloudScaleConfiguration config;
	private ActiveMQHelper server;
	
	private JCloudScaleConfiguration configure() {

		return new JCloudScaleConfigurationBuilder()
		.withLogging(Level.INFO)
		.build();
	}
	
	@Before
	public void start() throws Exception
	{
		config = configure();
		JCloudScaleClient.setConfiguration(config);
		server = new ActiveMQHelper(config.common().communication());
		server.start();
	}


	@After
	public void cleanup() throws Exception
	{
		if(server != null)
			server.close();
	}
	
	@Test
	public void startStopHostTest()
	{
		//
		// Starting Static host
		//
		try(StaticHostToolClient client = new StaticHostToolClient(config))
		{
			assertTrue("Failed to start static host management client.", client.start());
			assertEquals("There are static hosts running prior we started execution!", 
					0, client.getRunningHostsCount());
			
			IVirtualHost host = client.startHost(null);
			assertEquals("Started Static Host is missing!", 1, client.getRunningHostsCount());
			assertTrue("Started Static Host is missing!", client.getRunningHosts().contains(host));
		}
		
		
		//
		// Stopping Static Host
		//
		try(StaticHostToolClient client = new StaticHostToolClient(config))
		{
			assertTrue("Failed to start static host management client.", client.start());
			assertEquals("Static Host that is expected to run is missing!", 
					1, client.getRunningHostsCount());
			
			IVirtualHost host = client.getRunningHosts().get(0);
			
			assertTrue("Failed to stop static host!", client.stopHost(host.getId()));
		}
		
		//
		// Ensuring its missing
		//
		try(StaticHostToolClient client = new StaticHostToolClient(config))
		{
			assertTrue("Failed to start static host management client.", client.start());
			assertEquals("There are static hosts running after complete execution!", 
					 0, client.getRunningHostsCount());
		}
	}
}
