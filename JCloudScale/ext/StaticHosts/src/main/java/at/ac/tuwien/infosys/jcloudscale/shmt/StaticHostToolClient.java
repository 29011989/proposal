package at.ac.tuwien.infosys.jcloudscale.shmt;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.jms.JMSException;
import javax.naming.NamingException;

import at.ac.tuwien.infosys.jcloudscale.classLoader.caching.dto.ClassLoaderRequest;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.IMQWrapper;
import at.ac.tuwien.infosys.jcloudscale.messaging.JMSConnectionHolder;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.TimeoutException;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;
import at.ac.tuwien.infosys.jcloudscale.vm.VirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalCloudPlatformConfiguration;

public class StaticHostToolClient implements Closeable {

	private JCloudScaleConfiguration config;
	private IdManager idManager;
	private Map<UUID, IVirtualHost> idToHostMap = new HashMap<>();

	public StaticHostToolClient(JCloudScaleConfiguration config) {

		this.config = config;
	}

	public boolean start() {

		System.out.println("Applying configuration...");
		JCloudScaleClient.setConfiguration(config);

		try {
			//
			// Verifying if everything is ok.
			//
			if (!isMQRunning())
				return false;

			if (!ensureNoClientRunning())
				return false;

			//
			// Starting necessary services.
			//
			JCloudScaleClient.getClient();
			this.idManager = new StaticHostIdManager(config.common().communication());

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	public int getRunningHostsCount() {

		return this.idManager.getRegisteredInstances().size();
	}

	public String getServerAddress() {

		return this.config.common().communication().getServerAddress().trim().replace("http://", "")
				.replace("tcp://", "").replaceAll("\\?.+", "");
	}

	public int getServerPort() {

		return this.config.common().communication().getServerPort();
	}

	public List<IVirtualHost> getRunningHosts() {

		List<IVirtualHost> newStartedHosts = new ArrayList<>();

		for (UUID id : this.idManager.getRegisteredInstances())
			if (!idToHostMap.containsKey(id)) {
				IVirtualHost host = this.config.server().cloudPlatform().getVirtualHost(idManager);
				host.startupHost(null, null);
				newStartedHosts.add(host);
			}

		for (IVirtualHost host : newStartedHosts)
			idToHostMap.put(host.getId(), host);

		List<IVirtualHost> res = new ArrayList<>();
		res.addAll(idToHostMap.values());
		Collections.sort(res, new Comparator<IVirtualHost>() {
			@Override
			public int compare(IVirtualHost o1, IVirtualHost o2) {

				return o1.getId().compareTo(o2.getId());
			}
		});

		return res;
	}

	public String getDefaultInstanceType() {

		try {
			CloudPlatformConfiguration cloudPlatform = this.config.server().cloudPlatform();
			if (cloudPlatform instanceof LocalCloudPlatformConfiguration)
				return "";

			Field field = cloudPlatform.getClass().getDeclaredField("newInstanceType");
			if (!field.isAccessible())
				field.setAccessible(true);
			return (String) field.get(cloudPlatform);
		} catch (RuntimeException ex) {
			throw ex;
		} catch (Exception ex) {
			return "";
		}
	}

	public IVirtualHost startHost(String hostSize) {

		IVirtualHost host = this.config.server().cloudPlatform().getVirtualHost(this.idManager);
		host.startupHost(null, hostSize);
		idToHostMap.put(host.getId(), host);
		return host;
	}

	public boolean stopHost(UUID hostId) {

		IVirtualHost host = idToHostMap.remove(hostId);
		if (host == null)
			return false;

		// telling host that it is not static.
		try {
			Field field = VirtualHost.class.getDeclaredField("isStatic");
			if (!field.isAccessible())
				field.setAccessible(true);
			field.setBoolean(host, false);
		} catch (Exception ex) {
			System.out
					.println("Failed to modify host status! Seems that internal structure of host proxy changed and someone forgot to update Static Host Management Tool!");
		}
		host.close();
		idManager.removeId(hostId);
		return true;
	}

	public List<IVirtualHost> getHostsByIP(String ipAddress) {

		List<IVirtualHost> result = new ArrayList<>();

		for (IVirtualHost host : this.idToHostMap.values())
			if (host.getIpAddress() != null && host.getIpAddress().equals(ipAddress))
				result.add(host);

		return result;
	}

	@Override
	public void close() {

		for (IVirtualHost host : idToHostMap.values())
			host.close();

		if (idManager != null)
			idManager.close();

			JCloudScaleClient.closeClient();
	}

	// --------------------------------------PRIVATE METHODS------------------------------------

	private boolean ensureNoClientRunning() {

		MessageQueueConfiguration mqConfig = this.config.common().communication().clone();
		mqConfig.setRequestTimeout(250);
		try (IMQWrapper mq = mqConfig.newWrapper()) {
			UUID id = UUID.randomUUID();
			String requestQueue = getFieldValue(this.config.common().classLoader(), "requestQueue");
			String responseQueue = getFieldValue(this.config.common().classLoader(), "responseQueue");

			if (requestQueue == null || responseQueue == null)
				return true;// we failed to get configured classloading topic values, let's assume everything is fine.

			mq.createTopicConsumer(responseQueue, "JMSCorrelationID = '" + id + "'");
			mq.createQueueProducer(requestQueue);

			ClassLoaderRequest request = new ClassLoaderRequest(Object.class.getName());
			try {
				mq.requestResponse(request, id);
			} catch (JMSException | TimeoutException e) {
				return true;
			}

			// if we managed to get response, we're not alone.
			System.out.printf("%1$s %2$s detected JCloudScale-based application "
					+ "that is currently using configured Message Queue Server.%n  "
					+ "Unfortunately, %2$s cannot operate in parallel with other applications.%n  "
					+ "Please, shut down the application and run the %2$s again.%n", Main.ERROR, Main.APP_NAME);
			return false;
		} catch (NamingException | JMSException e) {
			e.printStackTrace();
		}
		return true;
	}

	private String getFieldValue(Object obj, String filed) {

		try {
			Field field = obj.getClass().getDeclaredField(filed);
			if (!field.isAccessible())
				field.setAccessible(true);
			return field.get(obj).toString();
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

	private boolean isMQRunning() throws NamingException, JMSException {

		String hostname = config.common().communication().getServerAddress();
		int port = config.common().communication().getServerPort();

		if (!JMSConnectionHolder.isMessageQueueServerAvailable(hostname, port)) {
			System.out
					.printf("%1$s Failed to connect to Message Queue Server \"%2$s:%3$s\". Ensure it is running and accessible.%n",
							Main.ERROR, hostname, port);
			return false;
		}

		System.out.printf("Successfully connected to \"%1$s:%2$s\"%n", hostname, port);
		return true;
	}

}
