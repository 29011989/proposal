package at.ac.tuwien.infosys.jcloudscale.shmt;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import asg.cliche.Command;
import asg.cliche.Param;
import asg.cliche.Shell;
import asg.cliche.ShellDependent;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;

public class CommandHandler implements ShellDependent, Closeable {
	// private Shell shell;
	private StaticHostToolClient client;
	private ExecutorService threadPool = Executors.newCachedThreadPool();

	public CommandHandler(StaticHostToolClient client) {

		this.client = client;
	}

	@Override
	public void cliSetShell(Shell shell) {

		// this.shell = shell;
	}

	@Command(name = "list", abbrev = "ls", description = "Lists available static hosts.")
	public void listHosts() {

		for (IVirtualHost host : client.getRunningHosts())
			System.out.printf("\t%s @ %s%n", host.getId(), host.getIpAddress());
		System.out.printf("%s static hosts currently available.%n", client.getRunningHostsCount());
	}

	@Command(name = "start", abbrev = "start", description = "Starts a new static host with default size.")
	public void startHost() {

		startHost(1, null);
	}

	@Command(name = "start", abbrev = "start", description = "Starts a new static host with a specified size.")
	public void startHost(
			@Param(name = "<An instance type (flavour name) to use for new static host>") String instanceType) {

		startHost(1, instanceType);
	}

	@Command(name = "start", abbrev = "start", description = "Starts a specified count of new static hosts.")
	public void startHost(@Param(name = "<Amount of static hosts to start>") int count,
			@Param(name = "<An instance type (flavour name) to use for new static host>") final String instanceType) {

		System.out.printf("Starting %s new \"%s\" hosts.%n", count,
				instanceType == null ? this.client.getDefaultInstanceType() : instanceType);

		final CountDownLatch latch = new CountDownLatch(count);

		for (int i = 1; i <= count; ++i) {
			threadPool.execute(new Runnable() {
				@Override
				public void run() {
					try
					{
						long start = System.nanoTime();
						IVirtualHost host = client.startHost(instanceType);
						long startTimeSec = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start);
						System.out.printf("Host %s with address %s started successfully in %ssec.%n",
								host.getId(), host.getIpAddress(), startTimeSec);
					}
					catch(Exception ex)
					{
						System.out.println("Failed to start a host: "+ex);
					}
					finally
					{
						latch.countDown();
					}
				}
			});
		}
		
		long waitStartTime = System.nanoTime();
		try
		{
			while(!latch.await(5, TimeUnit.SECONDS))
			{
				String warningMessage = "";
				if(TimeUnit.NANOSECONDS.toMinutes(System.nanoTime() - waitStartTime) > 2)
				{
					warningMessage = " Host startup takes too long!";
					if(client.getRunningHostsCount() == 0)
						warningMessage += String.format("%n  (Hosts appear in web UI? Is MQ Server \"%s:%s\" accessible from cloud hosts?)",
							client.getServerAddress(), client.getServerPort());
				}
				
				System.out.printf("Waiting for %s hosts to start...%s%n", latch.getCount(), warningMessage);
			}
		}
		catch(InterruptedException ex)
		{
			System.out.println("Host startup sequence interrupted!");
		}
		
		listHosts();
	}

	@Command(name = "stop", abbrev = "stop", description = "Stops any static host.")
	public void stopHost() {

		if (this.client.getRunningHostsCount() == 0) {
			System.out.println("Currently there's no hosts running. Nothing to stop.");
			return;
		}

		stopHost(this.client.getRunningHosts().get(0).getId());
	}

	@Command(name = "stop", abbrev = "stop", description = "Stops particular host or all hosts.")
	public void stopHost(
			@Param(name = "<An id or ip of static host to stop or \"all\" to stop all hosts>") String hostToStop) {

		if (this.client.getRunningHostsCount() == 0) {
			System.out.println("Currently there's no hosts running. Nothing to stop.");
			return;
		}

		try {
			if ("all".equals(hostToStop.toLowerCase())) {

				System.out.printf("Stopping all %s static hosts...%n", this.client.getRunningHostsCount());
				for (IVirtualHost host : this.client.getRunningHosts())
					stopHost(host.getId());
			} else {
				try {
					UUID id = UUID.fromString(hostToStop);
					stopHost(id);
					return;
				} catch (IllegalArgumentException ex) {// failed to parse UUID.
				}

				// this is not an UUID, trying IP.
				List<IVirtualHost> hosts = client.getHostsByIP(hostToStop);
				if (hosts.size() == 0) {
					System.out.println("Failed to find host that have either IP or ID \"" + hostToStop + "\"");
					return;
				}

				UUID idToStop = hosts.get(0).getId();
				if (hosts.size() > 1) {
					System.out.printf(
							" ** WARNING: ** There's %s hosts with IP address \"%s\". Stopping one of them (%s)%n",
							hosts.size(), hostToStop, idToStop);
				}
				
				stopHost(idToStop);
			}
		} finally {
			listHosts();
		}
	}

	private void stopHost(UUID hostId) {
		System.out.println("Shutting down host "+hostId+"...");
		this.client.stopHost(hostId);
		System.out.println("Shut down host " + hostId);
	}

	@Override
	public void close() throws IOException {
		if(this.threadPool != null)
			this.threadPool.shutdown();
	}
}
