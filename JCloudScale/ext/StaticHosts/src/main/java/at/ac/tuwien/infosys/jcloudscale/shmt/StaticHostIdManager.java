package at.ac.tuwien.infosys.jcloudscale.shmt;

import java.util.UUID;

import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IdManager;

public class StaticHostIdManager extends IdManager {

	public StaticHostIdManager(MessageQueueConfiguration communicationConfiguration) {
		super(communicationConfiguration);
	}

	@Override
	public boolean isStaticId(UUID serverId) {
		return true;
	}

}
