package at.ac.tuwien.infosys.jcloudscale.shmt;

import asg.cliche.Shell;
import asg.cliche.ShellFactory;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;

public class Main {
	public static final String APP_NAME = "JCloudScale Static Hosts Management Tool";
	public static final String ERROR = " ** ERROR: ** ";
	private static final String[] CFG_DEFAULT_EXTS = new String[] { ".xml", ".cfg" };

	public static void main(String[] args) throws Exception {

		try {
			//
			// Loading configuration
			//
			JCloudScaleConfiguration config = ConfigurationLoader.loadConfiguration(args, CFG_DEFAULT_EXTS);

			if (config == null) {
				System.out.println(ERROR + APP_NAME
						+ " cannot work without JCloudScaleConfiguration instance specified.");
				return;
			}

			//
			// Verifying and Applying configuration
			//
			try (StaticHostToolClient client = new StaticHostToolClient(config);
					CommandHandler handler = new CommandHandler(client)) {
				if (!client.start())
					return;

				//
				// Creating objects
				//
				String mqServer = client.getServerAddress();
				String prompt = "JCS@" + mqServer;

				Shell shell = ShellFactory.createConsoleShell(prompt, APP_NAME, handler);

				//
				// Determining whether we have command passed or should work in interactive mode
				//

				boolean interactiveMode = args.length < 2;
				String commandToExecute = "ls";// if no command passed, we just list existing hosts.
				if (!interactiveMode)
					commandToExecute = prepareCommandToExecute(args);
				else {
					// Printing welcome message
					System.out.printf("Welcome to %1$s!%nThis tool supports following commands:%n", APP_NAME);
					shell.processLine("?list");
					System.out.println("To exit, type \"exit\"");
				}

				shell.processLine(commandToExecute);

				//
				// Starting command loop
				//
				if (interactiveMode)
					shell.commandLoop();
			}
		} finally {
			System.out.println(APP_NAME + " terminated.");
		}
	}

	private static String prepareCommandToExecute(String[] args) {

		if (args.length < 2)
			return "";

		StringBuilder builder = new StringBuilder();

		builder.append(args[1]);
		for (int i = 2; i < args.length; ++i)
			builder.append(" ").append(args[i]);

		return builder.toString();
	}
}
