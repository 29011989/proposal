package at.ac.tuwien.infosys.jcloudscale.shmt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;

public class ConfigurationLoader {
	public static JCloudScaleConfiguration loadConfiguration(String[] programArgs, String[] cfgExts) throws IOException {

		System.out.println("Trying to load configuration...");
		JCloudScaleConfiguration config = null;
		// loading from app path
		if (programArgs != null && programArgs.length > 0)
			config = loadConfiguration(programArgs[0], true);

		// searching in current directory
		if (config == null)
			config = automaticLoadConfiguration(cfgExts);

		// requesting from user.
		if (config == null)
			config = requestConfiguration();

		return config;
	}

	private static JCloudScaleConfiguration requestConfiguration() throws IOException {

		JCloudScaleConfiguration result = null;
		System.out.printf(
				"Please, specify the path to serialized JCloudScale configuration class. Press ENTER to abort.%n"
						+ "(e.g., \"%1$sfolder%1$ssubfolder%1$sconfigFile.xml\"):%n", File.separator);

		String prompt = new File(".").getCanonicalPath();
		if (!prompt.endsWith(File.separator))
			prompt = prompt + File.separator;

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, Charset.defaultCharset()));
		while (result == null) {
			System.out.print(prompt);
			String path = reader.readLine();
			
			if(path != null)
				path = path.trim();

			if (path == null || path.length() == 0)
				break;

			result = loadConfiguration(path, true);
		}
		return result;
	}

	private static JCloudScaleConfiguration automaticLoadConfiguration(final String[] cfgExts) throws IOException {

		if (cfgExts == null || cfgExts.length == 0)
			return null;

		System.out.printf("Trying to find configuration in current directory (%s)...%n",
				allowedExtensionsToString(cfgExts));

		File curDir = new File(".");
		String[] files = curDir.list(new FilenameFilter() {
			@Override
			public boolean accept(File localPath, String filename) {

				for (String ext : cfgExts)
					if (filename.endsWith(ext))
						return true;
				return false;
			}
		});

		JCloudScaleConfiguration config = null;
		for (String path : files) {
			config = loadConfiguration(path, false);
			if (config != null)
				break;
		}
		if (config == null)
			System.out.printf("Matched %s files, none of them contained valid JCloudScale configuration.%n",
					files.length);

		return config;
	}

	private static String allowedExtensionsToString(String[] cfgExts) {

		StringBuilder builder = new StringBuilder();
		builder.append("\"").append("*").append(cfgExts[0]);

		for (int i = 1; i < cfgExts.length; ++i)
			builder.append(", ").append("*").append(cfgExts[i]);

		return builder.append("\"").toString();
	}

	private static JCloudScaleConfiguration loadConfiguration(String path, boolean printErrorMessage)
			throws IOException {

		File configFile = new File(path);
		if (!configFile.exists() || !configFile.isFile()) {
			if (printErrorMessage)
				System.out.printf("%sPath \"%s\" does not exist or is not a file.%n", Main.ERROR,
						configFile.getCanonicalPath());
			return null;
		}
		JCloudScaleConfiguration config = null;

		try {
			config = JCloudScaleConfiguration.load(configFile);
			System.out.printf("Successfully loaded \"%s\" configuration from \"%s\".%n", 
						config.server().cloudPlatform().getClass().getSimpleName().replace("Configuration", ""), 
						configFile.getCanonicalPath());
		} catch (Exception ex) {
			if (printErrorMessage) {
				System.out.printf("%sFailed to load configuration from \"%s\".%n", Main.ERROR,
						configFile.getCanonicalPath());
				ex.printStackTrace();
			}
		}

		return config;
	}
}
