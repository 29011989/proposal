/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.vm.ec2;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.ActiveMQHelper;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.server.PlatformSpecificUtil;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IIdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.VirtualHostPool;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EC2CloudPlatformConfiguration extends CloudPlatformConfiguration {
	private static final long serialVersionUID = 1L;

	/**
	 * The EC2 user script template that creates a file with MQ server access credentials
	 */
	private static final String mqAddressDistributionUserScript = "#!/bin/sh" + "\n" + // have to use Linux separators
																						// here.
			"mkdir -p %s" + "\n" + // creating directory, if it is missing.
			"echo \"%s\" > %s"; // writing file.
	
	private static final String ACCESS_KEY_PROPERTY = "accessKey";
	private static final String SECRET_KEY_PROPERTY = "secretKey";
	private static final String AWS_ENDPOINT_PROPERTY = "awsEndpoint";

	private String imageName = "JCloudScale" + "_v" + JCloudScaleConfiguration.CS_VERSION;// "JCloudScale_v0.1"
	private String mqImageName = "ActiveMQ";

	private String newInstanceType = "t1.micro";
	private String newMqInstanceType = "m1.small";

	private String sshKey = "";// if empty -- don't attach anything.

	private String awsEndpoint;

	private String accessKey;
	private String secretKey;

	private transient EC2Wrapper ec2Wrapper = null;

	// ----------------------------------------------------------------------

	/**
	 * Creates the new instance of <b>EC2CloudPlatformConfiguration</b> with default configuration.
	 * Is not sufficient unless you specify <b>accessKey</b>, <b>secretKey</b> and <b>awsEndpoint</b> separately.
	 */
	public EC2CloudPlatformConfiguration() {

	}
	
	/**
	 * Creates a new instance of <b>EC2CloudPlatformConfiguration</b> with the configuration values stored in the
	 * properties.
	 * 
	 * @param properties
	 *            The collection of properties that can be used to initialize this instance of
	 *            <b>EC2CloudPlatformConfiguration</b>. 
	 *            Particularly, "accessKey", "secretKey" and "awsEndpoint" properties are expected.
	 */
	public EC2CloudPlatformConfiguration(Properties properties)
	{
		this(properties.getProperty(ACCESS_KEY_PROPERTY), properties.getProperty(SECRET_KEY_PROPERTY), 
				properties.getProperty(AWS_ENDPOINT_PROPERTY));
	}
	
	/**
	 * Creates a new instance of <b>EC2CloudPlatformConfiguration</b> with the configuration values stored in the
	 * properties file.
	 * 
	 * @param accessKeyFilePath
	 *            The file name of the file that contains properties that can be used to initialise new instance of
	 *            <b>EC2CloudPlatformConfiguration</b>.
	 *            This file should contain "accessKey", "secretKey" and "awsEndpoint" properties.
	 * @throws IOException
	 *             In case the file is missing or cannot be read.
	 */
	public EC2CloudPlatformConfiguration(String accessKeyFilePath) throws IOException
	{
		this(loadProperties(accessKeyFilePath));
	}

	/**
	 * Constructs a new instance of EC2 configuration.
	 * When an instance of this configuration is specified to JCloudScale configuration,
	 * all host-related operations (startup, lookup, shutdown...) will be performed using
	 * Amazon EC2 credentials provided.
	 * 
	 * @param accessKey
	 *            The AWS access key.
	 * @param secretKey
	 *            The AWS secret access key.
	 */
	public EC2CloudPlatformConfiguration(String accessKey, String secretKey) {

		setAccessKey(accessKey);
		setSecretKey(secretKey);
	}

	/**
	 * Constructs a new instance of EC2 configuration.
	 * When an instance of this configuration is specified to JCloudScale configuration,
	 * all host-related operations (startup, lookup, shutdown...) will be performed using
	 * Amazon EC2 credentials provided.
	 * 
	 * @param accessKey
	 *            The AWS access key.
	 * @param secretKey
	 *            The AWS secret access key.
	 * @param awsEndpoint
	 *            The address of the Amazon EC2 endpoint.
	 */
	public EC2CloudPlatformConfiguration(String accessKey, String secretKey, String awsEndpoint) {

		this(accessKey, secretKey);
		setAwsEndpoint(awsEndpoint);
	}

	String getImageName() {

		return imageName;
	}

	/**
	 * Specifies the name of the image that will be used to start new instances in the EC2 Cloud.
	 * 
	 * @param imageName
	 *            The existing name of the image that new virtual hosts will be spawned from.
	 */
	public void setImageName(String name) {

		this.imageName = name;
	}

	String getInstanceType() {

		return newInstanceType;
	}

	/**
	 * Specifies the type name of the new virtual hosts that should be started.
	 * 
	 * @param instanceTypeName
	 *            The type name (e.g., "m1.micro") of the new virtual hosts that should be used.
	 */
	public void setInstanceType(String type) {

		this.newInstanceType = type;
	}

	String getMqImageName() {

		return mqImageName;
	}

	/**
	 * Specifies the image name that should be used to start new Message Queue host.
	 * 
	 * @param mqImageName
	 *            The existing name of the image that can be used in case
	 *            Message Queue host has to be started.
	 */
	public void setMqImageName(String imageName) {

		this.mqImageName = imageName;
	}

	String getSshKey() {

		return this.sshKey;
	}

	String getMqInstanceTypeName() {

		return newMqInstanceType;
	}

	/**
	 * Specifies the SSH key name that may be used to access the instance through SSH connection.
	 * 
	 * @param sshKeyName
	 *            The name of existing SSH key that will be attached to the new virtual host
	 *            to allow remote access.
	 */
	public void setSshKey(String sshKey) {

		this.sshKey = sshKey;
	}

	synchronized void setEC2Wrapper(EC2Wrapper ec2Wrapper) {

		this.ec2Wrapper = ec2Wrapper;
	}

	/**
	 * Specifies the instance type of the Message Queue host.
	 * 
	 * @param mqInstanceTypeName
	 *            The type name (e.g., "m1.small") of the message queue host that should be used.
	 */
	public void setNewMqInstanceType(String newMqInstanceType) {

		this.newMqInstanceType = newMqInstanceType;
	}

	String getAwsEndpoint() {

		return awsEndpoint;
	}

	/**
	 * Specifies the address of the Amazon EC2 endpoint.
	 * 
	 * @param awsEndpoint
	 *            The address of the Amazon EC2 endpoint.
	 */
	public void setAwsEndpoint(String awsEndpoint) {

		this.awsEndpoint = awsEndpoint;
	}

	private void setAccessKey(String accessKey) {

		this.accessKey = accessKey;
	}

	private void setSecretKey(String secretKey) {

		this.secretKey = secretKey;
	}

	String getAccessKey() {

		return accessKey;
	}

	String getSecretKey() {

		return secretKey;
	}

	// ----------------------------------------------------------------------

	/**
	 * Specifies the address of the Amazon EC2 endpoint.
	 * 
	 * @param awsEndpoint
	 *            The address of the Amazon EC2 endpoint.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withAwsEndpoint(String awsEndpoint) {

		setAwsEndpoint(awsEndpoint);
		return this;
	}

	/**
	 * Specifies the Amazon Web Service Access Key.
	 * 
	 * @param accessKey
	 *            The AWS access key.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withAccessKey(String accessKey) {

		setAccessKey(accessKey);
		return this;
	}

	/**
	 * Specifies the Amazon Web Service Secret Key.
	 * 
	 * @param secretKey
	 *            The AWS secret key.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withSecretKey(String secretKey) {

		setSecretKey(secretKey);
		return this;
	}

	/**
	 * Specifies the name of the image that will be used to start new instances in the EC2 Cloud.
	 * 
	 * @param imageName
	 *            The existing name of the image that new virtual hosts will be spawned from.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withInstanceImage(String imageName) {

		setImageName(imageName);
		return this;
	}

	/**
	 * Specifies the type name of the new virtual hosts that should be started.
	 * 
	 * @param instanceTypeName
	 *            The type name (e.g., "m1.micro") of the new virtual hosts that should be used.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withInstanceType(String instanceTypeName) {

		setInstanceType(instanceTypeName);
		return this;
	}

	/**
	 * Specifies the SSH key name that may be used to access the instance through SSH connection.
	 * 
	 * @param sshKeyName
	 *            The name of existing SSH key that will be attached to the new virtual host to allow remote access.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withSshKey(String sshKeyName) {

		setSshKey(sshKeyName);
		return this;
	}

	/**
	 * Specifies the image name that should be used to start new Message Queue host.
	 * 
	 * @param mqImageName
	 *            The existing name of the image that can be used in case Message Queue host has to be started.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withMQImage(String mqImageName) {

		setMqImageName(mqImageName);
		return this;
	}

	/**
	 * Specifies the instance type of the Message Queue host.
	 * 
	 * @param mqInstanceTypeName
	 *            The type name (e.g., "m1.small") of the message queue host that should be used.
	 * @return The current instance of <b>EC2CloudPlatformConfiguration</b> to continue configuration.
	 */
	public EC2CloudPlatformConfiguration withMQInstanceType(String mqInstanceTypeName) {

		setNewMqInstanceType(mqInstanceTypeName);
		return this;
	}

	// ----------------------------------------------------------------------

	@Override
	public IVirtualHost getVirtualHost(IIdManager idManager) {

		boolean startPerformanceMonitoring = JCloudScaleConfiguration.getConfiguration().common().monitoring()
				.isEnabled();

		return new EC2Instance(this, idManager, startPerformanceMonitoring);
	}

	// it's public only for tests. change test's package?
	public synchronized EC2Wrapper getEC2Wrapper() {

		if (this.ec2Wrapper == null)
			try {
				this.ec2Wrapper = new EC2Wrapper(this);
			} catch (IOException e) {
				e.printStackTrace();
			}

		return this.ec2Wrapper;
	}

	@Override
	protected AutoCloseable startMessageQueueServer(MessageQueueConfiguration communicationConfiguration)
			throws Exception {

		String mqAddress = communicationConfiguration.getServerAddress();
		if ("localhost".equals(mqAddress) || PlatformSpecificUtil.isThisHostIPAddress(mqAddress)) {
			// we should start message queue on localhost
			MessageQueueConfiguration mqConfig = communicationConfiguration.clone();
			mqConfig.setServerAddress("0.0.0.0");
			ActiveMQHelper mqServer = new ActiveMQHelper(mqConfig);
			mqServer.start();
			return mqServer;
		} else {
			final String ec2Id = getEC2Wrapper().startNewMessageQeueHost();

			if (ec2Id == null)
				throw new Exception("Failed to start MQ Server in EC2.");

			return new Closeable() {
				@Override
				public void close() throws IOException {

					getEC2Wrapper().shutdownHost(ec2Id);
				}
			};
		}
	}

	@Override
	public VirtualHostPool getVirtualHostPool() {

		return new VirtualHostPool(this, getMessageQueueConfiguration());
	}

	public String generateHostUserData() {

		Logger log = JCloudScaleConfiguration.getLogger(this);
		MessageQueueConfiguration mqConfig = getMessageQueueConfiguration();
		String mqAddress = mqConfig.getServerAddress();
		if("localhost".equals(mqAddress))
		{
			try {
				mqAddress = PlatformSpecificUtil.findBestIP();
			} catch (UnknownHostException | SocketException e) {
				log.severe("Failed to detect IP address of current host: "+e);
			}
		}
		String mqServerLocationData = mqAddress + ":" + mqConfig.getServerPort();
		log.info("Configuring remote host to connect to message queue \""+mqServerLocationData+"\"");

		String mqFileOnServer = mqConfig.getMessageQueueConnectionFilePath();
		String mqFolderOnServer = new File(mqFileOnServer).getParent().replace('\\', '/');// have to replace here to not
																							// have windows-slashes.

		return String.format(mqAddressDistributionUserScript, mqFolderOnServer, mqServerLocationData, mqFileOnServer);
	}
}
