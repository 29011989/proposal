/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.test.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.policy.sample.SingleHostScalingPolicy;
import at.ac.tuwien.infosys.jcloudscale.vm.ec2.EC2CloudPlatformConfiguration;

public class EC2ConfigurationHelper extends ConfigurationHelper
{
    private static final String EC2_CONFIG_FILE = "aws.props";
    
    private static final String SERVER_ADDRESS_CONFIG =  "mq.address";
	private static final String EC2_ACCESS_KEY = "accessKey";
	private static final String EC2_SECRET_KEY = "secretKey";

    public static JCloudScaleConfiguration createDefaultAmazonTestConfiguration() throws FileNotFoundException, IOException, URISyntaxException
    {
        Properties ec2Properties = new Properties();
        try(InputStream propertiesStream = ClassLoader.getSystemResourceAsStream(EC2_CONFIG_FILE))
        {
            ec2Properties.load(propertiesStream);
        }

        String serverAddress = ec2Properties.getProperty(SERVER_ADDRESS_CONFIG);

        if(serverAddress == null || serverAddress.length() == 0)
            throw new RuntimeException("Could not read ActiveMQ server address from the property \""+
                    SERVER_ADDRESS_CONFIG+"\" from the config file "+EC2_CONFIG_FILE);

        return applyCommonConfiguration(
                new JCloudScaleConfigurationBuilder(
                        new EC2CloudPlatformConfiguration()
                        .withAccessKey(ec2Properties.getProperty(EC2_ACCESS_KEY))
                        .withSecretKey(ec2Properties.getProperty(EC2_SECRET_KEY))
                        .withAwsEndpoint("ec2.eu-west-1.amazonaws.com")
                        .withInstanceType("t1.micro")
                        .withSshKey("aws_pl")
                        )
                .withMQServerHostname(serverAddress))
                .with(new SingleHostScalingPolicy())
                .build();
    }
}
