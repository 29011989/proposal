package at.ac.tuwien.infosys.jcloudscale.test.integration.ec2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.ec2.EC2CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.ec2.EC2Wrapper;

public class EC2Helper {
	public static final int STATIC_HOSTS_COUNT = 1;
	private static final long HOSTS_STARTUP_TIMEOUT = 15 * 60 * 1000;

	private List<String> staticHosts = new ArrayList<>();
	private EC2Wrapper ec2Wrapper;
	private MessageQueueConfiguration mqConfig;

	public EC2Helper(JCloudScaleConfiguration config) {

		this.mqConfig = config.common().communication();
		ec2Wrapper = ((EC2CloudPlatformConfiguration) config.server().cloudPlatform()).getEC2Wrapper();
	}

	public void startStaticInstances() throws InterruptedException {

		System.out.println("Starting " + STATIC_HOSTS_COUNT + " static hosts...");

		//
		// waiting for servers to actually start.
		//
		try (IdManager idManager = new IdManager(mqConfig)) {
			int existingHosts = idManager.getRegisteredInstances().size();
			if (existingHosts > 0)
				System.out.println("Detected " + existingHosts + " cloud hosts running already!");

			if (existingHosts < STATIC_HOSTS_COUNT) {
				int newHostsCount = STATIC_HOSTS_COUNT - existingHosts;
				System.out.println("Starting " + newHostsCount + " new static hosts...");
				for (int i = 0; i < newHostsCount; ++i)
					ec2Wrapper.startNewHost(null);
			} else
				System.out.println("Using already running hosts.");

			long startTime = System.nanoTime();
			boolean timeout = false;
			while (idManager.getRegisteredInstances().size() < STATIC_HOSTS_COUNT) {
				if (TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) > HOSTS_STARTUP_TIMEOUT) {
					System.out.println("Timeout waiting for hosts to start. Cleaning up environment and aborting.");
					timeout = true;
					break;
				}

				System.out.println("Waiting for hosts to start...");
				Thread.sleep(5000);
			}

			//
			// Saving static hosts ips
			//
			for (UUID hostId : idManager.getRegisteredInstances())
				staticHosts.add(idManager.getIpToId(hostId));

			if (timeout) {
				shudownStaticInstances();
				throw new RuntimeException("Failed to start additional hosts within the configured timeout of "
						+ HOSTS_STARTUP_TIMEOUT + "ms!");
			}

			System.out.printf("Started successfully %s static hosts: %s.%n", staticHosts.size(),
					Arrays.toString(staticHosts.toArray()));
		}
	}

	public void shudownStaticInstances() {

		System.out.printf("Shutting down %s static hosts: %s.%n", staticHosts.size(),
				Arrays.toString(staticHosts.toArray()));
		for (String hostIp : staticHosts) {
			System.out.println("Shutting down host " + hostIp + "...");
			try {
				ec2Wrapper.shutdownHost(hostIp);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean verifyWithAWS(IVirtualHost host, String instanceSize) {

		String ip = host.getIpAddress();
		String id = ec2Wrapper.findEC2Id(ip);
		String size = ec2Wrapper.getInstanceSize(id);
		return instanceSize.equals(size);
	}

	public void shutdownHost(IVirtualHost host) {

		System.out.println("Shutting down host " + host.getIpAddress() + "...");
		ec2Wrapper.shutdownHost(host.getIpAddress());
	}
}
