package at.ac.tuwien.infosys.jcloudscale.test.unit;

import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.ec2.EC2CloudPlatformConfiguration;

public class TestConfigurationEC2 extends TestConfiguration
{
    @Test
    public void testTrickyEC2Configuration() throws Exception
    {
        String accessKey = "some access key";
        String secretKey = "some super-duper secret key";
        String awsEndpoint = "some aws endpoint";
        String imageName = "some Image Name";
        String mqImageName = "Some MQ Image Name";
        
        JCloudScaleConfiguration config = createTrickyConfiguration(new EC2CloudPlatformConfiguration()
                                            .withAccessKey(accessKey)
                                            .withSecretKey(secretKey)
                                            .withAwsEndpoint(awsEndpoint)
                                            .withInstanceImage(imageName)
                                            .withMQImage(mqImageName));
        
        JCloudScaleConfiguration.getLogger(config, this);
        
        testCloning(config);
        traverseConfiguration(config);
        ensureSerializationWorks(config);
    }
}
