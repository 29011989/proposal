package at.ac.tuwien.infosys.jcloudscale.vm.bursting;

import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.exception.JCloudScaleException;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IIdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHostPool;

public class AggregatedCloudPlatformConfiguration extends CloudPlatformConfiguration {

	private static final long serialVersionUID = 1L;
	private CloudPlatformConfiguration[] cloudPlatforms;

	/**
	 * Creates new instance of aggregated cloud platform configuration for the single message queue scenario.
	 * 
	 * @param cloudPlatforms
	 *            The set of cloud platforms that will communicate through the single message queue.
	 */
	public AggregatedCloudPlatformConfiguration(CloudPlatformConfiguration... cloudPlatforms) {

		this(null, cloudPlatforms);
	}

	/**
	 * Creates the new instance of the aggregated cloud platform configuration for the multiple message queue scenario.
	 * Assumes that each cloud platform has its own message queue configuration
	 * and they are stored within aggregated message queue configuration in the same order as cloud platforms are
	 * provided.
	 * 
	 * @param messageQueueConfiguration
	 *            An instance of the aggregated message queue configuration that contains
	 *            a list of message queue configurations for each cloud platform.
	 * 
	 * @param cloudPlatforms
	 *            The list of cloud platforms that should be aggregated within this aggregated message queue
	 *            configuration.
	 */
	public AggregatedCloudPlatformConfiguration(AggregatedMessageQueueConfiguration messageQueueConfiguration,
			CloudPlatformConfiguration... cloudPlatforms) {

		this.cloudPlatforms = cloudPlatforms;

		//
		// Setting appropriate MessageQueueConfiguration for each cloud platform
		//
		if (messageQueueConfiguration == null)
			return;// everyone will use default one

		if (this.cloudPlatforms.length != messageQueueConfiguration.getMqConfigurations().length)
			throw new JCloudScaleException(
					"The amount of Message Queue Configurations is not equal to amount of cloud platforms.");

		for (int i = 0; i < cloudPlatforms.length; ++i) {
			// this.cloudPlatforms[i].setMessageQueueConfiguration(messageQueueConfiguration.getMqConfigurations()[i]);
			try {
				final Method setMQConfigMethod = CloudPlatformConfiguration.class.getDeclaredMethod(
						"setMessageQueueConfiguration", MessageQueueConfiguration.class);

				if (!setMQConfigMethod.isAccessible())
					AccessController.doPrivileged(new PrivilegedAction<Object>() {
			            @Override
			            public Object run() {
			                 setMQConfigMethod.setAccessible(true);
			                 return null;
			            }});

				setMQConfigMethod.invoke(this.cloudPlatforms[i], messageQueueConfiguration.getMqConfigurations()[i]);
			} catch (Exception ex) {
				throw new JCloudScaleException(ex, "Failed to specify partial message queue configuration.");
			}
		}
	}

	public CloudPlatformConfiguration[] getCloudPlatforms() {

		return cloudPlatforms;
	}

	@Override
	public IVirtualHost getVirtualHost(IIdManager idManager) {

		throw new RuntimeException("Not Implementable");
	}

	@Override
	public IVirtualHostPool getVirtualHostPool() {

		return new AggregatedVirtualHostPool(this);
	}

	@Override
	public AutoCloseable ensureCommunicationServerRunning() throws Exception {

		final List<AutoCloseable> messageQueues = new ArrayList<>();
		for (CloudPlatformConfiguration cfg : cloudPlatforms) {
			try {
				messageQueues.add(cfg.ensureCommunicationServerRunning());
			} catch (Exception ex) {
				JCloudScaleConfiguration.getLogger(this).warning(
						String.format("Failed to ensure communication server on platform %s is running: %s",
								cfg.getClass().getName(),
								ex));
			}
		}

		return new AutoCloseable() {
			@Override
			public void close() throws Exception {

				for (AutoCloseable mq : messageQueues)
					if (mq != null)
						mq.close();
			}
		};
	}

	@Override
	protected AutoCloseable startMessageQueueServer(MessageQueueConfiguration communicationConfiguration)
			throws Exception {

		return null;
	}
}

//
// + !client has to listen for updates on all servers in all queues!
// LogReceiver, SysoutputReceiver, classProvider, configurationProvider,
// IdManager, JCloudScaleReferenceManager, EventCorrelationEngine, MonitoringMQHelper
// ...
//
