package at.ac.tuwien.infosys.jcloudscale.test.unit;

import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.openstack.OpenstackCloudPlatformConfiguration;


public class TestConfigurationOpenstack extends TestConfiguration
{
    @Test
    public void testTrickyOpenstackConfiguration() throws Exception
    {
        String identityUrl = "some-identity-url";
        String tenantName = "some-tenant-name";
        String imageName = "some image name";
        String mqImageName = "someMQImageName0";
        String login ="my login";
        String password = "password";
        
        JCloudScaleConfiguration config = createTrickyConfiguration(
                new OpenstackCloudPlatformConfiguration(identityUrl, tenantName, imageName, login, password)
                .withMQImage(mqImageName));

        JCloudScaleConfiguration.getLogger(config, this);
        
        testCloning(config);
        traverseConfiguration(config);
        ensureSerializationWorks(config);
    }
 
}
