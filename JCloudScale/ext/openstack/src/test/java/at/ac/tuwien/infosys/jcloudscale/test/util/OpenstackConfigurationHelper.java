/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.test.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.vm.openstack.OpenstackCloudPlatformConfiguration;

public class OpenstackConfigurationHelper extends ConfigurationHelper
{
    private static final String OPENSTACK_CONFIG_FILE = "openstack.props";
    
    private static final String SERVER_ADDRESS_CONFIG =  "mq.address";

    public static JCloudScaleConfigurationBuilder createDefaultCloudTestConfiguration() throws FileNotFoundException, IOException
    {
        Properties openstackProperties = new Properties();
        try(InputStream propertiesStream = ClassLoader.getSystemResourceAsStream(OPENSTACK_CONFIG_FILE))
        {
            openstackProperties.load(propertiesStream);
        }

        String serverAddress = openstackProperties.getProperty(SERVER_ADDRESS_CONFIG);

        if(serverAddress == null || serverAddress.length() == 0)
            throw new RuntimeException("Could not read ActiveMQ server address from the property \""+
                    SERVER_ADDRESS_CONFIG+"\" from the config file "+OPENSTACK_CONFIG_FILE);
        
        
        OpenstackCloudPlatformConfiguration cloud =  new OpenstackCloudPlatformConfiguration(openstackProperties);
        return applyCommonConfiguration(new JCloudScaleConfigurationBuilder(cloud)
        	.withMQServerHostname(serverAddress));

    }
}
