/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package src.test.java.at.ac.tuwien.infosys.jcloudscale.test.database;

import at.ac.tuwien.infosys.jcloudscale.test.MyTest;
import at.ac.tuwien.infosys.jcloudscale.test.model.SuiteExecution;
import at.ac.tuwien.infosys.jcloudscale.test.model.TestExecution;
import com.google.common.collect.Lists;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;

/**
 * @author Gregor Schauer
 */
public class TestMonitorTest {
	@Test
	public void test() throws Exception {
		Injector injector = Guice.createInjector(new DatabaseModule());
		TestMonitor monitor = injector.getInstance(TestMonitor.class);

		long now = System.currentTimeMillis();
		TestExecution execution = new TestExecution(now - 100, now, MyTest.class.getName(), "test");
		execution.state = TestExecution.State.SUCCESS;

		SuiteExecution suite = new SuiteExecution();
		suite.className = TestMonitorTest.class.getName();
		suite.tests = Lists.newArrayList(execution);
		suite.start = execution.start;
		suite.end = execution.end;

		Long id = monitor.save(suite);
		System.out.println(id);
	}
}
