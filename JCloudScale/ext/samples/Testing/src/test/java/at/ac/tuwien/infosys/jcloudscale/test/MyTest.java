/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package src.test.java.at.ac.tuwien.infosys.jcloudscale.test;

import at.ac.tuwien.infosys.jcloudscale.test.test.JCloudScaleRunner;
import at.ac.tuwien.infosys.jcloudscale.test.util.ScriptManager;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

import javax.inject.Inject;
import javax.inject.Named;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author Gregor Schauer
 */
@RunWith(JCloudScaleRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/beans.xml")
public class MyTest {
	@Inject @Named("foo")
	String foo;
	
	@Test
	public void test1() throws InterruptedException {
		Thread.sleep(1000);
		assertTrue(true);
	}

	@Test
	public void test2() throws InterruptedException {
		Thread.sleep(1000);
		assertTrue(true);
	}

	@Test
	public void test3() throws InterruptedException {
		Thread.sleep(1000);
		assertTrue(true);
	}

	@Ignore
	@Test
	public void testIgnore() {
	}

	@Test
	public void testFailure() throws Exception {
		fail();
	}

	@Test
	public void testError() throws Exception {
		throw new ThreadDeath();
	}

	@Test(expected = IOException.class)
	public void testNotImplemented() throws IOException {
		throw new IOException();
	}

	@Test
	public void testScriptEngine() throws ScriptException, IOException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine scriptEngine = manager.getEngineByExtension("js");

		InputStream script = ScriptManager.load("tests/dateTest.js");

		long before = System.currentTimeMillis();
		Double eval = (Double) scriptEngine.eval(IOUtils.toString(script));
		long after = System.currentTimeMillis();

		assertTrue(before <= eval.longValue());
		assertTrue(eval.longValue() <= after);
	}
}
