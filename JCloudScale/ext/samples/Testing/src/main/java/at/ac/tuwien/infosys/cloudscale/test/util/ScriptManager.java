/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.util;

import at.ac.tuwien.infosys.jcloudscale.server.JCloudScaleServerRunner;

import java.io.InputStream;

/**
 * Loads scripts from the classpath.
 *
 * @author Gregor Schauer
 */
public class ScriptManager {
	static ClassLoader classLoader = JCloudScaleServerRunner.getInstance().createClassLoader(null);

	/**
	 * Returns an input stream for reading the specified resource.
	 *
	 * @param fileName the resource name
	 * @return an input stream for reading the resource, or {@code null} if the resource could not be found
	 * @see ClassLoader#getResourceAsStream(String)
	 */
	public static InputStream load(String fileName) {
		return classLoader.getResourceAsStream(fileName);
	}
}
