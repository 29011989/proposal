/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.database;

import at.ac.tuwien.infosys.jcloudscale.annotations.CloudObject;
import at.ac.tuwien.infosys.jcloudscale.test.database.mapping.SuiteExecutionMapper;
import at.ac.tuwien.infosys.jcloudscale.test.database.mapping.TestExecutionMapper;
import at.ac.tuwien.infosys.jcloudscale.test.model.SuiteExecution;
import at.ac.tuwien.infosys.jcloudscale.test.model.TestExecution;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.mybatis.guice.transactional.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

/**
 * Stores results from test executions and provides query methods for retrieving them on demand.
 *
 * @author Gregor Schauer
 */
@CloudObject
public class TestMonitor {
	@Inject
	SuiteExecutionMapper suiteExecutionMapper;
	@Inject
	TestExecutionMapper testExecutionMapper;
	
	@Transactional
	public Long save(SuiteExecution execution) {
		for (TestExecution test : execution.tests) {
			save(test);
		}
		return getSuiteExecutionMapper().insert(execution);
	}
	
	@Transactional
	public Long save(TestExecution execution) {
		return getTestExecutionMapper().insert(execution);
	}
	
	@Transactional
	public List<TestExecution> list(UUID id) {
		return getTestExecutionMapper().server(id);
	}

	@Transactional
	public List<TestExecution> list() {
		return getTestExecutionMapper().list();
	}

	private TestExecutionMapper getTestExecutionMapper() {
		if (testExecutionMapper == null) {
			initialize();
		}
		return testExecutionMapper;
	}

	private SuiteExecutionMapper getSuiteExecutionMapper() {
		if (suiteExecutionMapper == null) {
			initialize();
		}
		return suiteExecutionMapper;
	}

	private void initialize() {
		Injector injector = Guice.createInjector(new DatabaseModule());
		injector.injectMembers(this);

		testExecutionMapper.dropTable();
		suiteExecutionMapper.dropTable();
		suiteExecutionMapper.createTable();
		testExecutionMapper.createTable();
	}
}
