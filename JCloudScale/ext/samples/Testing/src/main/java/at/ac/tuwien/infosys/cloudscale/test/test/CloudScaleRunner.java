/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.test;

import at.ac.tuwien.infosys.jcloudscale.api.CloudObjects;
import at.ac.tuwien.infosys.jcloudscale.exception.JCloudScaleException;
import org.apache.commons.lang.ArrayUtils;
import org.junit.internal.runners.statements.InvokeMethod;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import java.io.Closeable;

/**
 * @author Gregor Schauer
 */
public class JCloudScaleRunner extends BlockJUnit4ClassRunner implements AutoCloseable, Closeable {
	Object test;
	
	public JCloudScaleRunner(Class<?> testClass) throws InitializationError {
		super(testClass);
	}

	@Override
	public void run(RunNotifier notifier) {
		JCloudScaleRunListener.getInstance().attach(notifier).addRunner(this);
		super.run(notifier);
	}

	@Override
	protected synchronized Object createTest() throws Exception {
		if (test == null) {
			test = CloudObjects.create(getTestClass().getJavaClass(), ArrayUtils.EMPTY_OBJECT_ARRAY);
		}
		return test;
	}

	@Override
	protected Statement methodInvoker(final FrameworkMethod method, final Object test) {
		return new InvokeMethod(method, test) {
			@Override
			public void evaluate() throws Throwable {
				try {
					CloudObjects.invoke(test, method.getMethod(), ArrayUtils.EMPTY_OBJECT_ARRAY);
				} catch (JCloudScaleException e) {
					throw e.getCause();
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}
		};
	}

	@Override
	public void close() {
		if (test != null) {
			CloudObjects.destroy(test);
			test = null;
		}
	}
}
