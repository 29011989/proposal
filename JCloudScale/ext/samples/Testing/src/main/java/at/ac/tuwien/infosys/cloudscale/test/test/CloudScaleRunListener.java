/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.test;

import at.ac.tuwien.infosys.jcloudscale.test.database.TestMonitor;
import at.ac.tuwien.infosys.jcloudscale.test.model.SuiteExecution;
import at.ac.tuwien.infosys.jcloudscale.test.model.TestExecution;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.io.Closer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author Gregor Schauer
 */
public class JCloudScaleRunListener extends RunListener implements AutoCloseable {
	private static final Logger LOGGER = LogManager.getLogger(JCloudScaleRunListener.class);
	private static final JCloudScaleRunListener INSTANCE = new JCloudScaleRunListener();

	Closer closer = Closer.create();
	Map<Description, TestExecution> executionMap = Maps.newIdentityHashMap();
	SuiteExecution suiteExecution;

	public static JCloudScaleRunListener getInstance() {
		return INSTANCE;
	}

	private JCloudScaleRunListener() {
	}

	@Override
	public void testRunStarted(Description description) throws Exception {
		log(description);
		super.testRunStarted(description);
	}

	public void testRunFinished(Result result) throws Exception {
		log(result);
		TestMonitor monitor = new TestMonitor();

		SuiteExecution suite = JCloudScaleRunListener.getInstance().suiteExecution;

		if (suite != null && !executionMap.isEmpty()) {
			Ordering<TestExecution> ordering = Ordering.from(new Comparator<TestExecution>() {
				@Override
				public int compare(TestExecution o1, TestExecution o2) {
					return ComparisonChain.start().compare(o1.start, o2.start).result();
				}
			});

			suite.start = ordering.min(executionMap.values()).start;
			suite.end = ordering.max(executionMap.values()).end;
			suite.tests = executionMap.values();

			monitor.save(suite);
		} else {
			for (TestExecution execution : executionMap.values()) {
				monitor.save(execution);
			}
		}

		LOGGER.info(monitor.list().toString().replace("}, ", "},\n"));
		close();
	}

	@Override
	public void testStarted(Description description) throws Exception {
		log(description);
		executionMap.put(description, new TestExecution(System.currentTimeMillis(), null, description.getClassName(), description.getMethodName()));
	}

	@Override
	public void testFinished(Description description) throws Exception {
		log(description);
		TestExecution execution = executionMap.get(description);
		if (execution.state == null) {
			execution.state = TestExecution.State.SUCCESS;
			execution.end = System.currentTimeMillis();
		}
	}

	@Override
	public void testIgnored(Description description) throws Exception {
		log(description);
		long now = System.currentTimeMillis();
		executionMap.put(description, new TestExecution(now, now, description.getClassName(), description.getMethodName()));
		executionMap.get(description).state = TestExecution.State.IGNORE;
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Test ignored: " + description);
		}
	}

	@Override
	public void testFailure(Failure failure) throws Exception {
		log(failure);
		TestExecution execution = executionMap.get(failure.getDescription());
		execution.state = TestExecution.State.FAILURE;
		execution.end = System.currentTimeMillis();
	}

	private void log(Description obj) {
		LOGGER.info(String.format("%-15s%s", getPhase(), obj));
	}

	private void log(Result obj) {
		LOGGER.info(String.format("%-15s%d", getPhase(), obj.getRunCount()));
	}

	private void log(Failure obj) {
		LOGGER.info(String.format("%-15s%s", getPhase(), obj));
	}

	private String getPhase() {
		return Thread.currentThread().getStackTrace()[3].getMethodName().substring(4);
	}

	public void addRunner(JCloudScaleRunner runner) {
		closer.register(runner);
	}

	@Override
	public void close() throws IOException {
		closer.close();
	}

	@SuppressWarnings("unchecked")
	public synchronized JCloudScaleRunListener attach(RunNotifier notifier) {
		List<RunListener> listeners = (List<RunListener>) ReflectionTestUtils.getField(notifier, "fListeners");
		if (!listeners.contains(this)) {
			notifier.addListener(this);
		}
		return this;
	}
}
