/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.model;

import com.google.common.collect.ComparisonChain;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Gregor Schauer
 */
public class SuiteExecution implements Comparable<SuiteExecution>, Serializable {
	public Long id;
	public Long start;
	public Long end;
	public String className;
	public Collection<TestExecution> tests;

	@Override
	public int compareTo(SuiteExecution o) {
		return ComparisonChain.start().compare(this.id, o.id).compare(this.start, o.start)
				.compare(this.className, o.className).result();
	}

	@Override
	public boolean equals(Object o) {
		return this == o || o != null && getClass() == o.getClass() && compareTo((SuiteExecution) o) == 0;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + start.hashCode();
		result = 31 * result + className.hashCode();
		return result;
	}
}
