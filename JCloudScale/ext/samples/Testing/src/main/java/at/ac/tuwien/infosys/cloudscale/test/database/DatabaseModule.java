/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.database;

import at.ac.tuwien.infosys.jcloudscale.test.database.mapping.SuiteExecutionMapper;
import at.ac.tuwien.infosys.jcloudscale.test.database.mapping.TestExecutionMapper;
import com.google.inject.name.Names;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.mybatis.guice.MyBatisModule;
import org.mybatis.guice.datasource.builtin.PooledDataSourceProvider;
import org.mybatis.guice.datasource.helper.JdbcHelper;

import java.util.Properties;

/**
 * Contains dependencies for storing test execution results into a database.
 *
 * @author Gregor Schauer
 */
public class DatabaseModule extends MyBatisModule {
	@Override
	protected void initialize() {
		Properties myBatisProperties = new Properties();
		myBatisProperties.setProperty("mybatis.environment.id", "test");
		myBatisProperties.setProperty("JDBC.schema", "database/testing");
		myBatisProperties.setProperty("JDBC.username", "sa");
		myBatisProperties.setProperty("JDBC.password", "");
		myBatisProperties.setProperty("JDBC.autoCommit", "false");
		Names.bindProperties(binder(), myBatisProperties);
		
		install(JdbcHelper.HSQLDB_Embedded);
		
		bindDataSourceProviderType(PooledDataSourceProvider.class);
		bindTransactionFactoryType(JdbcTransactionFactory.class);

		addMapperClass(TestExecutionMapper.class);
		addMapperClass(SuiteExecutionMapper.class);
	}
}
