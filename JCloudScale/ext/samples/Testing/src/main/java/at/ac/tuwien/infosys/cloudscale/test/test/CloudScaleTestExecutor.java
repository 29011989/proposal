/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.test;

import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;
import org.junit.runners.model.RunnerBuilder;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.springframework.util.ClassUtils.resolveClassName;

/**
 * @author Gregor Schauer
 */
public class JCloudScaleTestExecutor {
	public static void main(String... args) throws IOException {
		if (args.length == 0) {
			System.out.printf("Usage: %s CLASS...%n", JCloudScaleTestExecutor.class.getSimpleName());
			return;
		}

		List<Class<?>> classes = newArrayList();
		for (String arg : args) {
			Class<?> testClass = resolveClassName(arg, null);
			Suite.SuiteClasses suiteClasses = testClass.getAnnotation(Suite.SuiteClasses.class);
			if (suiteClasses != null) {
				Collections.addAll(classes, suiteClasses.value());
			} else {
				classes.add(testClass);
			}
		}

		JUnitCore core = new JUnitCore();
		Result result = core.run(new DistributedComputer(), classes.toArray(new Class[classes.size()]));

		System.out.println("-------------------------------------------------------");
		System.out.printf("Results:%n%n");
		int failures = countFailures(result);
		System.out.printf("Tests run: %d, Failures: %d, Errors: %d, Skipped: %d%n",
				result.getRunCount(), failures, result.getFailureCount() - failures, result.getIgnoreCount());
		System.out.printf("Total time: %ss%n", DurationFormatUtils.formatDurationHMS(result.getRunTime()));
		System.out.printf("Finished at: %s%n", new Date());
		System.out.println("-------------------------------------------------------");
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		if (result.getFailureCount() > 0) {
			System.out.println("-------------------------------------------------------");
		}

		JCloudScaleClient.closeClient();
	}

	private static int countFailures(Result result) {
		return Collections2.filter(result.getFailures(), new Predicate<Failure>() {
			@Override
			public boolean apply(Failure input) {
				return input.getException() instanceof AssertionError;
			}
		}).size();
	}

	private static class DistributedComputer extends ParallelComputer {
		public DistributedComputer() {
			super(true, false);
		}

		@Override
		protected Runner getRunner(RunnerBuilder builder, Class<?> testClass) throws Throwable {
			return new JCloudScaleRunner(testClass);
		}
	}
}
