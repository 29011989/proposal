package at.ac.tuwien.infosys.jcloudscale.genetics.common;

import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Contains the parameters of a genetic simulation.
 */
public class SimulationParameters implements Serializable {
	private static final long serialVersionUID = 1L;

	double elitismRate = 0.9;
	long maxTime;
	int populationLimit;
	int nucleotides;

	protected SimulationParameters() {
	}

	public SimulationParameters(double elitismRate, long maxTime, int populationLimit, int nucleotides) {
		this.elitismRate = elitismRate;
		this.maxTime = maxTime;
		this.populationLimit = populationLimit;
		this.nucleotides = nucleotides;
	}

	public double getElitismRate() {
		return elitismRate;
	}

	public long getMaxTime() {
		return maxTime;
	}

	public int getPopulationLimit() {
		return populationLimit;
	}

	public int getNucleotides() {
		return nucleotides;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("elitismRate", elitismRate)
				.add("maxTime", maxTime)
				.add("populationLimit", populationLimit)
				.add("nucleotides", nucleotides)
				.toString();
	}
}
