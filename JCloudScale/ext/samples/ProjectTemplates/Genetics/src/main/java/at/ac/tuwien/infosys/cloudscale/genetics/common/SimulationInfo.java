package at.ac.tuwien.infosys.jcloudscale.genetics.common;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Contains information about a particular genetic simulation.
 * @see SimulationParameters
 */
public class SimulationInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum State {
        NEW, RUNNING, FINISHED, FAILED
	}

	UUID id = UUID.randomUUID();
    SimulationParameters parameters;
	State state = State.NEW;
	Date latestStart;

	public SimulationInfo() {
	}

	public SimulationInfo(SimulationParameters parameters, Date latestStart) {
        this.parameters = parameters;
        this.latestStart = latestStart;
    }

	public UUID getId() {
		return id;
	}

	public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getLatestStart() {
        return latestStart;
    }

    public SimulationParameters getParameters() {
        return parameters;
    }
}
