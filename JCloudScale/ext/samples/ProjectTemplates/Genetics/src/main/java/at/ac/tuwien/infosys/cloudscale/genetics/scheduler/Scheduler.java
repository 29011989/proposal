package at.ac.tuwien.infosys.jcloudscale.genetics.scheduler;

import at.ac.tuwien.infosys.jcloudscale.genetics.common.SimulationParameters;
import org.apache.commons.math3.genetics.Chromosome;

import java.util.UUID;

/**
 * Schedules genetic simulations.
 */
public class Scheduler {
	/**
	 * Schedules a simulation for execution in the cloud.
	 *
	 * @param parameters the parameters of the simulation to run
	 * @return the request identifier
	 */
	public UUID schedule(SimulationParameters parameters) {
		return null;
	}
	
	/**
	 * Returns and removes the {@link Chromosome} resulting from running a certain simulation.
	 *
	 * @param id the ID of the simulation request
	 * @return the fittest chromosome or {@code null} if there is no result for the given request
	 */
	public Chromosome getResult(UUID id) {
		return null;
	}
}
