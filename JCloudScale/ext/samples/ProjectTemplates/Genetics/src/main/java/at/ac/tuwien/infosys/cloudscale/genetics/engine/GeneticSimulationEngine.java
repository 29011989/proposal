package at.ac.tuwien.infosys.jcloudscale.genetics.engine;

import at.ac.tuwien.infosys.jcloudscale.genetics.common.NucleotideMutation;
import at.ac.tuwien.infosys.jcloudscale.genetics.common.SimulationParameters;
import at.ac.tuwien.infosys.jcloudscale.genetics.util.GeneticsUtils;
import org.apache.commons.math3.genetics.*;

import java.util.concurrent.TimeUnit;

/**
 * Simulates genetic evolution of {@link Population Populations}.
 */
public class GeneticSimulationEngine {
	/**
     * Runs a simulation using the provided {@link SimulationParameters}.
     *
     * @param parameters the parameters of the simulation.
     * @return the fittest chromosome of the population after the stopping condition is satisfied
     */
	public Chromosome run(SimulationParameters parameters) {
		GeneticAlgorithm ga = new GeneticAlgorithm(
				new OnePointCrossover<Integer>(), 1,
				new NucleotideMutation(), 1,
				new TournamentSelection(parameters.getPopulationLimit())
		);

		FixedElapsedTime condition = new FixedElapsedTime(parameters.getMaxTime(), TimeUnit.SECONDS);
		Population population = GeneticsUtils.createRandomPopulation(
				parameters.getPopulationLimit(), parameters.getNucleotides(), parameters.getElitismRate());
		Chromosome chromosome = ga.evolve(population, condition).getFittestChromosome();
		return chromosome;
	}
}
