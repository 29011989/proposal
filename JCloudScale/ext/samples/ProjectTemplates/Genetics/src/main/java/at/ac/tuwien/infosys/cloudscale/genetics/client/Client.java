package at.ac.tuwien.infosys.jcloudscale.genetics.client;

public class Client {
	// Approximate genetic information of Escherichia coli
	static final int NUCLEOTIDE_BASES = 4650000;
	static final double ELITISM_RATE = 0.9;
	static final int POPULATION_LIMIT = 10;

	public static void main(String... args) throws Exception {
		// TODO: you can use this class as the main entry point for running simulations
	}
}
