package at.ac.tuwien.infosys.jcloudscale.genetics.common;

import at.ac.tuwien.infosys.jcloudscale.genetics.util.GeneticsUtils;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.collect.*;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.genetics.AbstractListChromosome;
import org.apache.commons.math3.genetics.Chromosome;
import org.apache.commons.math3.genetics.GeneticAlgorithm;
import org.apache.commons.math3.genetics.InvalidRepresentationException;
import org.apache.log4j.Logger;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

import static at.ac.tuwien.infosys.jcloudscale.genetics.util.GeneticsUtils.*;

/**
 * {@link Chromosome} represented by a byte vector of {@link GeneticsUtils#A adenine}, {@link GeneticsUtils#T thymine},
 * {@link GeneticsUtils#G guanine} and {@link GeneticsUtils#C cytosine}.
 */
public class NucleotideChromosome extends AbstractListChromosome<Byte> implements Comparable<Chromosome>, Externalizable {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(NucleotideChromosome.class);

	public static NucleotideChromosome create(List<Byte> representation) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Creating new chromosome with size " + representation.size());
		}
		return new NucleotideChromosome(representation);
	}

	public NucleotideChromosome() {
		this(Lists.<Byte>newArrayList());
	}

	protected NucleotideChromosome(List<Byte> representation) {
		super(representation);
	}

	/**
	 * Computes the fitness.<br/>
	 * This is usually very time-consuming, so the value should be cached.
	 * <p/>
	 * The fitness is calculated using the following formula:<br/>
	 * <pre>((max(A, T) / max(G, C)) - 1) x 100</pre>
	 * where A, T, G and C are the number of adenine, thymine, guanine and cytosine nucleotides in this chromosome.
	 *
	 * @return fitness
	 */
	public double fitness() {
		ImmutableMultiset<Byte> multiset = ImmutableMultiset.copyOf(getRepresentation());
		int a = multiset.count(A);
		int t = multiset.count(T);
		int g = multiset.count(G);
		int c = multiset.count(C);
		int at = Ints.max(a, t);
		int gc = Ints.max(g, c);
		return (((double) at / gc) - 1.0) * 100.0;
	}

	@Override
	public NucleotideChromosome newFixedLengthChromosome(List<Byte> chromosomeRepresentation) {
		return new NucleotideChromosome(chromosomeRepresentation);
	}

	@Override
	protected void checkValidity(List<Byte> chromosomeRepresentation) throws InvalidRepresentationException {
		chromosomeRepresentation = chromosomeRepresentation == null ? Lists.<Byte>newArrayList() : chromosomeRepresentation;
		Optional<Byte> found = Iterables.tryFind(chromosomeRepresentation, Predicates.not(Range.closed((byte) 0, (byte) 3)));
		if (found.isPresent()) {
			throw new InvalidRepresentationException(LocalizedFormats.INVALID_BINARY_DIGIT, found.get());
		}
	}

	@Override
	public List<Byte> getRepresentation() {
		// Note that this method just widens the visibility of the inherited one
		return super.getRepresentation();
	}

	/**
	 * Returns a valid representation of a {@link NucleotideChromosome} as a random array of length <code>length</code>.
	 *
	 * @param length length of the array
	 * @return a random byte array of length <code>length</code>
	 */
	public static List<Byte> randomRepresentation(int length) {
		List<Byte> rList = new ArrayList<>(length);
		for (int j = 0; j < length; j++) {
			rList.add((byte) GeneticAlgorithm.getRandomGenerator().nextInt(4));
		}
		return rList;
	}

	@Override
	public int compareTo(Chromosome o) {
		return ComparisonChain.start().compare(this.getFitness(), o.getFitness()).result();
	}

	/**
	 * Returns a string representation of this {@link Chromosome} consisting of a sequence of 'A', 'T', 'G' and 'C'.
	 * <p/>
	 * This is usually very time-consuming, so the value should be cached.
	 *
	 * @return a string representation of the chromosome
	 */
	public String toString() {
		return Joiner.on("").join(Iterables.transform(getRepresentation(), NucleotideRepresentationFunction.getInstance()));
	}

	@Override
	protected boolean isSame(Chromosome another) {
		if (!(another instanceof NucleotideChromosome)) {
			return false;
		}
		NucleotideChromosome anotherNc = (NucleotideChromosome) another;
		if (getLength() != anotherNc.getLength()) {
			return false;
		}

		for (int i = 0; i < getRepresentation().size(); i++) {
			if (!(getRepresentation().get(i).equals(anotherNc.getRepresentation().get(i)))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(getLength());
		out.write(Bytes.toArray(getRepresentation()));
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		int length = in.readInt();
		byte[] data = new byte[length];
		in.readFully(data);
		ReflectionTestUtils.setField(this, "representation", Bytes.asList(data));
	}
}
