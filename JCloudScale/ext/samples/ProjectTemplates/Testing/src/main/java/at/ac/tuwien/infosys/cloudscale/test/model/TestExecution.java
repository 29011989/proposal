package at.ac.tuwien.infosys.jcloudscale.test.model;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

import java.io.Serializable;
import java.util.UUID;

public class TestExecution implements Comparable<TestExecution>, Serializable {
	public static enum State {
		SUCCESS, FAILURE, ERROR, IGNORE
	}
	
	public Long id;
	public Long start;
	public Long end;
	public String className;
	public String methodName;
	public State state;
	public UUID objectId;
	public UUID serverId;

	private TestExecution() {
	}

	public TestExecution(Long start, Long end, String className, String methodName) {
		this.start = start;
		this.end = end;
		this.className = className;
		this.methodName = methodName;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("id", id)
				.add("start", start)
				.add("end", end)
				.add("className", className)
				.add("methodName", methodName)
				.toString();
	}

	@Override
	public int compareTo(TestExecution o) {
		return ComparisonChain.start().compare(this.id, o.id).compare(this.start, o.start)
				.compare(this.className, o.className).compare(this.methodName, o.methodName).result();
	}

	@Override
	public boolean equals(Object o) {
		return this == o || o != null && getClass() == o.getClass() && compareTo((TestExecution) o) == 0;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + start.hashCode();
		result = 31 * result + className.hashCode();
		result = 31 * result + methodName.hashCode();
		return result;
	}
}
