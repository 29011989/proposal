package at.ac.tuwien.infosys.jcloudscale.test.monitor;

import at.ac.tuwien.infosys.jcloudscale.test.model.SuiteExecution;
import at.ac.tuwien.infosys.jcloudscale.test.model.TestExecution;

import java.util.List;
import java.util.UUID;

/**
 * Stores results from test executions and provides query methods for retrieving them on demand.
 * <p/>
 * // TODO: the methods below are just stubs. you do not have to stick to the method signatures.
 */
public class TestMonitor {
	public Long save(SuiteExecution execution) {
		return null;
	}

	public Long save(TestExecution execution) {
		return null;
	}

	public List<TestExecution> list(UUID id) {
		return null;
	}

	public List<TestExecution> list() {
		return null;
	}
}
