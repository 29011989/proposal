package at.ac.tuwien.infosys.jcloudscale.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Contains example tests.
 * 
 * // TODO: you should add more tests i.e., by copying ExampleTest and modifying the duration so that the test classes are executed on several hosts in parallel
 */  
@RunWith(Suite.class)
@Suite.SuiteClasses({
		ExampleTest.class,
	    PiTest.class,
})
public class ExampleSuite {
}
