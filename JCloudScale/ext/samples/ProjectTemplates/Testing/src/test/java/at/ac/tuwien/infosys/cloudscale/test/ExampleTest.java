package at.ac.tuwien.infosys.jcloudscale.test;

import at.ac.tuwien.infosys.jcloudscale.test.util.ScriptManager;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Contains example tests.
 * 
 * TODO: Note that the tests are just small examples of what your implementation should be able to deal with
 */
public class ExampleTest {
	@Test
	public void test1() throws InterruptedException {
		Thread.sleep(1000);
		assertTrue(true);
	}

	@Test
	public void test2() throws InterruptedException {
		Thread.sleep(1000);
		assertTrue(true);
	}

	@Test
	public void test3() throws InterruptedException {
		Thread.sleep(1000);
		assertTrue(true);
	}

	@Ignore
	@Test
	public void testIgnore() {
	}

	@Test
	public void testFailure() {
		// This test should always fail by throwing an AssertionError
		fail();
	}

	@Test
	public void testError() {
		// This test should always fail by throwing an expected exception
		throw new ThreadDeath();
	}

	@Test(expected = IOException.class)
	public void testNotImplemented() throws IOException {
		throw new IOException();
	}

	/**
	 * Uses the {@link ScriptEngine} to check whether the time retrieved using JavaScript is within a certain range.
	 * @throws ScriptException if error occurrs in script.
	 * @throws IOException if an I/O error occurs
	 */
	@Test
	public void testScriptEngine() throws ScriptException, IOException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine scriptEngine = manager.getEngineByExtension("js");

		InputStream script = ScriptManager.load("tests/dateTest.js");

		long before = System.currentTimeMillis();
		Double time = (Double) scriptEngine.eval(IOUtils.toString(script));
		long after = System.currentTimeMillis();

		assertTrue(before <= time.longValue());
		assertTrue(time.longValue() <= after);
	}
}
