/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics.common;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.genetics.Chromosome;
import org.apache.commons.math3.genetics.GeneticAlgorithm;
import org.apache.commons.math3.genetics.MutationPolicy;

import java.util.ArrayList;
import java.util.List;

/**
 * Mutation for {@link NucleotideChromosome NucleotideChromosomes}.
 *
 * @author Gregor Schauer
 */
public class NucleotideMutation implements MutationPolicy {
	/**
	 * Mutate the given chromosome. Randomly changes one gene.
	 *
	 * @param original the original chromosome.
	 * @return the mutated chromosome.
	 * @throws MathIllegalArgumentException if {@code original} is not an instance of {@link NucleotideChromosome}.
	 */
	public Chromosome mutate(Chromosome original) throws MathIllegalArgumentException {
		if (!(original instanceof NucleotideChromosome)) {
			throw new MathIllegalArgumentException(LocalizedFormats.INVALID_BINARY_CHROMOSOME);
		}

		NucleotideChromosome origChrom = (NucleotideChromosome) original;
		List<Byte> newRepr = new ArrayList<>(origChrom.getRepresentation());

		// randomly select a gene
		int geneIndex = GeneticAlgorithm.getRandomGenerator().nextInt(origChrom.getLength());
		// and change it
		newRepr.set(geneIndex, (byte) ((origChrom.getRepresentation().get(geneIndex)+1) % 4));

		return origChrom.newFixedLengthChromosome(newRepr);
	}
}
