/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics.common;

import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Contains the parameters of a genetic simulation.
 *
 * @author Gregor Schauer
 */
public class SimulationParameters implements Serializable {
	private static final long serialVersionUID = 1L;

	double elitismRate = 0.9;
	long maxTime;
	int populationLimit;
	int nucleotides;

	protected SimulationParameters() {
	}

	public SimulationParameters(double elitismRate, long maxTime, int populationLimit, int nucleotides) {
		this.elitismRate = elitismRate;
		this.maxTime = maxTime;
		this.populationLimit = populationLimit;
		this.nucleotides = nucleotides;
	}

	public double getElitismRate() {
		return elitismRate;
	}

	public long getMaxTime() {
		return maxTime;
	}

	public int getPopulationLimit() {
		return populationLimit;
	}

	public int getNucleotides() {
		return nucleotides;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("elitismRate", elitismRate)
				.add("maxTime", maxTime)
				.add("populationLimit", populationLimit)
				.add("nucleotides", nucleotides)
				.toString();
	}
}
