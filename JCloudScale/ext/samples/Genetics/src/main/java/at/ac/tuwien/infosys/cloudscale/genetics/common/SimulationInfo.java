/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics.common;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Contains information about a particular genetic simulation.
 *
 * @author Gregor Schauer
 * @see SimulationParameters
 */
public class SimulationInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum State {
        NEW, RUNNING, FINISHED, FAILED
	}

	UUID id = UUID.randomUUID();
    SimulationParameters parameters;
	State state = State.NEW;
	Date latestStart;

	public SimulationInfo() {
	}

	public SimulationInfo(SimulationParameters parameters, Date latestStart) {
        this.parameters = parameters;
        this.latestStart = latestStart;
    }

	public UUID getId() {
		return id;
	}

	public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getLatestStart() {
        return latestStart;
    }

    public SimulationParameters getParameters() {
        return parameters;
    }
}
