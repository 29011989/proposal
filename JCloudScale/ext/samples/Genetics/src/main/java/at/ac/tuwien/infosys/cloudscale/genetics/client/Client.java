/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics.client;

import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleConfigurationProvider;
import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleShutdown;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.genetics.common.SimulationParameters;
import at.ac.tuwien.infosys.jcloudscale.genetics.scheduler.Scheduler;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.CPUEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.RAMEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.StateEvent;
import at.ac.tuwien.infosys.jcloudscale.policy.IScalingPolicy;
import at.ac.tuwien.infosys.jcloudscale.server.JCloudScaleServerRunner;
import at.ac.tuwien.infosys.jcloudscale.server.aspects.eventing.StateEventAspect;
import at.ac.tuwien.infosys.jcloudscale.vm.*;
import at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalCloudPlatformConfiguration;
import com.google.common.collect.Lists;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.math3.genetics.Chromosome;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * @author Gregor Schauer
 */
public class Client {
	private static final Logger LOGGER = Logger.getLogger(Client.class);
	// Approximate genetic information of Escherichia coli
	static final int NUCLEOTIDE_BASES = 4650000;

	static final int POPULATION_LIMIT = 10;
	static final double ELITISM_RATE = 0.9;
	
	@JCloudScaleShutdown
	public static void main(String... args) throws Exception {
		
		JCloudScaleClient.setConfiguration(JCloudScaleConfiguration.load(new File("config.xml")));
		
		Scheduler scheduler = new Scheduler();

		List<UUID> ids = Lists.newArrayList();
		for (int i = 0; i < 20; i++) {
			UUID id = scheduler.schedule(new SimulationParameters(ELITISM_RATE, 3, POPULATION_LIMIT, NUCLEOTIDE_BASES));
			ids.add(id);
			Thread.sleep(1000);
		}

		while (!ids.isEmpty()) {
			Chromosome result = scheduler.getResult(ids.get(0));
			if (result != null) {
				LOGGER.info(String.format("Got result for %s: %s", ids.remove(0), result.getFitness()));
			} else {
				Thread.sleep(1000);
			}
		}

	}
}
