package at.ac.tuwien.infosys.jcloudscale.genetics.client;

import at.ac.tuwien.infosys.jcloudscale.policy.IScalingPolicy;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudObjectDescriptor;
import at.ac.tuwien.infosys.jcloudscale.vm.IHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHostPool;

public class ScalingPolicy implements IScalingPolicy {
	
	@Override
	public IHost selectHost(CloudObjectDescriptor newCloudObject, IVirtualHostPool hostPool) {
		System.out.println("Selecting");
		for (IHost host : hostPool.getHosts()) {
			// Note that the constant has to be adjusted to the amount of system memory
			if (host.isOnline() && host.getCurrentRAMUsage() > 0 && host.getCurrentRAMUsage() < 0.6) {
				return host;
			}
		}
		return null;
	}

	@Override
	public boolean scaleDown(IHost scaledHost, IVirtualHostPool hostPool) {
		return scaledHost.getCloudObjectsCount() == 0
				&& (System.currentTimeMillis() - scaledHost.getLastRequestTime().getTime()) > 10000;
	}
	
}
