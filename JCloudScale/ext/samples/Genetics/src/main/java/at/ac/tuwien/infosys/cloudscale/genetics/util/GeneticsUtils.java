/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics.util;

import at.ac.tuwien.infosys.jcloudscale.genetics.common.NucleotideChromosome;
import com.google.common.base.Function;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.genetics.Chromosome;
import org.apache.commons.math3.genetics.ElitisticListPopulation;
import org.apache.commons.math3.genetics.InvalidRepresentationException;
import org.apache.commons.math3.genetics.Population;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains utility methods for {@link Chromosome} and {@link Population}.
 *
 * @author Gregor Schauer
 */
public class GeneticsUtils {
    // Nucleotide bases
    public static final byte A = 0;  // adenine
    public static final byte T = 1;  // thymine
    public static final byte G = 2;  // guanine
    public static final byte C = 3;  // cytosine

    private GeneticsUtils() {
    }

    /**
     * Returns the memory consumption for storing the genetic information of single-stranded nucleic acids in megabytes
     * using 1 byte per nucleotide base.
     * <p/>
     * Note that the DNA double helix polymer can be synthesized out of a single-strand most of the time without
     * information loss.
     *
     * @param bases the number of nucleotides.
     * @return the amount of memory in megabytes (1 megabyte conforms to 1048576 bytes)
     */
    public static double memory(int bases) {
        return bases / 1048576D;
    }

    /**
     * Creates a random {@link Population} of the given size of {@link Chromosome Chromosomes}.
     *
     * @param populationLimit the maximal size of the population
     * @param nucleotides     the number of nucleotides of a chromosome
     * @param elitismRate     amount of chromosomes directly transferred to the next generation [in %]
     * @return the population
     */
    public static Population createRandomPopulation(int populationLimit, int nucleotides, double elitismRate) {
        List<Chromosome> popList = new ArrayList<>(populationLimit);
        for (int i = 0; i < populationLimit; i++) {
			popList.add(NucleotideChromosome.create(NucleotideChromosome.randomRepresentation(nucleotides)));
        }
        return new ElitisticListPopulation(popList, popList.size(), elitismRate);
    }

	public static class NucleotideRepresentationFunction implements Function<Byte, Character> {
		private static final NucleotideRepresentationFunction INSTANCE = new NucleotideRepresentationFunction();

		public static NucleotideRepresentationFunction getInstance() {
			return INSTANCE;
		}

		private NucleotideRepresentationFunction() {
		}

		@Override
		public Character apply(Byte input) {
			switch (input) {
				case A: return 'A';
				case T: return 'T';
				case G: return 'G';
				case C: return 'C';
				default: throw new InvalidRepresentationException(LocalizedFormats.INVALID_BINARY_DIGIT, input);
			}
		}
	}
}
