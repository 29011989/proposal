/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics.server;

import at.ac.tuwien.infosys.jcloudscale.annotations.ByValueParameter;
import at.ac.tuwien.infosys.jcloudscale.annotations.CloudObject;
import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleConfigurationProvider;
import at.ac.tuwien.infosys.jcloudscale.annotations.DestructCloudObject;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.genetics.common.NucleotideMutation;
import at.ac.tuwien.infosys.jcloudscale.genetics.common.SimulationParameters;
import at.ac.tuwien.infosys.jcloudscale.genetics.util.GeneticsUtils;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.RAMEvent;
import at.ac.tuwien.infosys.jcloudscale.policy.IScalingPolicy;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudObjectDescriptor;
import at.ac.tuwien.infosys.jcloudscale.vm.ICloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHostPool;
import at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalCloudPlatformConfiguration;

import org.apache.commons.math3.genetics.*;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

/**
 * Simulates genetic evolution of {@link Population Populations}.
 *
 * @author Gregor Schauer
 */
@CloudObject
public class GeneticSimulationEngine {
	static final Logger LOGGER = Logger.getLogger(GeneticSimulationEngine.class);
	
	/**
     * Runs a simulation using the provided {@link SimulationParameters}.
     *
     * @param parameters the parameters of the simulation.
     * @return the fittest chromosome of the population after the stopping condition is satisfied
     */
	@DestructCloudObject
	@ByValueParameter
	public Chromosome run(@ByValueParameter SimulationParameters parameters) {
		GeneticAlgorithm ga = new GeneticAlgorithm(
				new OnePointCrossover<Integer>(), 1,
				new NucleotideMutation(), 1,
				new TournamentSelection(parameters.getPopulationLimit())
		);

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Starting simulation with parameters " + parameters);
		}

		FixedElapsedTime condition = new FixedElapsedTime(parameters.getMaxTime(), TimeUnit.SECONDS);
		Population population = GeneticsUtils.createRandomPopulation(
				parameters.getPopulationLimit(), parameters.getNucleotides(), parameters.getElitismRate());
		Chromosome chromosome = ga.evolve(population, condition).getFittestChromosome();

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Finished simulation " + chromosome.getFitness());
		}
		return chromosome;
	}
}
