/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics.scheduler;

import at.ac.tuwien.infosys.jcloudscale.api.CloudObjects;
import at.ac.tuwien.infosys.jcloudscale.genetics.common.SimulationInfo;
import at.ac.tuwien.infosys.jcloudscale.genetics.common.SimulationParameters;
import at.ac.tuwien.infosys.jcloudscale.genetics.server.GeneticSimulationEngine;
import com.google.common.base.Throwables;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.primitives.Longs;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import org.apache.commons.math3.genetics.Chromosome;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.google.common.util.concurrent.MoreExecutors.getExitingScheduledExecutorService;
import static com.google.common.util.concurrent.MoreExecutors.listeningDecorator;
import static java.util.concurrent.Executors.newScheduledThreadPool;

/**
 * Schedules genetic simulations.
 *
 * @author Gregor Schauer
 */
public class Scheduler {
	private static final Logger LOGGER = Logger.getLogger(Scheduler.class);
	private static final long SLA_MS = 10 * 1000;	// milliseconds until the simulation must be finished

	final BiMap<SimulationParameters, SimulationInfo> queue = HashBiMap.create();
	final SortedSet<SimulationInfo> simulationInfo = Sets.newTreeSet(new Comparator<SimulationInfo>() {
		@Override
		public int compare(SimulationInfo o1, SimulationInfo o2) {
			return Longs.compare(o1.getLatestStart().getTime(), o2.getLatestStart().getTime());
		}
	});
	final Map<UUID, Chromosome> resultMap = Maps.newHashMap();

	final ListeningScheduledExecutorService executorService = listeningDecorator(getExitingScheduledExecutorService(
			(ScheduledThreadPoolExecutor) newScheduledThreadPool(4)));

	public UUID schedule(final SimulationParameters parameters) throws IllegalAccessException {
		Date latestStart = new Date(System.currentTimeMillis() + SLA_MS - parameters.getMaxTime());
		final SimulationInfo simulationInfo = new SimulationInfo(parameters, latestStart);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info(String.format("Scheduling request %s with parameters %s", simulationInfo.getId(), parameters));
		}

		executorService.submit(new Runnable() {
			@Override
			public void run() {
//				try {
//					GeneticSimulationEngine engine = CloudObjects.create(GeneticSimulationEngine.class);
					// TODO: constructor invocation is not intercepted
					 GeneticSimulationEngine engine = new GeneticSimulationEngine();
					Chromosome chromosome = engine.run(parameters);
					resultMap.put(simulationInfo.getId(), chromosome);
//				} catch (ReflectiveOperationException e) {
//					e.printStackTrace();
//				}
			}
		});
		
		return simulationInfo.getId();
	}
	
	/*
	public UUID schedule(final SimulationParameters parameters) throws IllegalAccessException {
		synchronized (queue) {
			Date latestStart = new Date(System.currentTimeMillis() + SLA_MS - parameters.getMaxTime());
			final SimulationInfo simulationInfo = new SimulationInfo(parameters, latestStart);
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(String.format("Scheduling request %s with parameters %s", simulationInfo.getId(), parameters));
			}

			this.queue.put(parameters, simulationInfo);
			this.simulationInfo.add(simulationInfo);

			executorService.schedule(new Runnable() {
				@Override
				public void run() {
					if (simulationInfo.getState() == SimulationInfo.State.NEW) {
						if (LOGGER.isInfoEnabled()) {
							LOGGER.info(String.format("Simulation %s must be started now!", simulationInfo.getId()));
						}
						Scheduler.this.run(simulationInfo);
					} else if (LOGGER.isInfoEnabled()) {
						LOGGER.info(String.format("Simulation %s already started - skipping auto scheduling", simulationInfo.getId()));
					}
				}
			}, latestStart.getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);

			reschedule();

			return simulationInfo.getId();
		}
	}

	private void reschedule() {
		synchronized (queue) {
			LOGGER.info("Rescheduling tasks");
			long now = System.currentTimeMillis();

			for (SimulationInfo simulationInfo : this.simulationInfo) {
				if (simulationInfo.getState() == SimulationInfo.State.NEW) {
					if (simulationInfo.getLatestStart().getTime() < now) {
						run(simulationInfo);
					/*
					} else {
						TODO: run simulation immediately if there is a host that has enough memory
						Collection<IVirtualHost> hosts = CloudManager.getInstance().getHostPool().getHosts();
						for (IVirtualHost host : hosts) {
							if (host.isOnline() && host.getCurrentRAMUsage() < 1) {
								run(simulationInfo);
							}
						}
					* /
					}
				}
			}
		}
	}

	private void run(final SimulationInfo simulationInfo) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info(String.format("Submitting simulation %s for execution", simulationInfo.getId()));
		}
		simulationInfo.setState(SimulationInfo.State.RUNNING);

		ListenableFuture<?> future = executorService.submit(new Runnable() {
			@Override
			public void run() {
				try {
					GeneticSimulationEngine engine = new GeneticSimulationEngine();

					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("Starting remote simulation of " + simulationInfo.getId());
					}
					Chromosome chromosome = engine.run(simulationInfo.getParameters());
					resultMap.put(simulationInfo.getId(), chromosome);

					simulationInfo.setState(SimulationInfo.State.FINISHED);
				} catch (Exception e) {
					simulationInfo.setState(SimulationInfo.State.FAILED);
					LOGGER.warn("Exception occurred during simulation", e);
					throw Throwables.propagate(e);
				}
			}
		});

		future.addListener(new Runnable() {
			@Override
			public void run() {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info(String.format("Simulation %s finished", simulationInfo.getId()));
				}
				reschedule();
			}
		}, executorService);
	}
	*/

	/**
	 * Returns and removes the {@link Chromosome} resulting from running a certain simulation.
	 *
	 * @param id the ID of the simulation request
	 * @return the fittest chromosome or {@code null} if there is no result for the given request
	 */
	public Chromosome getResult(UUID id) {
		return resultMap.remove(id);
	}
}
