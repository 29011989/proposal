/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.genetics;

import at.ac.tuwien.infosys.jcloudscale.genetics.common.SimulationParameters;
import at.ac.tuwien.infosys.jcloudscale.genetics.util.GeneticsUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.math3.genetics.ElitisticListPopulation;
import org.apache.commons.math3.genetics.FixedElapsedTime;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Gregor Schauer
 */
public class SimulationDescriptorTest {
	@SuppressWarnings("unchecked")
	@Test
	public void test() throws Exception {
        SimulationParameters descriptor = new SimulationParameters(0.9, 10, 10, 10);

		byte[] data = SerializationUtils.serialize(descriptor);
		SimulationParameters object = (SimulationParameters) SerializationUtils.deserialize(data);

		assertEquals(descriptor, object);
	}
}
