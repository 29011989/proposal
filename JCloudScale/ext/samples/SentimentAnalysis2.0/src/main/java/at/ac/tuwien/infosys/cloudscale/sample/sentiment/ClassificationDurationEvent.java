package at.ac.tuwien.infosys.jcloudscale.sample.sentiment;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.Event;

public class ClassificationDurationEvent extends Event {
	
	private static final long serialVersionUID = 1L;
	
	private String tweet;
	private long duration;
	private String hostId;
	
	public ClassificationDurationEvent() {} 
	
	public ClassificationDurationEvent(String tweet, long duration, String hostId) {
		this.tweet = tweet;
		this.duration = duration;
		this.hostId = hostId;
	}
	
	public String getTweet() {
		return tweet;
	}
	public void setTweet(String tweet) {
		this.tweet = tweet;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public String getHostId() {
		return hostId;
	}
	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

}
