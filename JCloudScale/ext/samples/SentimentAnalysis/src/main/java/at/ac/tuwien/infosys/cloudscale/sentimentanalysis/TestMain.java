/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.sentimentanalysis;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;

import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.scheduler.IScheduler;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.scheduler.LinearScheduler;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.SentimentResult;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.twitter.ThreadTwitterSentimentAnalyzer;

import classifier.ClassifierBuilder;
import classifier.IClassifier;
import classifier.Item;
import classifier.WeightedMajority;
import classifier.WekaClassifier;
import twitter4j.*;
import util.Options;

public class TestMain 
{
	private static final String taskFile = "task.txt";
	private static AtomicLong waitingResults = new AtomicLong(0);
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception 
	{
		System.out.println("HELLo World!");
		
		//sentimental1();

		//sentimental2();
		
		//twitterTest();

		//localCallbackTest();
		
		runnerTest();
		
		System.out.println("Done.");
	}

	private static void runnerTest() throws InterruptedException 
	{
		//
		// reading tasks
		//
		List<String> tasks = readTasks(taskFile);
		System.out.println(String.format("%1$s tasks readed.", tasks.size()));
		waitingResults.set(tasks.size());
		
		//
		// scheduling tasks.
		//
		List<ThreadTwitterSentimentAnalyzer> runningTasks = new ArrayList<ThreadTwitterSentimentAnalyzer>(tasks.size());
		for(String task : tasks)
			runningTasks.add(new ThreadTwitterSentimentAnalyzer(task));
		
		System.out.println(String.format("%1$s tasks created. Starting...", runningTasks.size()));
		
		//
		// running tasks.
		//
		for(ThreadTwitterSentimentAnalyzer runner : runningTasks)
			runner.startAsync();
		
		System.out.println(String.format("%1$s tasks started.", runningTasks.size()));
		
		//
		// waiting for results
		//
		System.out.println("Waiting for results... Press enter to exit.");
		Scanner readkey = new Scanner(System.in);
		
		List<ThreadTwitterSentimentAnalyzer> completedTasks = new ArrayList<ThreadTwitterSentimentAnalyzer>();
		while(runningTasks.size() > 0 )
		{
			completedTasks.clear();
			for(ThreadTwitterSentimentAnalyzer runner : runningTasks)
				if(runner.isReady())
				{
					System.out.println("Results received: "+ runner.getResult().toString());
					completedTasks.add(runner);
				}
			
			if(completedTasks.size() > 0)
			{
				for(ThreadTwitterSentimentAnalyzer runner : completedTasks)
					runningTasks.remove(runner);
				
				System.out.println(String.format("Still waiting for %1$s tasks...", runningTasks.size()));
			}
			
			Thread.sleep(500);
		}
	}

	private static void localCallbackTest() throws InterruptedException 
	{
		// reading tasks
		List<String> tasks = readTasks(taskFile);
		System.out.println(String.format("%1$s tasks readed.", tasks.size()));
		waitingResults.set(tasks.size());
		
		// scheduling and running tasks.
		IScheduler scheduler = new LinearScheduler(); // new ThreadPoolScheduler(); //
		
		try
		{
			SentimentResultCallback callback = new SentimentResultCallback();
			
//			for(String task : tasks)
//				scheduler.schedule(new TwitterSentimentAnalyzer(task));
			
		}
		finally
		{
			scheduler.dispose();
		}
		
		// waiting for results
		System.out.println("Waiting for results... Press enter to exit.");
		Scanner readkey = new Scanner(System.in);
		
		while(waitingResults.get() > 0 && !readkey.hasNextLine())
			Thread.sleep(300);
	}

	private static List<String> readTasks(String taskfile) 
	{
		List<String> res = new ArrayList<String>();
		try
		{
			Scanner scanner = new Scanner(new FileReader(taskFile));
			try
			{
				while(scanner.hasNext() && res.size() < 10)
					res.add(scanner.nextLine());
			}
			finally
			{
				scanner.close();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return res;
	}
	
	//-------------------------------------------------------------------------------------

	private static class SentimentResultCallback implements ISentimentResultCallback
	{
		@Override
		public void resultsRetrieved(SentimentResult result) 
		{
			System.out.println("Results received: "+ result.toString());
			waitingResults.decrementAndGet();
		}
	}
	
	//-----------------------------------------TEST CODE -----------------------------------------------------
	
	private static void twitterTest() throws TwitterException 
	{
		Twitter twitter = new TwitterFactory().getInstance();
	    Query query = new Query("windows :(");
	    QueryResult result = twitter.search(query);
	    
	    List<Tweet> tweets = result.getTweets(); 
	    for (Tweet tweet : tweets) 
	        System.out.println(tweet.getFromUser() + ":" + tweet.getText());
	    
	    System.out.println("Tweets: "+result.getTweets().size());
	}
	
//	private static void sentimental1() throws Exception
//	{
//		ClassifierBuilder clb = new ClassifierBuilder();
//		Options opt = new Options();
//		clb.setOpt(opt);
//	
//		opt.setSelectedFeaturesByFrequency(true);
//		//seleziona solamente 150 termini
//		opt.setNumFeatures(150);
//		//rimuove le emoticons
//		opt.setRemoveEmoticons(true);
//		//prepara le strutture dati per il train e il test
//		clb.prepareTrain();
//		clb.prepareTest();
//		//classificatore Weka
//		NaiveBayes nb = new NaiveBayes();
////		//costruzione e memorizzazione su disco del classificatore
//		WekaClassifier wc = clb.constructClassifier(nb);
//		//WekaClassifier wc = clb.retrieveClassifier("weka.classifiers.functions.VotedPerceptron");
//		//classificazione di un tweet
//		String item = wc.classify("Windows is very scary.");
//		//stampa la classificazione data al tweet dal classificatore
//		System.out.println(item);
//	}
	
	private static void sentimental2() throws Exception
	{
		List<IClassifier> classifiers = new LinkedList<IClassifier>();
		ClassifierBuilder cb = new ClassifierBuilder();
		//prende tre classificatori gi costruiti
		WekaClassifier wc1 = cb.retrieveClassifier("weka.classifiers.bayes.NaiveBayes");
		WekaClassifier wc2 = cb.retrieveClassifier("weka.classifiers.trees.J48");
		WekaClassifier wc3 = cb.retrieveClassifier("weka.classifiers.functions.VotedPerceptron");
		classifiers.add(wc1);
		classifiers.add(wc2);
		classifiers.add(wc3);
		WeightedMajority wm  = new WeightedMajority(classifiers);
		//costruisce e classifica un tweet
		Item item = wm.weightedClassify("Windows is very scary.");
		//stampa la classificazione data al tweet dal classificatore
		System.out.println(item.getPolarity());
		//imposta la classificazione reale del tweet
		item.setTarget("4");
		//comunica al classificatore weighted majority la polarit esatta del tweet
		wm.setTarget(item);
		System.out.println(wm.get_cl2weight().get(1) + " " + wm.get_cl2weight().get(2) + " " + wm.get_cl2weight().get(3));
	}

}
