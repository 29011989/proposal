/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.twitter;

import at.ac.tuwien.infosys.jcloudscale.annotations.CloudObject;
import at.ac.tuwien.infosys.jcloudscale.annotations.DestructCloudObject;
import at.ac.tuwien.infosys.jcloudscale.policy.IScalingPolicy;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.Main;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.Classifier;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.ClassifierFactory;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.SentimentResult;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudObjectDescriptor;
import at.ac.tuwien.infosys.jcloudscale.vm.IHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHostPool;


@CloudObject
public class ThreadTwitterSentimentAnalyzer implements Runnable
{
	private Thread workerThread;
	private String query;
	private volatile SentimentResult result = null;
	
	//------------------------------------------------
	
	public ThreadTwitterSentimentAnalyzer(String query)
	{
		this.query = query;
	}
	
	//------------------------------------------------
	
	public void startAsync()
	{
		if(workerThread != null)
			throw new RuntimeException("The thread is already running!");
		
		workerThread = new Thread(this);
		workerThread.setName(query);
		
		workerThread.start();
	}
	
	public boolean isReady()
	{
		return result != null;
	}
	
	@DestructCloudObject
	public SentimentResult getResult()
	{
		return result;
	}

	@DestructCloudObject
	public void abort()
	{
		if(workerThread != null)
		{
			workerThread.stop();
			workerThread = null;
		}
	}
	
	@Override
	public void run() 
	{
		Classifier classifier = ClassifierFactory.createClassifier();
		//Classifier classifier = ClassifierFactory.getSingletonInstance();
		
		result = TwitterSentimentAnalyzer.analyzeTweets(query, new TwitterQueryWrapper(), classifier);
	}
	
	//-------------------------------------------------------------
	
	public static class JCloudScalePolicy implements IScalingPolicy
	{
		@Override
		public IHost selectHost(CloudObjectDescriptor newCloudObject, IVirtualHostPool hostPool) 
		{
			//selecting least loaded host or start new one if none exist.
			IHost selectedHost = null;
			int managedObjects = Integer.MAX_VALUE;
			
			int hostsCount = hostPool.getHostsCount();
			System.out.println("SCALING POLICY: OFFERED A CHOICE BETWEEN "+hostsCount+" HOSTS.");
			
			if(hostsCount < Main.HOSTS_COUNT)
			{
				System.out.println("SCALING POLICY: NOT ALL STATIC HOSTS ARE USED, ASKING FOR NEW ONE.");
				return null;
			}
			
			for(IHost h: hostPool.getHosts())
			{
				int objectsCount = h.getCloudObjectsCount();
				System.out.println("SCALING POLICY: HOST "+h.getId()+" HAS "+objectsCount+" OBJECTS");
				
				if(objectsCount < managedObjects)
				{
					selectedHost = h;
					managedObjects = objectsCount;
				}
			}
			
			System.out.println("SCALING POLICY: SELECTED "+ (selectedHost != null ? selectedHost.getId() : "NULL"));
			
			return selectedHost;
		}

		@Override
		public boolean scaleDown(IHost scaledHost, IVirtualHostPool hostPool) 
		{
			return false;
		}
		
	}
	
}
