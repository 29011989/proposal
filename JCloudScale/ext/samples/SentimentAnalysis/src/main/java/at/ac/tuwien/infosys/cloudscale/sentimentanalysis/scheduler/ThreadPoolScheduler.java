/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.scheduler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolScheduler implements IScheduler 
{
	private ExecutorService threadPool = Executors.newCachedThreadPool();
	
	//-------------------------------------
	
	@Override
	public void schedule(Runnable task) 
	{
		threadPool.execute(task);
	}

	@Override
	public void dispose() 
	{
		threadPool.shutdown();
	}

}
