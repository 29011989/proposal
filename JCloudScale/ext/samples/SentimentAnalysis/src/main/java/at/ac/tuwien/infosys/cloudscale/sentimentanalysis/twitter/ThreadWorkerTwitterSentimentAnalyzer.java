/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.twitter;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.Classifier;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.ClassifierFactory;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.SentimentResult;

public class ThreadWorkerTwitterSentimentAnalyzer implements Runnable 
{
	private static AtomicLong nextId = new AtomicLong(0);
	
	private Thread thread;
	private BlockingQueue<String> taskQueue = null;
	private BlockingQueue<SentimentResult> resultQueue = null;
	private long threadId;
	
	//------------------------------------------------------
	
	public ThreadWorkerTwitterSentimentAnalyzer(BlockingQueue<String> taskQueue, BlockingQueue<SentimentResult> resultQueue)
	{
		this.taskQueue = taskQueue;
		this.resultQueue = resultQueue;
		
		threadId = nextId.incrementAndGet();
	}
	
	//-----------------------------------------------------
	
	public void start()
	{
		if(thread != null)
			return;
		
		thread = new Thread(this);
		thread.setName("Worker Thread " + threadId);
		thread.start();
	}
	
	public void stop()
	{
		if(thread != null)
			thread.interrupt();
		
		thread = null;
	}
	
	//-----------------------------------------------------
	
	@Override
	public void run() 
	{
		try
		{
			TwitterQueryWrapper twitter = new TwitterQueryWrapper();
			Classifier classifier = ClassifierFactory.createClassifier();
			
			while(true)
			{
				try
				{
					String task = taskQueue.take();
					
					resultQueue.put(TwitterSentimentAnalyzer.analyzeTweets(task, twitter, classifier));
				}
				catch(InterruptedException ex)
				{
					throw ex;
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		}
		catch(InterruptedException ex)
		{
		}
	}
}
