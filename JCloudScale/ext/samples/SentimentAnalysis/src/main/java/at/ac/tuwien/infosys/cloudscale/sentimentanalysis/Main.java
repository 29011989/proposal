/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.sentimentanalysis;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;

import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleConfigurationProvider;
import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleShutdown;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.sentiment.SentimentResult;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.twitter.ThreadTwitterSentimentAnalyzer;
import at.ac.tuwien.infosys.jcloudscale.sentimentanalysis.twitter.ThreadWorkerTwitterSentimentAnalyzer;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;

public class Main 
{
	private static final String taskFile = "task.txt";
//	private static final String resultFile = "res.csv";
	public static final int HOSTS_COUNT = 5;
	
	@JCloudScaleConfigurationProvider
	public static JCloudScaleConfiguration getConfiguration()
	{
		JCloudScaleConfiguration cfg = JCloudScaleConfigurationBuilder
		.createLocalConfigurationBuilder(new ThreadTwitterSentimentAnalyzer.JCloudScalePolicy(), 
				Level.INFO,
				"server", "jcloudscale.core-0.0.1.jar;*"
				)
//				.withServerLoggingLevel(Level.ALL)
				.withMonitoring(false)
				.build();
//				.createInfosysOpenStackConfigurationBuilder("128.130.172.197")
//				.withGlobalLoggingLevel(Level.INFO)
//				.build();
		
//		cfg.server().setRequestQueueName("RST_REQUEST");
//		cfg.server().setResponseQueueName("RST_RESPONSE");
//		cfg.server().setInitializationQueueName("RST_INIT");
//		cfg.server().logging().setLoggingQueueName("RST_LOGGING");
//		cfg.server().logging().setOutputRedirectQueue("RST_OUTPUT_REDIRECT");
//		cfg.common().monitoring().setQueueName("RST_MONITORING");
		//cfg.common().communication().setRequestTimeout(1000);
//		cfg.common().communication().setMessageSizeLimit(4000000);
		
//		((CachingClassLoaderConfiguration)cfg.common().classLoader()).setCacheType(CacheType.RiakCache, true);
		return cfg;
	}
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws InterruptedException, IOException 
	{
		System.out.println("Starting operation sequence.");
		
		int tasksCount = 100;
		
		JCloudScaleClient.setConfiguration(getConfiguration());
		
		try
		{
//			cleanup(queueAddress, cacheType, riakAddress);
			
			//
			// reading tasks
			//
			List<String> tasks = readTasks(taskFile, tasksCount);
			System.out.println(String.format("%1$s tasks readed.", tasks.size()));
			
			long start = System.nanoTime();
			
			threadedTest(tasks);
			
			//workerTest(tasks, 3);
			
			long timeMs = (System.nanoTime() - start)/1000000;
			System.out.println(String.format("Processing took %1$s ms", timeMs));
//			SaveResults(queueAddress, cacheType, fileCollector, tasksCount, timeMs);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			System.out.println("We're Done.");
//			Thread.sleep(2000);//time to finish all bg. activities.
//			System.exit(0);
			shutdown();
		}
	}	

	@JCloudScaleShutdown
	private static void shutdown() 
	{
		
	}

	//----------------------------------------------------------------
	
//	private static void SaveResults(String queueAddress, CacheType cacheType, FileCollectorAbstract fileCollector, int tasksCount, long timeMs) throws IOException 
//	{
//		boolean newFile = !new File(resultFile).exists();
//		
//		try(FileWriter writer = new FileWriter(resultFile, true))
//		{
//			if(newFile)
//				writer.write("EXPERIMENT TIME, LOCATION, FILE COLLECTOR, CACHE TYPE, HOSTS, TASKS, DURATION"+ System.lineSeparator());
//			
//			String line = String.format("%s, %s, %s, %s, %s, %s, %s" + System.lineSeparator(), 
//								new SimpleDateFormat("yyy.mm.dd HH:MM").format(new Date(System.currentTimeMillis())),
//								queueAddress.trim().toLowerCase().equals("localhost") ? "LOCAL" : queueAddress,
//								fileCollector.getClass().getSimpleName(),
//								cacheType.toString(),
//								HOSTS_COUNT,
//								tasksCount,
//								timeMs);
//			writer.write(line);
//		}
//		
//	}
	
	private static void threadedTest(List<String> tasks) throws InterruptedException 
	{
		//
		// scheduling tasks.
		//
		System.out.print("Creating tasks ");
		List<ThreadTwitterSentimentAnalyzer> runningTasks = new ArrayList<ThreadTwitterSentimentAnalyzer>(tasks.size());
		for(String task : tasks)
		{
			runningTasks.add(new ThreadTwitterSentimentAnalyzer(task));//new ThreadPoolTwitterSentimentAnalyzer(task));//
			System.out.print(".");
		}
		System.out.println();
		System.out.println(String.format("%1$s tasks created. Starting...", runningTasks.size()));
		
		//
		// running tasks.
		//
		for(ThreadTwitterSentimentAnalyzer runner : runningTasks)
			runner.startAsync();
		
		System.out.println(String.format("%1$s tasks started.", runningTasks.size()));
		
		//
		// waiting for results
		//
		System.out.println("Waiting for results...");
		
		List<ThreadTwitterSentimentAnalyzer> completedTasks = new ArrayList<ThreadTwitterSentimentAnalyzer>();
		while(runningTasks.size() > 0 )
		{
			Thread.sleep(1000);
			
			completedTasks.clear();
			for(ThreadTwitterSentimentAnalyzer runner : runningTasks)
				if(runner.isReady())
				{
					System.out.println("Result received: "+ runner.getResult().toString());
					completedTasks.add(runner);
				}
			
			if(completedTasks.size() > 0)
			{
				for(ThreadTwitterSentimentAnalyzer runner : completedTasks)
					runningTasks.remove(runner);
				
				System.out.println(String.format("Still waiting for %1$s tasks...", runningTasks.size()));
			}
			
//			if(readkey.hasNextLine())
//			{
//				//TODO: send to the cloud "abort": @DestructCloudObject?
//				for(ThreadTwitterSentimentAnalyzer task : runningTasks)
//					task.abort();
//			}
		}
	}
	
	private static void workerTest(List<String> tasks, int workerCount) throws InterruptedException
	{
		System.out.println("Creating workers...");
		
		//
		// Creating queues
		//
		BlockingQueue<String> taskQueue = new LinkedBlockingQueue<String>();
		BlockingQueue<SentimentResult> resultQueue = new LinkedBlockingQueue<SentimentResult>();
		
		//
		// Creating workers.
		//
		List<ThreadWorkerTwitterSentimentAnalyzer> workers = new ArrayList<ThreadWorkerTwitterSentimentAnalyzer>();
		for(int i=0;i<workerCount;++i)
			workers.add(new ThreadWorkerTwitterSentimentAnalyzer(taskQueue, resultQueue));
		
		//
		// Starting workers
		//
		for(ThreadWorkerTwitterSentimentAnalyzer worker : workers)
			worker.start();
		
		//
		// Sending tasks.
		//
		System.out.println("Sending tasks...");
		for(String task :tasks)
			taskQueue.put(task);
		
		//
		// Reading results.
		//
		System.out.println("Waiting results...");
		int results = tasks.size();
		while(results-- > 0)
		{
			System.out.println("Results received: "+ resultQueue.take().toString());
		}
		
		//
		// Shutting down workers.
		//
		System.out.println("Shutting down workers...");
		for(ThreadWorkerTwitterSentimentAnalyzer worker : workers)
			worker.stop();
	}
	//----------------------------------------------------------------
	
	private static List<String> readTasks(String taskfile, int maxTasksToRead) 
	{
		List<String> res = new ArrayList<String>();
		try
		{
//			System.out.println(new File(taskFile).getCanonicalPath());
			Scanner scanner = new Scanner(new FileReader(taskFile));
			try
			{
				while(scanner.hasNext() && res.size() < maxTasksToRead)
					res.add(scanner.nextLine());
			}
			finally
			{
				scanner.close();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return res;
	}

}
