import at.ac.tuwien.infosys.jcloudscale.annotations.*;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.management.CloudManager;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Random;
import java.util.UUID;

@CloudObject
public class CO {

    @DataSource(name = "riak")
    private Datastore riak;

    @CloudObjectId
    private UUID coId;

    public String saveSomeStuff(String hostId) throws InterruptedException {

        MyComplexResult result = new MyComplexResult();
        result.setValue(String.valueOf(new Random().nextInt()));

        MyComplexResult subresult = new MyComplexResult();
        subresult.setValue(String.valueOf(new Random().nextInt()));

        result.setResult(subresult);

        // riak.save(result);


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("cloud");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Report report = new Report();
        report.setName("Report " + result.getUuid());
        report.setResult(result);
        report.setHostId(hostId);
        entityManager.persist(report);
        entityManager.getTransaction().commit();

        // Thread.sleep(new Random().nextInt(2000));

        System.out.println("######### Returning: "+result.getUuid());
        return result.getUuid();
    }

    public String getId() {
        return coId.toString();
    }

    public class MyComplexResult {

        @DatastoreId
        private String uuid;

        private MyComplexResult result;

        private String value;

        public String getUuid() {
            return uuid;
        }


        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public MyComplexResult getResult() {
            return result;
        }

        public void setResult(MyComplexResult result) {
            this.result = result;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("uuid", uuid)
                    .append("result", result)
                    .append("value", value).toString();
        }

    }
}