import at.ac.tuwien.infosys.jcloudscale.annotations.Datastore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Report {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String hostId;

    @Transient
    @Datastore(value = "riak")
    private transient CO.MyComplexResult result;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Transient
    public CO.MyComplexResult getResult() {
        return result;
    }

    public void setResult(CO.MyComplexResult result) {
        this.result = result;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }
}
