import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleShutdown;
import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.core.DatastoreFactory;
import at.ac.tuwien.infosys.jcloudscale.management.CloudManager;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.lang.Override;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class Main {

    @DataSource(name = "riak")
    private static Datastore riak;

    private static EntityManagerFactory entityManagerFactory;

    private static List<String> ids = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {

        JCloudScaleConfiguration config = JCloudScaleConfigurationBuilder
                .createLocalConfigurationBuilder()
                .build();
        config.common().clientLogging().setDefaultLoggingLevel(Level.INFO);
        config.server().logging().setDefaultLoggingLevel(Level.INFO);
        JCloudScaleClient.setConfiguration(config);

        Thread[] threads = new Thread[5];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Main.ThreadStarter();
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) {
            threads[i].join();
        }


        //entityManagerFactory = Persistence.createEntityManagerFactory("cloud");
        //EntityManager entityManager = entityManagerFactory.createEntityManager();

        //entityManager.getTransaction().begin();
        for (String id : ids) {
            // System.out.println(id);
            CO.MyComplexResult result = riak.find(CO.MyComplexResult.class, id);
            // System.out.println(result);
        }

        shutdown();
    }

    @JCloudScaleShutdown
    public static void shutdown() {

    }

    static class ThreadStarter extends Thread {

        @Override
        public void run() {
            CO co = new CO();
            try {
                UUID hostId = CloudManager.getInstance().getHost(UUID.fromString(co.getId())).getId();
                String id = co.saveSomeStuff(hostId.toString());
                ids.add(id);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


        }


    }

}