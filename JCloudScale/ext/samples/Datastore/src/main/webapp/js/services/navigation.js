webstoreApp.factory('navigation', function() {
    return new function() {
        //known pages per role
        var known_pages = {
            "ADMIN" : [{"name":"Products", "link":"products"}, {"name":"Cart", "link":"order"}, {"name":"My Account", "link":"account"}],
            "CUSTOMER" : [{"name":"Products", "link":"products"}, {"name":"Cart", "link":"order"}, {"name":"My Account", "link":"account"}]
        };
        var navigation = {
            firstPage: function(role) {
                return known_pages[role][0];
            },
            pages: function(role) {
                return known_pages[role];
            }
        };
        return navigation;
    };
});