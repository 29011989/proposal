/**
 * The navigation controller
 */
webstoreApp.controller('NavCtrl', function($scope, navigation) {
    //load the current user
    $scope.currentUser();

    $scope.getPages = function(roleName) {
        return navigation.pages(roleName);
    };
});