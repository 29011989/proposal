webstoreApp.factory('Product', ['$resource',
    function($resource) {
        return $resource('/cloudscale.datastore/services/product/:id', {id: '@id'});
    }
]);

webstoreApp.factory('MultiProductLoader', ['Product', '$q',
    function (Product, $q) {
        return function () {
            var delay = $q.defer();
            Product.query(function (products) {
                delay.resolve(products);
            }, function () {
                delay.reject('Unable to fetch products');
            });
            return delay.promise;
        };
    }
]);

webstoreApp.factory('ProductLoader', ['Product', '$route', '$q',
    function(Product, $route, $q) {
        return function() {
            var delay = $q.defer();
            Product.get({id: $route.current.params.id}, function(product) {
                delay.resolve(product);
            }, function() {
                delay.reject('Unable to fetch product '  + $route.current.params.id);
            });
            return delay.promise;
        };
    }
]);