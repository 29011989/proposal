/**
 * The login controller
 */
webstoreApp.controller('LoginCtrl', function($rootScope, $scope, $http) {

    /**
     * Function to submit credentials
     */
    $scope.login = function() {
        var config = {params: {username : $scope.credentials.username,
                               password: $scope.credentials.password}};
        $http.post('/cloudscale.datastore/login', {}, config).success(function(data) {
            $rootScope.$broadcast('login-successful');
            $scope.credentials.username = "";
            $scope.credentials.password = "";
        });
    };
});