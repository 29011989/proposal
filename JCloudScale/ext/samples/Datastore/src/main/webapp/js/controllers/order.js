/**
 * The order view controller
 */
webstoreApp.controller('OrderViewCtrl', ['$scope', 'order', 'Order', '$http',
    function($scope, order, Order, $http) {
        //bind available order
        $scope.order = order;
        $scope.sum = getSum();

        //delete order item
        $scope.deleteItem = function(orderID, productID) {
            Order.delete(orderID, productID);
        };

        $scope.migrate = function() {
            var url = '/cloudscale.datastore/services/migration';
            return $http.post(url);
        };

        $scope.pay = function() {
            var url = '/cloudscale.datastore/services/payment';
            return $http.post(url);
        };

        function getSum() {
            var sum = 0;
            angular.forEach($scope.order.orderItems, function(orderItem) {
                sum = sum + (orderItem.product.price * orderItem.count);
            });
            return sum;
        }
    }
]);