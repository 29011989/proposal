/**
 * Setup angularJSs
 */

var webstoreApp = angular.module('webstoreApp', ['ngResource'])
    .config(['$routeProvider','$httpProvider', function($routeProvider,$httpProvider) {

        /**
         * Configure Routes
         */
        $routeProvider
            .when('/products', {
                templateUrl: 'views/products.html',
                controller: 'ProdListCtrl',
                resolve: {
                    products: function(MultiProductLoader) {
                        return MultiProductLoader();
                    }
                }
            })
            .when('/product/new', {
                templateUrl:'views/productForm.html',
                controller: 'ProdNewCtrl'
            }).
            when('/product/:id', {
                templateUrl: 'views/product.html',
                controller: 'ProdViewCtrl',
                resolve: {
                    product: function(ProductLoader) {
                        return ProductLoader();
                    },
                    account: function(AccountLoader) {
                        return AccountLoader();
                    }
                }
            }).
            when('/product/edit/:id', {
                templateUrl:'views/productForm.html',
                controller: 'ProdEditCtrl',
                resolve: {
                    product: function(ProductLoader) {
                        return ProductLoader();
                    }
                }
            })
            .when('/account', {
                templateUrl: 'views/account.html',
                controller: 'AccountCtrl',
                resolve: {
                    account: function(AccountLoader) {
                        return AccountLoader();
                    }
                }
            })
            .when('/order', {
                templateUrl: 'views/order.html',
                controller: 'OrderViewCtrl',
                resolve: {
                    order: function(OrderLoader) {
                        return OrderLoader();
                    }
                }
            })
            .otherwise({
                redirectTo: '/products'
            });

        /**
         * Add authentication interceptor
         */
        $httpProvider.responseInterceptors.push('authenticationInterceptor');
    }]);