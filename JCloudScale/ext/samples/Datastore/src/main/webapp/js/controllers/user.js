/**
 * The account view controller
 */
webstoreApp.controller('AccountCtrl', ['$scope', 'account',
    function($scope, account) {
        $scope.account = account;
    }
]);