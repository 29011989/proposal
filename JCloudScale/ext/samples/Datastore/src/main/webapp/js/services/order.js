webstoreApp.factory('Order', ['$http', function($http) {
    var baseUrl = '/cloudscale.datastore/services/order';
    return {
        query: function(success, error) {
            return $http.get(baseUrl).success(success).error(error);
        },
        add: function(order, success) {
            return $http.post(baseUrl, order).success(success);
        },
        delete: function(orderID, prodctID){
            var url = baseUrl + "/" + orderID + "/" + prodctID;
            return $http.delete(url);
        }
    }
}]);

webstoreApp.factory('OrderLoader', ['Order', '$q',
    function (Order, $q) {
        return function () {
            var delay = $q.defer();
            Order.query(function (order) {
                delay.resolve(order);
            }, function () {
                delay.reject('Unable to fetch order');
            });
            return delay.promise;
        };
    }
]);

