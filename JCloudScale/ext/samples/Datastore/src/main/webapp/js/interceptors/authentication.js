/**
 * Authentication Interceptor
 */
webstoreApp.factory('authenticationInterceptor', function($rootScope, $q) {

    /**
     * Request Success Callback
     *
     * @param response the server response
     */
    function success(response) {
        return response;
    }

    /**
     * Request Error Callback
     *
     * @param response the server response
     */
    function error(response) {
        if(response.status === 400) {
            var deferred = $q.defer();
            $rootScope.$broadcast('login-required');
            return deferred.promise;
        }
        return $q.reject(response);
    }

    return function(promise) {
        return promise.then(success, error);
    }
});