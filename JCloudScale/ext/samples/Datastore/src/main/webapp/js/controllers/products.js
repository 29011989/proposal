/**
 * The product list controller
 */
webstoreApp.controller('ProdListCtrl', ['$scope', 'products', 'Product',
    function($scope, products,Product) {
        //bind available products
        $scope.products = products;

        //function to delete a product
        $scope.deleteProduct = function(id) {
            Product.delete({id: id});
            $scope.products = deleteFromScope(id);
        };

        function deleteFromScope(id) {
            var result = [];
            angular.forEach($scope.products, function(product) {
                if(product.id !== id) {
                    result.push(product);
                }
            });
            return result;
        }
    }
]);

/**
 * The new product controller
 */
webstoreApp.controller('ProdNewCtrl', ['$scope', '$location', 'Product',
    function($scope, $location, Product) {
        $scope.product = new Product();
        $scope.product.properties = [];;
        $scope.propertyName = "";

        $scope.save = function() {
            $scope.product.$save(function(product) {
                $location.path('/product/' + product.id);
            });
        };

        $scope.addProperty = function() {
           $scope.product.properties.push({"name":$scope.propertyName, "value":""})
           $scope.propertyName = "";
        };
    }
]);

/**
 * The product view controller
 */
webstoreApp.controller('ProdViewCtrl', ['$scope', '$location', 'product', 'account', 'Order',
    function($scope, $location, product, account, Order) {
        $scope.product = product;
        $scope.account = account;

        $scope.buy = function() {
            var orderItem = {};
            orderItem.product = product;
            orderItem.count = 1;

            var data = {};
            data.uid = account.username;
            data.orderItems = [orderItem];
            Order.add(data, function() {
                $location.path('/order');
            });
        };
    }
]);

/**
 * The product edit controller
 */
webstoreApp.controller('ProdEditCtrl', ['$scope', '$location', 'product',
    function($scope, $location, product) {
        $scope.product = product;

        $scope.save = function() {
            $scope.product.$save(function(product) {
                $location.path('/product/' + product.id);
            });
        };
    }
]);
