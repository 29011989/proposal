/**
 * The main controller
 */
webstoreApp.controller('MainCtrl', function($rootScope, $scope, $http, $route) {

    $scope.authenticated = true;

    /**
     * Login required callback (used to display login dialog)
     */
    $scope.$on('login-required', function() {
        $scope.authenticated = false;
    });

    /**
     * Login successful callback (used to hide login dialog)
     */
    $scope.$on("login-successful", function() {
        $scope.authenticated = true;
        $scope.currentUser();
        $route.reload();
    });

    /**
     * Logout function
     */
    $scope.logout = function() {
        $http.get('/cloudscale.datastore/services/logout').success(function(data, status, headers, config) {
            $scope.test = 'logged out';
            $scope.user = null;
            $rootScope.user = null;
        });
        $rootScope.$broadcast('login-required');
    };

    /**
     * Load current user
     */
    $scope.currentUser = function() {
        if(!$scope.user) {
            $http.get('/cloudscale.datastore/services/user').success(function(data, status, headers, config) {
                $scope.user = data;
                $rootScope.user = data;
                //$location.path('/' + navigation.firstPage($scope.user.role).link);
            });
        }
    };

    $rootScope.isAdmin = function() {
        return $rootScope.user.role === "ADMIN";
    };
});