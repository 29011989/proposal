webstoreApp.factory('Account', ['$http', function($http) {
    var baseUrl = '/cloudscale.datastore/services/user';
    return {
        query: function(success, error) {
            return $http.get(baseUrl).success(success).error(error);
        }
    }
}]);

webstoreApp.factory('AccountLoader', ['Account', '$q',
    function (Account, $q) {
        return function () {
            var delay = $q.defer();
            Account.query(function (account) {
                delay.resolve(account);
            }, function () {
                delay.reject('Unable to fetch user.');
            });
            return delay.promise;
        };
    }
]);