package at.ac.tuwien.infosys.jcloudscale.datastore.model;

import at.ac.tuwien.infosys.jcloudscale.annotations.Adapter;
import at.ac.tuwien.infosys.jcloudscale.annotations.Adapters;
import at.ac.tuwien.infosys.jcloudscale.datastore.driver.hbase.HbaseCell;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.adapter.HbaseProductReferenceTypeAdapter;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.adapter.JsonProductReferenceTypeAdapter;
import com.google.gson.JsonElement;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class OrderItem {

    @Adapters({
        @Adapter(targetClass = JsonElement.class, adapterClass = JsonProductReferenceTypeAdapter.class),
        @Adapter(targetClass = HbaseCell.class, adapterClass = HbaseProductReferenceTypeAdapter.class)
    })
    private Product product;

    private Integer count;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("product", product)
                .append("count", count)
                .toString();
    }
}
