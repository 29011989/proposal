package at.ac.tuwien.infosys.jcloudscale.datastore.model;

import at.ac.tuwien.infosys.jcloudscale.annotations.DatastoreId;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.dozer.Mapping;

import java.util.List;

/**
 * Class representing a sing product
 */
public class Product {

    /**
     * The id of the product
     */
    @Mapping("id")
    @DatastoreId
    private String _id;

    /**
     * The name of the product
     */
    private String name;

    /**
     * The price of the product
     */
    private Double price;

    private List<Property> properties;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("id", _id)
                .append("name", name)
                .append("price", price)
                .append("properties", properties)
                .toString();
    }
}
