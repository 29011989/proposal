package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;

public class MigrationDaoImpl implements MigrationDao {

    @DataSource(name = "hbase")
    private Datastore hbase;

    @Override
    public Order getOrderById(String orderId) {
        return hbase.find(Order.class, orderId);
    }

    @Override
    public void delete(Order order) {
        hbase.delete(order);
    }
}
