package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;

/**
 * Service for migrating order data to the session and vice versa
 */
public interface MigrationService {

    /**
     * Restore the order with the given id (migrate to session)
     *
     * @param orderID the given id
     */
    void restore(String orderID);

    /**
     * Remove migrated data for given order
     *
     * @param order the given order
     */
    void cleanup(Order order);
}
