package at.ac.tuwien.infosys.jcloudscale.datastore.model.adapter;

import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.driver.hbase.HbaseCell;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.type.TypeAdapter;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.type.TypeMetadata;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Product;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseProductReferenceTypeAdapter implements TypeAdapter<Product, HbaseCell> {

    @DataSource(name = "couchdb")
    private Datastore datastore;

    @Override
    public HbaseCell serialize(Product object, TypeMetadata<HbaseCell> typeMetadata) {
        String className = typeMetadata.getParent().getClass().getSimpleName();
        return new HbaseCell(className, typeMetadata.getFieldName(), Bytes.toBytes(object.get_id()));
    }

    @Override
    public Product deserialize(HbaseCell element, TypeMetadata<HbaseCell> typeMetadata) {
        byte[] value = element.getValue();
        String productID = Bytes.toString(value);
        return datastore.find(Product.class, productID);
    }
}
