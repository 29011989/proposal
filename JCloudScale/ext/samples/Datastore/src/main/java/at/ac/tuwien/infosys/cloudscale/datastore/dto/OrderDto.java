package at.ac.tuwien.infosys.jcloudscale.datastore.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO for order related information
 */
public class OrderDto {

    private String orderID;
    private String uid;
    private List<OrderItemDto> orderItems;

    public OrderDto() {
        this.orderItems = new ArrayList<OrderItemDto>();
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<OrderItemDto> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItemDto> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("uid", uid)
                .append("orderItems", orderItems)
                .toString();
    }
}
