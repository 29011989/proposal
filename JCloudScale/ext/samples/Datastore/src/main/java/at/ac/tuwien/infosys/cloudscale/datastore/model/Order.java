package at.ac.tuwien.infosys.jcloudscale.datastore.model;

import at.ac.tuwien.infosys.jcloudscale.annotations.DatastoreId;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.IdStrategy;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

public class Order {

    @DatastoreId(strategy = IdStrategy.MANUAL)
    private String orderID;

    private String uid;

    private List<OrderItem> orderItems;

    private Boolean saved;

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Boolean getSaved() {
        return saved;
    }

    public void setSaved(Boolean saved) {
        this.saved = saved;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("orderID", orderID)
                .append("uid", uid)
                .append("orderItems", orderItems)
                .append("saved", saved)
                .toString();
    }
}
