package at.ac.tuwien.infosys.jcloudscale.datastore.model;

/**
 * Class representing a single product property
 */
public class Property {

    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
