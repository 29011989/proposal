package at.ac.tuwien.infosys.jcloudscale.datastore.servlet;

import at.ac.tuwien.infosys.jcloudscale.datastore.dto.UserDto;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private UserService userService;
    private AuthenticationService authenticationService;
    private MigrationService migrationService;

    public LoginServlet() {
        super();
        userService = new UserServiceImpl();
        authenticationService = new AuthenticationServiceImpl();
        migrationService = new MigrationServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        log.info("Login called. (User: {}, Password: {})", username, password);
        if(authenticationService.isValidLogin(username, password)) {
            HttpSession session = req.getSession();
            session.setAttribute("user", username);
            resp.setStatus(HttpServletResponse.SC_OK);
            loadOrderData(username, session);
            log.info("Login successful.");
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            log.info("Login not successful.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //DO NOTHING
    }

    private void loadOrderData(String username, HttpSession session) {
        UserDto userDto = userService.getUser(username);
        if(userDto.getOrderID() != null) {
            log.info("Adding order with ID {} to session.", userDto.getOrderID());
            session.setAttribute("orderID", userDto.getOrderID());
            migrationService.restore(userDto.getOrderID());
        }
    }
}
