package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.datastore.model.Product;

import java.util.List;

/**
 * DAO for accessing product data
 */
public interface ProductDao {

    /**
     * Get all known products
     *
     * @return list of products
     */
    List<Product> getAllProducts();

    /**
     * Get the product with the given id
     *
     * @param id the given id
     * @return the product
     */
    Product getById(String id);

    /**
     * Delete the given product
     *
     * @param product the given product
     */
    void delete(Product product);

    /**
     * Save the given product
     *
     * @param product the product to save
     */
    void save(Product product);

    /**
     * Update the given product
     *
     * @param product the given product
     */
    void update(Product product);
}
