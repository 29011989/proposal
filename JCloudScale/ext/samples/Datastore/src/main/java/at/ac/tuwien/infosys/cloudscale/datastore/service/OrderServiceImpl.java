package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dao.OrderDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.OrderDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.UserDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.UserDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.dto.OrderDto;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.OrderItem;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Product;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.User;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class OrderServiceImpl implements OrderService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private OrderDao orderDao;
    private UserDao userDao;
    private Mapper mapper;

    public OrderServiceImpl() {
        orderDao = new OrderDaoImpl();
        userDao = new UserDaoImpl();
        mapper = new DozerBeanMapper();
    }

    @Override
    public OrderDto getOrder(String orderID) {
        if(orderID == null) {
            log.info("No order found. Returning empty dto.");
            return new OrderDto();
        }
        Order order = orderDao.getById(orderID);
        OrderDto orderDto = mapper.map(order, OrderDto.class);
        log.info("Found order {}.", orderDto);
        return orderDto;
    }

    @Override
    public String newOrder(OrderDto orderDto) {
        Order order = mapper.map(orderDto, Order.class);
        String orderID = UUID.randomUUID().toString();
        order.setOrderID(orderID);
        orderDao.save(order);
        log.info("Finished persisting Order {}.", order);
        return orderID;
    }

    @Override
    public void addOrder(String orderID, OrderDto orderDto) {
        Order newOrder = mapper.map(orderDto, Order.class);
        Order order = orderDao.getById(orderID);
        addItemOrIncreaseCounter(order, newOrder);
        orderDao.update(order);
        log.info("Finished updating Order {}.", order);
    }

    @Override
    public void removeOrder(String orderID, String productID) {
        Order order = orderDao.getById(orderID);
        removeOrderItem(order, productID);
        orderDao.update(order);
        log.info("Finished removing item from order {}.", order);
    }

    @Override
    public void migrate(String orderID) {
        Order order = orderDao.getById(orderID);
        log.info("Starting migration of order {}.", order);
        orderDao.migrateOrUpdate(order);
        log.info("Finished migrating order {}.", order);
        User user = userDao.getUserByUsername(order.getUid());
        user.setOrderID(order.getOrderID());
        userDao.update(user);
        log.info("Finished updating user data.");
    }

    private void addItemOrIncreaseCounter(Order order, Order newOrder) {
        Product product = newOrder.getOrderItems().get(0).getProduct();
        for(OrderItem items : order.getOrderItems()) {
            if(items.getProduct().get_id().equals(product.get_id())) {
                items.setCount(items.getCount() + 1);
                return;
            }
        }
        order.getOrderItems().addAll(newOrder.getOrderItems());
    }

    private void removeOrderItem(Order order, String productID) {
        List<OrderItem> itemList = new ArrayList<OrderItem>();
        for(OrderItem orderItem : order.getOrderItems()) {
            if(orderItem.getProduct().get_id().equals(productID)) {
                if(orderItem.getCount() > 1) {
                    log.info("Increasing counter for Item {}.", orderItem);
                    orderItem.setCount(orderItem.getCount() -1);
                } else {
                    log.info("Removing Item {}.", orderItem);
                    continue;
                }
            }
            itemList.add(orderItem);
        }
        order.setOrderItems(itemList);
    }
}
