package at.ac.tuwien.infosys.jcloudscale.datastore.model.adapter;

import at.ac.tuwien.infosys.jcloudscale.datastore.api.DatastoreException;
import at.ac.tuwien.infosys.jcloudscale.datastore.driver.hbase.HbaseCell;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.type.TypeAdapter;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.type.TypeMetadata;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Role;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Type Adapter for mapping roles
 */
public class RoleTypeAdapter implements TypeAdapter<Role, HbaseCell> {

    public JsonElement serializeJSON(Role object, TypeMetadata typeMetadata) {
        return new JsonPrimitive(object.toString());
    }

    public Role deserializeJSON(JsonElement jsonElement, TypeMetadata typeMetadata) {
        JsonPrimitive jsonPrimitive = (JsonPrimitive) jsonElement;
        if(!jsonPrimitive.isString()) {
            throw new DatastoreException("Invalid value for role type.");
        }
        String value = jsonPrimitive.getAsString();
        return Role.valueOf(value);
    }

    @Override
    public HbaseCell serialize(Role object, TypeMetadata<HbaseCell> typeMetadata) {
        String className = typeMetadata.getParent().getClass().getSimpleName();
        return new HbaseCell(className, typeMetadata.getFieldName(), Bytes.toBytes(object.toString()));
    }

    @Override
    public Role deserialize(HbaseCell element, TypeMetadata<HbaseCell> typeMetadata) {
        byte[] value = element.getValue();
        String name = Bytes.toString(value);
        return Role.valueOf(name);
    }
}
