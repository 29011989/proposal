package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.User;


public class UserDaoImpl implements UserDao {

    @DataSource(name = "hbase")
    private Datastore datastore;

    @Override
    public User getUserByUsername(String username) {
        return datastore.find(User.class, username);
    }

    @Override
    public void update(User user) {
        datastore.update(user);
    }
}
