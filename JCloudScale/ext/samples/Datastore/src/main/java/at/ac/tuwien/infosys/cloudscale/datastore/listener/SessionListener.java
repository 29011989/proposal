package at.ac.tuwien.infosys.jcloudscale.datastore.listener;

import at.ac.tuwien.infosys.jcloudscale.datastore.dao.OrderDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.OrderDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private OrderDao orderDao;

    public SessionListener() {
        orderDao = new OrderDaoImpl();
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        //DO NOTHING
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        log.info("Handling Session Destroy Event.");
        HttpSession session = se.getSession();
        if(session.getAttribute("orderID") == null) {
            log.info("No active order. Skipping cleanup.");
            return;
        }
        String orderId = (String) session.getAttribute("orderID");
        Order order = orderDao.getById(orderId);
        orderDao.delete(order);
        log.info("Finished cleanup for Session Destroy.");
    }
}
