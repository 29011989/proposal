package at.ac.tuwien.infosys.jcloudscale.datastore.model;

/**
 * User Roles
 */
public enum Role {
    /**
     * ADMIN user role
     */
    ADMIN,

    /**
     * CUSTOMER user role
     */
    CUSTOMER
}
