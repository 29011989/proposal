package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.annotations.DatastoreLib;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Product;
import org.lightcouch.CouchDbClient;

import java.util.List;

public class ProductDaoImpl implements ProductDao {

    @DatastoreLib(datastore = "couchdb", name = "lightcouch")
    private CouchDbClient couchDbClient;

    @DataSource(name = "couchdb")
    private Datastore datastore;

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = couchDbClient.view("_all_docs")
                .includeDocs(true)
                .query(Product.class);
        return products;
    }

    @Override
    public Product getById(String id) {
        return datastore.find(Product.class, id);
    }

    @Override
    public void delete(Product product) {
        datastore.delete(product);
    }

    @Override
    public void save(Product product) {
        datastore.save(product);
    }

    @Override
    public void update(Product product) {
        datastore.update(product);
    }
}
