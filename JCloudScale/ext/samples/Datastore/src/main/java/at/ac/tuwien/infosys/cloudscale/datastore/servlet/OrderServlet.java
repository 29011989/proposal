package at.ac.tuwien.infosys.jcloudscale.datastore.servlet;

import at.ac.tuwien.infosys.jcloudscale.datastore.dto.OrderDto;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.Mapper;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.json.JsonMapperImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.OrderService;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.OrderServiceImpl;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class OrderServlet extends HttpServlet {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private OrderService orderService;
    private Mapper<String> jsonMapper;

    public OrderServlet() {
        orderService = new OrderServiceImpl();
        jsonMapper = new JsonMapperImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Requesting Order.");
        InputStream inputStream = req.getInputStream();
        OrderDto orderDto = map(inputStream);
        if(isNewOrder(req)) {
            log.info("Creating new Order {}.", orderDto);
            String orderID = orderService.newOrder(orderDto);
            setOrderID(req, orderID);
        } else {
            log.info("Adding to order {}.", orderDto);
            String orderID = getOrderID(req);
            orderService.addOrder(orderID, orderDto);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderID = getOrderID(req);
        log.info("Requesting order with id {}.", orderID);
        OrderDto orderDto = orderService.getOrder(orderID);
        resp.setContentType("application/json");
        PrintWriter writer = resp.getWriter();
        writer.write(jsonMapper.serialize(orderDto));
        writer.flush();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] pathParams = req.getPathInfo().substring(1, req.getPathInfo().length()).split("/");
        log.info("Deleting product {} from order with id {}.", pathParams[1], pathParams[0]);
        orderService.removeOrder(pathParams[0], pathParams[1]);
    }

    private boolean isNewOrder(HttpServletRequest req) {
        return req.getSession().getAttribute("orderID") == null;
    }

    private void setOrderID(HttpServletRequest req, String orderID) {
        req.getSession().setAttribute("orderID", orderID);
    }

    private String getOrderID(HttpServletRequest req) {
        return (String) req.getSession().getAttribute("orderID");
    }

    //TODO error handling
    private OrderDto map(InputStream inputStream) {
        try {
            String jsonString = IOUtils.toString(inputStream);
            return jsonMapper.deserialize(jsonString, OrderDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
