package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dto.ProductDto;

import java.util.List;

/**
 * Methods for handling products
 */
public interface ProductService {

    /**
     * Get all available products
     *
     * @return list of products
     */
    List<ProductDto> getAllProducts();

    /**
     * Get the product with the given id
     *
     * @param id the given id
     * @return the product
     */
    ProductDto get(String id);

    /**
     * Delete the product with the given id
     *
     * @param id the given id
     */
    void delete(String id);

    /**
     * Save the given product
     *
     * @param productDto the given product
     * @return the persisted product
     */
    ProductDto save(ProductDto productDto);

    /**
     * Update the given product
     *
     * @param productDto the given product
     * @return the updated product
     */
    ProductDto update(ProductDto productDto);
}
