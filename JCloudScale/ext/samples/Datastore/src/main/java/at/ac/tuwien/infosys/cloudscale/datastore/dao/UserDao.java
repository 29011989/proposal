package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.datastore.model.User;

/**
 * DAO for accessing user data
 */
public interface UserDao {

    /**
     * Get a user by username
     *
     * @param username the username
     * @return the user
     */
    User getUserByUsername(String username);


    /**
     * Update the given user
     *
     * @param user the user to update
     */
    void update(User user);
}
