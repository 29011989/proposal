package at.ac.tuwien.infosys.jcloudscale.datastore.model;

import at.ac.tuwien.infosys.jcloudscale.annotations.Adapter;
import at.ac.tuwien.infosys.jcloudscale.annotations.Adapters;
import at.ac.tuwien.infosys.jcloudscale.annotations.DatastoreId;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.IdStrategy;
import at.ac.tuwien.infosys.jcloudscale.datastore.driver.hbase.HbaseCell;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.adapter.RoleTypeAdapter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class User {

    @DatastoreId(strategy = IdStrategy.MANUAL)
    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String address;

    private String city;

    private String email;

    private String phone;

    private String orderID;

    @Adapters({
        @Adapter(targetClass = HbaseCell.class, adapterClass = RoleTypeAdapter.class)
    })
    private Role role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("username", username)
                .append("password", password)
                .append("firstName", firstName)
                .append("lastName", lastName).toString();
    }
}
