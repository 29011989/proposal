package at.ac.tuwien.infosys.jcloudscale.datastore.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

/**
 * Class for managing hibernate connections
 */
public class Hibernate {

    private static final String PERSISTENCE_UNIT_NAME = "cloud";

    @PersistenceUnit
    private static EntityManagerFactory entityManagerFactory;

    /**
     * Get an entity manager instance
     *
     * @return an entity manager instance
     */
    public static EntityManager getEntityManager() {
        if(entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        }
        return entityManagerFactory.createEntityManager();
    }
}
