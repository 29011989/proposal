package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dto.OrderDto;

/**
 * Methods for handling orders
 */
public interface OrderService {

    /**
     * Get the order with the given id
     *
     * @param orderID the given order id
     * @return the order
     */
    OrderDto getOrder(String orderID);

    /**
     * Creates a new order
     *
     * @param orderDto the order
     * @return the order id
     */
    String newOrder(OrderDto orderDto);

    /**
     * Add the given order to the order with the given id
     *
     * @param orderID the id of the order
     * @param orderDto the order to add
     */
    void addOrder(String orderID, OrderDto orderDto);

    /**
     * Remove the product with the given id from the order with the given id
     *
     * @param orderID the id of the order
     * @param productID the id of the product
     */
    void removeOrder(String orderID, String productID);

    /**
     * Migrate the order with the given ID
     *
     * @param orderID the id of the order
     */
    void migrate(String orderID);
}
