package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.datastore.model.Bill;

/**
 * DAO for accessing bill data
 */
public interface BillDao {

    /**
     * Save the given bill
     *
     * @param bill the given bill
     */
    void save(Bill bill);
}
