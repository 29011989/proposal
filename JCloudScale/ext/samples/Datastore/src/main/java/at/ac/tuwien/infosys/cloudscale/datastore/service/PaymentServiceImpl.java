package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dao.*;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.*;

import java.util.ArrayList;
import java.util.Date;

public class PaymentServiceImpl implements PaymentService {

    private MigrationService migrationService;

    private OrderDao orderDao;
    private BillDao billDao;
    private UserDao userDao;

    public PaymentServiceImpl() {
        migrationService = new MigrationServiceImpl();
        orderDao = new OrderDaoImpl();
        billDao = new BillDaoImpl();
        userDao = new UserDaoImpl();
    }

    @Override
    public void pay(String orderID) {
        Order order = orderDao.getById(orderID);
        Bill bill = map(order);
        billDao.save(bill);
        User user = userDao.getUserByUsername(order.getUid());
        user.setOrderID(null);
        userDao.update(user);
        migrationService.cleanup(order);
        orderDao.delete(order);
    }

    private Bill map(Order order) {
        Bill bill = new Bill();
        bill.setBillDate(new Date());
        bill.setUid(order.getUid());
        bill.setItems(new ArrayList<BillItem>());
        for(OrderItem orderItem : order.getOrderItems()) {
            BillItem billItem = new BillItem();
            billItem.setBill(bill);
            billItem.setProductID(orderItem.getProduct().get_id());
            billItem.setProductName(orderItem.getProduct().getName());
            billItem.setPrice(orderItem.getProduct().getPrice());
            billItem.setCount(orderItem.getCount());
            bill.getItems().add(billItem);
        }
        return bill;
    }
}
