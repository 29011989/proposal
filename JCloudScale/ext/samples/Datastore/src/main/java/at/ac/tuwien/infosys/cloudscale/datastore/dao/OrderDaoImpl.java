package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderDaoImpl implements OrderDao {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @DataSource(name = "riak")
    private Datastore riak;

    @DataSource(name = "hbase")
    private Datastore hbase;

    @Override
    public Order getById(String id) {
        return riak.find(Order.class, id);
    }

    @Override
    public void save(Order order) {
        riak.save(order);
    }

    @Override
    public void update(Order order) {
        riak.update(order);
    }

    @Override
    public void migrateOrUpdate(Order order) {
        if(order.getSaved() == null || order.getSaved().equals(Boolean.FALSE)) {
            log.info("Migrating order to HBase.");
            order.setSaved(Boolean.TRUE);
            riak.migrate(hbase, Order.class, order.getOrderID());
            riak.update(order);
        } else if(order.getSaved() != null && order.getSaved().equals(Boolean.TRUE)) {
            log.info("Updating migrated data.");
            hbase.update(order);
        }
    }

    @Override
    public void delete(Order order) {
        riak.delete(order);
    }
}
