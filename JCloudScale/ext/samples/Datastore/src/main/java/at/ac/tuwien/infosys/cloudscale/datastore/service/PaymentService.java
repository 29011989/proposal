package at.ac.tuwien.infosys.jcloudscale.datastore.service;

public interface PaymentService {

    /**
     * Pay the order with the given id
     *
     * @param orderID the given id
     */
    void pay(String orderID);
}
