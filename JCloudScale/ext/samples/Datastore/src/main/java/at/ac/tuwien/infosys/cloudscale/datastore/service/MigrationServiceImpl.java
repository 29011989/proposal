package at.ac.tuwien.infosys.jcloudscale.datastore.service;


import at.ac.tuwien.infosys.jcloudscale.datastore.dao.MigrationDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.MigrationDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.OrderDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.OrderDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MigrationServiceImpl implements MigrationService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private MigrationDao migrationDao;
    private OrderDao orderDao;

    public MigrationServiceImpl() {
        migrationDao = new MigrationDaoImpl();
        orderDao = new OrderDaoImpl();
    }

    @Override
    public void restore(String orderID) {
        log.info("Restoring order with id {}.", orderID);
        Order order = migrationDao.getOrderById(orderID);
        orderDao.save(order);
    }

    @Override
    public void cleanup(Order order) {
        log.info("Migration cleanup for oder {} called.", order);
        if(order.getSaved() != null && order.getSaved().equals(Boolean.TRUE)) {
            log.info("Removing migration data.");
            migrationDao.delete(order);
        }
    }
}
