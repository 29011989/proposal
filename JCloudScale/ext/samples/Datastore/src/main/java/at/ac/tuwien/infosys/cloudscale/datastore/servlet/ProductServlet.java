package at.ac.tuwien.infosys.jcloudscale.datastore.servlet;

import at.ac.tuwien.infosys.jcloudscale.datastore.dto.ProductDto;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.ProductService;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.ProductServiceImpl;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

public class ProductServlet extends HttpServlet {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private ProductService productService;
    private Gson gson;

    public ProductServlet() {
        productService = new ProductServiceImpl();
        gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(isSingleProductRequest(req)) {
            String id = req.getPathInfo().substring(1, req.getPathInfo().length());
            log.info("Requesting product with id {}.", id);
            ProductDto productDto = productService.get(id);
            resp.setContentType("application/json");
            PrintWriter writer = resp.getWriter();
            writer.write(gson.toJson(productDto));
            writer.flush();
        } else {
            log.info("Requesting all products.");
            List<ProductDto> products = productService.getAllProducts();
            resp.setContentType("application/json");
            PrintWriter writer = resp.getWriter();
            writer.write(gson.toJson(products));
            writer.flush();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getPathInfo().substring(1, req.getPathInfo().length());
        log.info("Deleting product with id {}.", id);
        productService.delete(id);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InputStream inputStream = req.getInputStream();
        ProductDto productDto = map(inputStream);
        if(isSingleProductRequest(req)) {
            String id = req.getPathInfo().substring(1, req.getPathInfo().length());
            log.info("Updating product {} with id {}.", productDto, id);
            productDto.setId(id);
            productDto = productService.update(productDto);
            resp.setContentType("application/json");
            PrintWriter writer = resp.getWriter();
            writer.write(gson.toJson(productDto));
            writer.flush();
        } else {
            log.info("Persisting product: {}.", productDto);
            productDto = productService.save(productDto);
            resp.setContentType("application/json");
            PrintWriter writer = resp.getWriter();
            writer.write(gson.toJson(productDto));
            writer.flush();
        }
    }

    private boolean isSingleProductRequest(HttpServletRequest request) {
        return request.getPathInfo() != null;
    }

    //TODO: Error handling
    private ProductDto map(InputStream inputStream) {
        try {
            String jsonString = IOUtils.toString(inputStream);
            ProductDto productDto = gson.fromJson(jsonString, ProductDto.class);
            return productDto;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
