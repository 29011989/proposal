package at.ac.tuwien.infosys.jcloudscale.datastore.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Bill {

    @Id
    @GeneratedValue
    private Long id;

    private String uid;

    @Temporal(TemporalType.DATE)
    private Date billDate;

    @OneToMany(mappedBy = "bill", cascade = CascadeType.ALL)
    private List<BillItem> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String user) {
        this.uid = user;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date date) {
        this.billDate = date;
    }

    public List<BillItem> getItems() {
        return items;
    }

    public void setItems(List<BillItem> items) {
        this.items = items;
    }
}
