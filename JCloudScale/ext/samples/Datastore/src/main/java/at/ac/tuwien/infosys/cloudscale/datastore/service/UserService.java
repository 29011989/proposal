package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dto.UserDto;

/**
 * Service for user related functions
 */
public interface UserService {

    /**
     * Get the information for the user with the given username
     *
     * @param username the given username
     * @return the user dto
     */
    UserDto getUser(String username);

}
