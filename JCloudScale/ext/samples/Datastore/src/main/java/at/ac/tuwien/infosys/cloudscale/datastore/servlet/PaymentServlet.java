package at.ac.tuwien.infosys.jcloudscale.datastore.servlet;

import at.ac.tuwien.infosys.jcloudscale.datastore.service.PaymentService;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.PaymentServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PaymentServlet extends HttpServlet {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private PaymentService paymentService;

    public PaymentServlet() {
        paymentService = new PaymentServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(!orderExists(req)) {
            log.info("Payment aborted. No order exists.");
            return;
        }
        String orderID = getOrderID(req);
        paymentService.pay(orderID);
        removeOrderID(req);
    }

    private boolean orderExists(HttpServletRequest req) {
        return req.getSession().getAttribute("orderID") != null;
    }

    private String getOrderID(HttpServletRequest req) {
        return (String) req.getSession().getAttribute("orderID");
    }

    private void removeOrderID(HttpServletRequest req) {
        req.getSession().removeAttribute("orderID");
    }
}
