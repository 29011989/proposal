package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;

/**
 * DAO for migration related functions
 */
public interface MigrationDao {

    /**
     * Get the order with the given id
     *
     * @param orderId the given id of the order
     * @return the order
     */
    Order getOrderById(String orderId);

    /**
     * Delete the given order
     *
     * @param order the given order
     */
    void delete(Order order);
}
