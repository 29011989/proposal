package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.datastore.model.Bill;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class BillDaoImpl implements BillDao {

    private EntityManager entityManager;

    public BillDaoImpl() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("cloud");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void save(Bill bill) {
        entityManager.getTransaction().begin();
        entityManager.persist(bill);
        entityManager.getTransaction().commit();
    }
}
