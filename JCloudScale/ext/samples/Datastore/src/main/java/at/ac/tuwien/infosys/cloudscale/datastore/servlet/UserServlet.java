package at.ac.tuwien.infosys.jcloudscale.datastore.servlet;

import at.ac.tuwien.infosys.jcloudscale.datastore.dto.UserDto;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.Mapper;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.json.JsonMapperImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.UserService;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class UserServlet extends HttpServlet {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private UserService userService;
    private Mapper<String> jsonMapper;

    public UserServlet() {
        userService = new UserServiceImpl();
        jsonMapper = new JsonMapperImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = (String) req.getSession().getAttribute("user");
        log.info("Requesting information for user with id {}.", username);
        UserDto userDto = userService.getUser(username);
        resp.setContentType("application/json");
        PrintWriter writer = resp.getWriter();
        writer.write(jsonMapper.serialize(userDto));
        writer.flush();
    }
}
