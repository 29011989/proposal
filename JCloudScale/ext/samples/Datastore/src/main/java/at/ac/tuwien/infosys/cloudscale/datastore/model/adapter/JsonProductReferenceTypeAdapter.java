package at.ac.tuwien.infosys.jcloudscale.datastore.model.adapter;

import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.DatastoreException;
import at.ac.tuwien.infosys.jcloudscale.datastore.driver.hbase.HbaseCell;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.type.TypeAdapter;
import at.ac.tuwien.infosys.jcloudscale.datastore.mapping.type.TypeMetadata;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Product;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

/**
 * Adapter for mapping product referenced
 * Used only when product already saved in CouchDB
 */
public class JsonProductReferenceTypeAdapter implements TypeAdapter<Product, JsonElement> {

    @DataSource(name = "couchdb")
    private Datastore datastore;

    @Override
    public JsonElement serialize(Product object, TypeMetadata<JsonElement> typeMetadata) {
        String productID = object.get_id();
        return new JsonPrimitive(productID);
    }

    @Override
    public Product deserialize(JsonElement element, TypeMetadata<JsonElement> typeMetadata) {
        JsonPrimitive jsonPrimitive = (JsonPrimitive) element;
        if(!jsonPrimitive.isString()) {
            throw new DatastoreException("Invalid value for product reference.");
        }
        String productID = jsonPrimitive.getAsString();
        return datastore.find(Product.class, productID);
    }
}
