package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dao.UserDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.UserDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.dto.UserDto;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.User;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private UserDao userDao;
    private Mapper mapper = new DozerBeanMapper();

    public UserServiceImpl() {
        userDao = new UserDaoImpl();
    }

    @Override
    public UserDto getUser(String username) {
        User user = userDao.getUserByUsername(username);
        UserDto userDto = new UserDto();
        if(user != null) {
            userDto = mapper.map(user, UserDto.class);
            userDto.setRole(user.getRole().toString());
        }
        log.info("Found user {}.", userDto);
        return userDto;
    }
}
