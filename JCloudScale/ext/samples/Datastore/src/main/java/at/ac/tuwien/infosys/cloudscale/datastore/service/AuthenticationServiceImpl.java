package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dao.UserDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.UserDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthenticationServiceImpl implements AuthenticationService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private UserDao userDao;

    public AuthenticationServiceImpl() {
        userDao = new UserDaoImpl();
    }

    @Override
    public boolean isValidLogin(String username, String password) {
        if(username == null || password == null) {
            return false;
        }
        User user = userDao.getUserByUsername(username);
        log.info("Found user {}.", user);
        if(user.getPassword().equals(password)) {
            return true;
        }
        return false;
    }
}
