package at.ac.tuwien.infosys.jcloudscale.datastore.dao;

import at.ac.tuwien.infosys.jcloudscale.datastore.model.Order;

/**
 * DAO for accessing order data
 */
public interface OrderDao {

    /**
     * Get an order by the given id
     *
     * @param id the given id
     * @return the order
     */
    Order getById(String id);

    /**
     * Save the given order
     *
     * @param order the given order
     * @return the id
     */
    void save(Order order);

    /**
     * Update the given order
     *
     * @param order the given order
     */
    void update(Order order);

    /**
     * Migrate or update the given order
     *
     * @param order the order to migrate
     */
    void migrateOrUpdate(Order order);

    /**
     * Delete the given order
     *
     * @param order the given order
     */
    void delete(Order order);
}
