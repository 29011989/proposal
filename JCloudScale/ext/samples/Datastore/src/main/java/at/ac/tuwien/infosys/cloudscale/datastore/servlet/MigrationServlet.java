package at.ac.tuwien.infosys.jcloudscale.datastore.servlet;

import at.ac.tuwien.infosys.jcloudscale.datastore.service.OrderService;
import at.ac.tuwien.infosys.jcloudscale.datastore.service.OrderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MigrationServlet extends HttpServlet {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private OrderService orderService;

    public MigrationServlet() {
        orderService = new OrderServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(!orderExists(req)) {
            log.info("Migration aborted. No order exists.");
            return;
        }
        String orderID = getOrderID(req);
        orderService.migrate(orderID);
    }

    private boolean orderExists(HttpServletRequest req) {
        return req.getSession().getAttribute("orderID") != null;
    }

    private String getOrderID(HttpServletRequest req) {
        return (String) req.getSession().getAttribute("orderID");
    }
}
