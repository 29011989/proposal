package at.ac.tuwien.infosys.jcloudscale.datastore;


import at.ac.tuwien.infosys.jcloudscale.annotations.DataSource;
import at.ac.tuwien.infosys.jcloudscale.datastore.api.Datastore;
import at.ac.tuwien.infosys.jcloudscale.datastore.core.DatastoreFactory;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Role;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.User;

public class Main {

    public static void main(String[] args) {
        User user = new User();
        user.setUsername("admin");
        user.setPassword("pass");
        user.setFirstName("Rene");
        user.setLastName("Nowak");
        user.setRole(Role.ADMIN);

        User customer = new User();
        customer.setUsername("customer");
        customer.setPassword("pass");
        customer.setFirstName("Rene");
        customer.setLastName("Nowak");
        customer.setRole(Role.CUSTOMER);

        Datastore datastore = DatastoreFactory.getInstance().getDatastoreByName("hbase");
        datastore.save(user);
        datastore.save(customer);
    }
}
