package at.ac.tuwien.infosys.jcloudscale.datastore.service;

/**
 * Basic Authentication Service
 */
public interface AuthenticationService {

    /**
     * Check if the given login credentials are valid
     *
     * @param username given username
     * @param password given password
     * @return true if valid credentials, false otherwise
     */
    public boolean isValidLogin(String username, String password);
}
