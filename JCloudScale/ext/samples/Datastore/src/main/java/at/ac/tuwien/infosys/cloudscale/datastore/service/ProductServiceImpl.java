package at.ac.tuwien.infosys.jcloudscale.datastore.service;

import at.ac.tuwien.infosys.jcloudscale.datastore.dao.ProductDao;
import at.ac.tuwien.infosys.jcloudscale.datastore.dao.ProductDaoImpl;
import at.ac.tuwien.infosys.jcloudscale.datastore.dto.ProductDto;
import at.ac.tuwien.infosys.jcloudscale.datastore.model.Product;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private ProductDao productDao;
    private Mapper mapper;

    public ProductServiceImpl() {
        productDao = new ProductDaoImpl();
        mapper = new DozerBeanMapper();
    }

    @Override
    public List<ProductDto> getAllProducts() {
        log.info("Get all available products.");
        List<Product> products = productDao.getAllProducts();
        log.info("Found {} products.", products.size());
        return map(products);
    }

    @Override
    public ProductDto get(String id) {
        Product product = productDao.getById(id);
        ProductDto productDto = mapper.map(product, ProductDto.class);
        log.info("Found product {}.", productDto);
        return productDto;
    }

    @Override
    public void delete(String id) {
        Product product = new Product();
        product.set_id(id);
        productDao.delete(product);
        log.info("Finished deleting product.");
    }

    @Override
    public ProductDto save(ProductDto productDto) {
        Product product = mapper.map(productDto, Product.class);
        productDao.save(product);
        productDto = mapper.map(product, ProductDto.class);
        log.info("Finished persisting Product {}.", productDto);
        return productDto;
    }

    @Override
    public ProductDto update(ProductDto productDto) {
        Product product = mapper.map(productDto, Product.class);
        productDao.update(product);
        log.info("Finished updating Product {}.", productDto);
        return productDto;
    }

    private List<ProductDto> map(List<Product> products) {
        log.info("Mapping Products to ProductsDto.");
        List<ProductDto> productDtos = new ArrayList<ProductDto>();
        for (Product product : products) {
            if(product.getName() != null) {
                ProductDto productDto = mapper.map(product, ProductDto.class);
                productDtos.add(productDto);
            }
        }
        log.info("Finished mapping {} products.", productDtos.size());
        return productDtos;
    }
}
