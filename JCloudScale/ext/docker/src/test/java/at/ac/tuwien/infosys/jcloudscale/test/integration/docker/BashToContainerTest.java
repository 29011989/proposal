/*
   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.integration.docker;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import org.junit.Assert;

import com.spotify.docker.client.DockerException;

import at.ac.tuwien.infosys.jcloudscale.test.util.DockerConfigurationHelper;
import at.ac.tuwien.infosys.jcloudscale.vm.docker.DockerCloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.docker.DockerWrapper;

public class BashToContainerTest {

	DockerCloudPlatformConfiguration configuration;

	// @Before
	// public void before() throws IOException {
	// Properties dockerProperties = new Properties();
	// try (InputStream propertiesStream = ClassLoader
	// .getSystemResourceAsStream(ConfigurationHelper.DOCKER_CONFIG_FILE)) {
	// dockerProperties.load(propertiesStream);
	// }
	// configuration = new DockerCloudPlatformConfiguration()
	// .withInstanceImage(dockerProperties.getProperty(ConfigurationHelper.INSTANCE_IMAGE_CONFIG))
	// .withDockerHost(dockerProperties.getProperty(ConfigurationHelper.DOCKER_HOST_CONFIG));
	//
	// }

	// TODO Not sure what this test is about, so I comment it out !
	// @Test
	public void echoUUID() {
		try {
			// Start a container
			String containerID = "34c993256a82f7431f273c6a01b8eb526fc5a929514cc602e132960f3b3ece51";
			// Do the magic
			DockerWrapper dw = DockerConfigurationHelper.getDockerWrapper();
			UUID uuid = UUID.randomUUID();
			dw.map(uuid, containerID);

			String lookup = dw.lookupHostByUUID(uuid);
			Assert.assertEquals(containerID, lookup);
		} catch (DockerException | InterruptedException | IOException | URISyntaxException e) {
			e.printStackTrace();
			Assert.fail();

		}
	}
}
