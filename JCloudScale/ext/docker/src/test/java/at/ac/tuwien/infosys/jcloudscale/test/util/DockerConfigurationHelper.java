/*
   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.test.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.policy.sample.SingleHostScalingPolicy;
import at.ac.tuwien.infosys.jcloudscale.vm.docker.DockerCloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.docker.DockerWrapper;

public class DockerConfigurationHelper extends ConfigurationHelper {

	private static final String DOCKER_CONFIG_FILE = "docker.props";
	private static final String DOCKER_HOST_CONFIG = "docker.host";
	private static final String INSTANCE_IMAGE_CONFIG = "instance.image";
	private static final String SERVER_ADDRESS_CONFIG = "mq.address";

	public static JCloudScaleConfigurationBuilder createDefaultDockerTestConfiguration()
			throws FileNotFoundException, IOException, URISyntaxException {
		Properties dockerProperties = new Properties();
		try (InputStream propertiesStream = ClassLoader.getSystemResourceAsStream(DOCKER_CONFIG_FILE)) {
			dockerProperties.load(propertiesStream);
		}

		String serverAddress = dockerProperties.getProperty(SERVER_ADDRESS_CONFIG);

		if (serverAddress == null || serverAddress.length() == 0)
			throw new RuntimeException("Could not read ActiveMQ server address from the property \""
					+ SERVER_ADDRESS_CONFIG + "\" from the config file " + DOCKER_CONFIG_FILE);

		System.out.println("ConfigurationHelper.createDefaultDockerTestConfiguration() "
				+ dockerProperties.getProperty(DOCKER_HOST_CONFIG));

		return applyCommonConfiguration(new JCloudScaleConfigurationBuilder(new DockerCloudPlatformConfiguration()
				// TODO Probably here we shall simply rely on configuration over
				// the image name instead !
				.withInstanceImage(dockerProperties.getProperty(INSTANCE_IMAGE_CONFIG))
				.withDockerHost(dockerProperties.getProperty(DOCKER_HOST_CONFIG))).withMQServerHostname(serverAddress));
		// TODO Migration tests requires a different policy to work are they are now
//						.with(new SingleHostScalingPolicy());
	}

	public static DockerWrapper getDockerWrapper() throws FileNotFoundException, IOException, URISyntaxException {

		Properties dockerProperties = new Properties();
		try (InputStream propertiesStream = ClassLoader.getSystemResourceAsStream(DOCKER_CONFIG_FILE)) {
			dockerProperties.load(propertiesStream);
		}

		return new DockerCloudPlatformConfiguration()
				.withInstanceImage(dockerProperties.getProperty(INSTANCE_IMAGE_CONFIG))
				.withDockerHost(dockerProperties.getProperty(DOCKER_HOST_CONFIG)).getDockerWrapper();

	}

}
