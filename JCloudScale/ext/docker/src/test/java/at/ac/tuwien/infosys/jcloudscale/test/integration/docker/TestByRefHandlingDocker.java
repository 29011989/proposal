/*
   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.integration.docker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleShutdown;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.management.CloudManager;
import at.ac.tuwien.infosys.jcloudscale.management.references.ClassPassingConfiguration;
import at.ac.tuwien.infosys.jcloudscale.test.integration.base.TestByRefHandling;
import at.ac.tuwien.infosys.jcloudscale.test.testobject.ByRefCloudObject;
import at.ac.tuwien.infosys.jcloudscale.test.testobject.TestByRefCallbackCloudObject;
import at.ac.tuwien.infosys.jcloudscale.test.testobject.TestByValueNotAnnotatedParameter;
import at.ac.tuwien.infosys.jcloudscale.test.util.DockerConfigurationHelper;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;

public class TestByRefHandlingDocker extends at.ac.tuwien.infosys.jcloudscale.test.integration.base.TestByRefHandling {
	private static DockerHelper staticInstanceManager;

	@BeforeClass
	public static void setup() throws Exception {
		//
		// Initializing JCloudScale
		//
		JCloudScaleConfiguration cfg = DockerConfigurationHelper.createDefaultDockerTestConfiguration().build();

		JCloudScaleClient.setConfiguration(cfg);

		//
		// Starting "static" hosts.
		//
		staticInstanceManager = new DockerHelper(cfg);
		staticInstanceManager.startStaticInstances();

		cs = CloudManager.getInstance();
	}

	@AfterClass
	@JCloudScaleShutdown
	public static void tearDown() throws Exception {
		staticInstanceManager.shudownStaticInstances();
	}

	@After
	public void cleanup() throws Exception {
		for (UUID obj : new ArrayList<>(cs.getCloudObjects()))
			cs.destructCloudObject(obj);
	}

	/**
	 * FIXME This test when executed results in the next two tests to fail !
	 * But If I redeclare the following tests like below, all tests pass ?
	 */
	@Test
	@Override
	public void testClassAsParameterAndResult() throws Exception {
		super.testClassAsParameterAndResult();
	}

	@Test
	@Override
	public void testConfiguredByValueParameter() throws Exception {
		super.testConfiguredByValueParameter();
	}

}
