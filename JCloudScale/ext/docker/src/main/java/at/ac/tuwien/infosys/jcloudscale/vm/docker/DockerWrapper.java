/*
   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.vm.docker;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerClient.ExecCreateParam;
import com.spotify.docker.client.DockerClient.ListContainersParam;
import com.spotify.docker.client.DockerException;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerInfo;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.exception.JCloudScaleException;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.ICloudWrapper;

/*
 * @author gambi
 * See https://github.com/spotify/docker-client/blob/master/docs/user_manual.md
 */
public class DockerWrapper implements ICloudWrapper {

	private final static String JCS = "JCloudScale";
	private final static String JCS_ID = "jcsID";

	private DockerCloudPlatformConfiguration config = null;
	private Logger log = null;

	final DockerClient docker;

	DockerWrapper(DockerCloudPlatformConfiguration config) {
		this.config = config;
		this.log = JCloudScaleConfiguration.getLogger(this);

		// AuthConfig authConfig =
		// AuthConfig.builder().username(config.getLogin())
		// .password(config.getPassword()).serverAddress(config.getDockerHost()).build();
		//
		// docker = DefaultDockerClient.authConfig(authConfig).build();
		docker = DefaultDockerClient.builder().uri(URI.create(config.getDockerHost())).build();

	}

	@Deprecated // Use startNewHost(size instead)
	public String startNewHost() {
		return startNewHostWithUUID(null);
	}

	@Override
	public void startNewHost(String size) {
		startNewHostWithUUID(null);
	}

	// Note this one !
	// The UUID is the one passed in by JCS since Docker has not the same UUID
	// schema of OS or EC2
	public String startNewHostWithUUID(UUID id) {

		// System.out.println("DockerWrapper.startNewHost() " + id);
		if (config.getImageId() == null) {
			String message = "Cannot scale up as requested since no valid " + "Openstack image could be found for name "
					+ config.getImageName();
			log.severe(message);
			throw new JCloudScaleException(message);
		}
		try {

			MessageQueueConfiguration activeMQ = JCloudScaleConfiguration.getConfiguration().common().communication();

			Map<String, String> labels = new HashMap<String, String>();
			labels.put(JCS, "");
			if (id != null) {
				labels.put(JCS_ID, id.toString());
			}
			// TODO Can we use the JCS_ID to identify ours VM and the ID mapping
			// ?
			final ContainerConfig containerConfig = ContainerConfig.builder().image(config.getImageId())
					.env(String.format("ACTIVEMQ=%s:%d", activeMQ.getServerAddress(), activeMQ.getServerPort()))
					//
					.labels(labels)
					//
					.build();
			// Before create container
			final ContainerCreation creation = docker.createContainer(containerConfig);

			// System.out.println("DockerWrapper.startNewHost() " +
			// creation.id());
			docker.startContainer(creation.id());

			// NOT SURE ABOUT THIS ONE THEN !!!
			// TODO Replace the ID inside the container with an ENV variable?
			if (id != null) {
				// System.out.println("DockerWrapper.startNewHost() Force
				// mapping to " + id);
				// log.warning("DockerWrapper.startNewHost() Storing JCS UUID
				// inside
				// the container.");
				// TODO Can we update the labels, the problem here is that the
				// creation.id is known only after the id ?
				map(id, creation.id());
			}

			return creation.id();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	// Image name is actually repository:tag or repository:latest
	String lookupImageId(String imgName) throws DockerException, InterruptedException {
		// System.out.println("DockerWrapper.lookupImageId() " + imgName);
		if (imgName == null || imgName.length() == 0)
			return null;

		for (com.spotify.docker.client.messages.Image img : docker.listImages()) {

			if (img.repoTags().toString().replace("[", "").replace("]", "").equals(imgName)) {
				return img.id();
			}
		}

		return null;

	}

	private boolean isHostRunningViaId(String containerId) throws DockerException, InterruptedException {

		// System.out.println("DockerWrapper.isHostRunningViaId() " +
		// containerId);
		for (Container c : docker.listContainers()) {
			if (c.id().equals(containerId)) {
				// System.out.println("DockerInstanceWrapper.isHostRunningViaId()
				// " + c.status());
				return c.status().toLowerCase().contains("up");
			}
		}
		return false;
	}

	// public void shutdownHost(UUID id) {
	// // Lookup using docker the metadata of all the VM/container to find out
	// // the corresponding containerID
	// // TODO Perform some check here ?!
	// try {
	// shutdownHostViaInternalId(lookupHostByUUID(id));
	// } catch (DockerException | InterruptedException e) {
	// e.printStackTrace();
	// throw new RuntimeException("Error while shutdown host with id " + id, e);
	// }
	// }

	// This is required for static hosts
	@Override
	public void shutdownHost(String ip) {
		try {
			shutdownHostViaInternalId(lookupHostByIp(ip));
		} catch (DockerException | InterruptedException e) {
			String msg = "Failed to shutdown host " + ip;
			log.severe(msg);
			throw new RuntimeException(msg, e);
		}
	}

	// TODO Update this using docker labels instead
	//
	// This is quite bad but cannot see other solution yet !
	// This is public for testing purposes !
	public String lookupHostByUUID(UUID id) throws DockerException, InterruptedException {
		// System.out.println("DockerWrapper.lookupHostByUUID() " + id);
		for (Container c : docker.listContainers()) {

			// DEBUG WHY THIS DOES NOT WORK !
			// final String[] command_ls = { "ls" };
			// final String execId_ls = docker.execCreate(c.id(), command_ls,
			// ExecParameter.STDOUT, ExecParameter.STDERR);
			// final LogStream stream_ls = docker.execStart(execId_ls);
			//
			// System.out.println("DockerWrapper.lookupHostByUUID() Checking " +
			// stream_ls.readFully());

			// NOTE: https://github.com/kubernetes/kubernetes/issues/25456
			// This might cause problems when VM are shutting down !
			final String[] command = { "cat", "UUID" };
			final String execId = docker.execCreate(c.id(), command, ExecCreateParam.attachStdout(),
					ExecCreateParam.attachStderr());

			try (final LogStream stream = docker.execStart(execId)) {
				final String _id = stream.readFully();
				// System.out.println("DockerWrapper.lookupHostByUUID() Checking
				// " +
				// c.id() + " got " + _id);

				if (id.toString().equals(_id.trim())) {
					return c.id();
				}
			}

		}
		log.warning("Cannot find container with UUID " + id);

		return null;
	}

	String lookupHostByIp(String ip) throws DockerException, InterruptedException {
		for (Container c : docker.listContainers(ListContainersParam.withLabel(JCS))) {
			//
			if (ip.equals(docker.inspectContainer(c.id()).networkSettings().ipAddress())) {
				return c.id();
			}
		}
		log.warning("Cannot find container with IP " + ip);
		return null;
	}

	void shutdownHostViaInternalId(String containerId) throws DockerException, InterruptedException {

		log.info("DockerWrapper.shutdownHostViaInternalId() " + containerId);
		// waiting for server to actually start shutting down.
		try {
			docker.killContainer(containerId);
			final int SLEEP_TIME = 100;
			int timeout = 100;// 10 sec should be enough.
			do {
				Thread.sleep(SLEEP_TIME);
			} while (timeout-- > 0 && isHostRunningViaId(containerId));

			if (timeout <= 0)
				log.warning("While Shutting down host with OpenStack Id=" + containerId
						+ " waited for timeout and server is still active.");
		} catch (InterruptedException e) {
		} finally {
			if (this.config.isRemoveContainers())
				docker.removeContainer(containerId);
		}
	}

	public void map(UUID id, String containerID) throws DockerException, InterruptedException {
		log.warning("Mappping" + id + " and " + containerID);
		final String[] command = { "/bin/bash", "-c", "echo \"" + id.toString() + "\" > UUID" };
		final String execId = docker.execCreate(containerID, command);
		final LogStream output = docker.execStart(execId);
		final String execOutput = output.readFully();
		log.fine("Mapping done. Result: \n " + execOutput);

	}

	@Override
	public List<String> listCloudHosts() {
		System.out.println("DockerWrapper.listCloudHosts()");
		List<Container> containers;
		List<String> containersIP = new ArrayList<String>();
		try {
			// List all the containers that we started
			containers = docker.listContainers(ListContainersParam.withLabel(JCS));
			for (Container container : containers) {
				final ContainerInfo info = docker.inspectContainer(container.id());
				containersIP.add(info.networkSettings().ipAddress());
			}
		} catch (DockerException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return containersIP;
	}

}
