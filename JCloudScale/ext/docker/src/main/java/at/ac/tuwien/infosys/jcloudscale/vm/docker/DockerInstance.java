/*
   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.vm.docker;

import at.ac.tuwien.infosys.jcloudscale.logging.Logged;
import at.ac.tuwien.infosys.jcloudscale.vm.IIdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalVM;

@Logged
public class DockerInstance extends LocalVM {

	private String containerID;

	public DockerInstance(DockerCloudPlatformConfiguration config, IIdManager idManager,
			boolean startPerformanceMonitoring) {
		super(idManager, startPerformanceMonitoring);
		this.config = config;
	}

	@Override
	protected void launchHost(String size) {
		this.instanceSize = size;
		this.containerID = ((DockerCloudPlatformConfiguration) config).getDockerWrapper().startNewHost();
	}

	// /*
	// *
	// * TODO This probably need some check and refactoring
	// *
	// *
	// * We needed to override this method to include the UUID-containerID
	// * mapping(non-Javadoc)
	// *
	// * @see
	// * at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalVM#startupHost(at.ac.
	// * tuwien.infosys.jcloudscale.vm.IHostPool, java.lang.String) logic.
	// Docker
	// * does not use UUID !
	// */
	// @Override
	// public void startupHost(IHostPool hostPool, String size) throws
	// ScalingException {
	//
	// // ?
	// UUID serverId = idManager.getFreeId(size == null);
	//
	// // This somehow blocks
	// if (serverId == null) {
	// launchHost(size);
	// serverId = idManager.waitForId();
	// }
	//
	// // check if this is an ID of a static or dynamic host
	// if (idManager.isStaticId(serverId)) {
	// System.out.println("======= LocalVM.startupHost() Setting " + this + "("
	// + serverId + ")" + "as static !");
	// this.isStatic = true;
	// }
	//
	// this.id = serverId;
	//
	// // Now is the moment to register UUID inside the Container
	// try {
	// if (this.containerID != null) {
	// ((DockerCloudPlatformConfiguration)
	// config).getDockerWrapper().map(this.id, this.containerID);
	// } else {
	// this.containerID = ((DockerCloudPlatformConfiguration)
	// config).getDockerWrapper()
	// .lookupHostByUUID(this.id);
	// }
	// } catch (DockerException | InterruptedException e1) {
	// throw new JCloudScaleException(e1, "Failed to map " + serverId + " and "
	// + this.containerID);
	// }
	//
	// this.serverIp = idManager.getIpToId(serverId);
	// log.info("Server " + serverId + " has IP address " + serverIp);
	//
	// // sending configuration.
	// try {
	// JCloudScaleConfiguration.getConfiguration().sendConfigurationToStaticHost(id);
	// log.info("Successfully sent configuration to host " + id + " (" +
	// this.serverIp + ")");
	// } catch (NamingException | JMSException | TimeoutException | IOException
	// e) {
	// throw new JCloudScaleException(e,
	// "Failed to send configuration to host " + serverId + "(" + this.serverIp
	// + ")");
	// }
	//
	// server = new MigrationEnabledVirtualHostProxy(this.id);
	//
	// if (startPerformanceMonitoring) {
	// registerCPUMetric();
	// registerRAMMetric();
	// }
	//
	//
	//
	// startupTime = new Date(System.currentTimeMillis());
	//
	// hostStarted();
	//
	// this.scaleDownTask = new ScaleDownTask(hostPool);
	// log.info("Started scale-down task for new server.");
	//
	// }

	@Override
	public void close() {
		super.close();
		if (!isStaticHost()) {
			log.info("DockerInstance.close() " + this + "(" + this.id + ")" + " is not static ! ");
			((DockerCloudPlatformConfiguration) config).getDockerWrapper().shutdownHost(serverIp);
		}
	}

}
