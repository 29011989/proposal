/*

   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.vm.docker;

import java.util.Properties;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.spotify.docker.client.DockerException;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.ActiveMQHelper;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.server.PlatformSpecificUtil;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.ICloudWrapper;
import at.ac.tuwien.infosys.jcloudscale.vm.ICloudWrapperProvider;
import at.ac.tuwien.infosys.jcloudscale.vm.IIdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.VirtualHostPool;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DockerCloudPlatformConfiguration extends CloudPlatformConfiguration implements ICloudWrapperProvider {
	private static final long serialVersionUID = 1L;

	// BAD Default
	private String imageName;
	private String imageId;// if empty, detect by name. One of them should
							// not be null

	private String dockerHost;

	// Not really useful the following
	private String sshKey = "";// if empty -- don't attach anything.
	private String tenantName;
	private String login;
	private String password;

	private transient DockerWrapper dockerWrapper = null;

	private boolean removeContainers = false;

	// ----------------------------------------------------------------------

	/**
	 * Creates the new instance of <b>DockerCloudPlatformConfiguration</b> with
	 * default configuration. No-args constructor mainly needed for
	 * serialization. Openstack configuration at this point is not sufficient
	 * unless you specify <b>identityPublicURL</b>, <b>tenantName</b>,
	 * <b>login</b> and <b>password</b> additionally.
	 */
	public DockerCloudPlatformConfiguration() {
		// Force the log of docker-java to be the same of the configuration. By
		// default it is off.
		// org.shaded.apache.log4j.Logger.getLogger("org.shaded.apache.http").setLevel();
		// TODO Not sure where I can put this to avoid problem with log4j
		// configuration !
	}

	/*
	 * Required for CloudManager dynamic instantiation
	 */
	public DockerCloudPlatformConfiguration(Properties properties) {
		this();
		this.dockerHost = properties.getProperty("dockerHost", null);
		this.tenantName = properties.getProperty("tenantName", null);
		this.login = properties.getProperty("login", null);
		this.password = properties.getProperty("password", null);
		this.imageName = properties.getProperty("imageName", null);
	}

	/**
	 * Creates the new instance of <b>DockerCloudPlatformConfiguration</b> with
	 * the properties specified.
	 * 
	 * @param dockerHost
	 *            The URL that specifies the authentication server (
	 *            <b>OS_AUTH_URL</b> openstack property value).
	 * @param tenantName
	 *            The name of the tenant that can be used by this application (
	 *            <b>OS_TENANT_NAME</b> openstack property value).
	 * @param login
	 *            The login of the user that can be used by this application (
	 *            <b>OS_USERNAME</b> openstack property value).
	 * @param password
	 *            The password of the user that correspond for the specified
	 *            username (<b>OS_PASSWORD</b> openstack property value).
	 */
	public DockerCloudPlatformConfiguration(String dockerHost, String tenantName, String login, String password) {
		this();
		this.dockerHost = dockerHost;
		this.tenantName = tenantName;
		this.login = login;
		this.password = password;
	}

	/**
	 * Creates the new instance of <b>DockerCloudPlatformConfiguration</b> with
	 * the properties specified.
	 * 
	 * @param dockerHost
	 *            The URL that specifies the authentication server (
	 *            <b>OS_AUTH_URL</b> openstack property value).
	 * @param tenantName
	 *            The name of the tenant that can be used by this application (
	 *            <b>OS_TENANT_NAME</b> openstack property value).
	 * @param login
	 *            The login of the user that can be used by this application (
	 *            <b>OS_USERNAME</b> openstack property value).
	 * @param password
	 *            The password of the user that correspond for the specified
	 *            username (<b>OS_PASSWORD</b> openstack property value).
	 * @param imageName
	 *            The name of the image that should be used to start new virtual
	 *            hosts.
	 */
	public DockerCloudPlatformConfiguration(String dockerHost, String tenantName, String imageName, String login,
			String password) {

		this(dockerHost, tenantName, login, password);
		this.imageName = imageName;
	}

	// ---------------------------------------------------------------------------------------------

	public boolean isRemoveContainers() {
		return removeContainers;
	}

	String getDockerHost() {

		return dockerHost;
	}

	String getLogin() {

		return login;
	}

	String getPassword() {

		return password;
	}

	String getTenantName() {

		return this.tenantName;
	}

	synchronized String getImageId() {

		if (this.imageId == null || this.imageId.length() == 0)
			try {
				this.imageId = getDockerWrapper().lookupImageId(imageName);
			} catch (DockerException | InterruptedException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}

		return imageId;
	}

	String getSshKey() {

		return this.sshKey;
	}

	String getImageName() {

		return imageName;
	}

	/**
	 * Specifies the name of the image that will be used to start new instances
	 * in the Openstack Cloud.
	 * 
	 * @param imageName
	 *            The existing name of the image that new virtual hosts will be
	 *            spawned from.
	 */
	public void setImageName(String imageName) {

		this.imageName = imageName;
	}

	/**
	 * Specifies the id of the image that will be used to start new instances in
	 * the Openstack Cloud.
	 * 
	 * @param imageId
	 *            The existing id of the image that new virtual hosts will be
	 *            spawned from.
	 */
	public synchronized void setImageId(String imageId) {

		this.imageId = imageId;
	}

	// /**
	// * Specifies the type name of the new virtual hosts that should be
	// started.
	// * Either InstanceType or InstanceTypeId has to be specified.
	// *
	// * @param newInstanceType
	// * The type name (e.g., "m1.tiny") of the new virtual hosts that should be
	// used.
	// */
	// public void setNewInstanceType(String newInstanceType) {
	//
	// this.newInstanceType = newInstanceType;
	// }

	// /**
	// * Specifies the type id of the new virtual hosts that should be started.
	// * Either InstanceType or InstanceTypeId has to be specified.
	// *
	// * @param newInstanceTypeId
	// * The type id of the new virtual hosts that should be used.
	// */
	// public synchronized void setNewInstanceTypeId(String newInstanceTypeId) {
	//
	// this.newInstanceTypeId = newInstanceTypeId;
	// }

	/**
	 * Specifies the SSH key name that may be used to access the instance
	 * through SSH connection.
	 * 
	 * @param sshKeyName
	 *            The name of existing SSH key that will be attached to the new
	 *            virtual host to allow remote access.
	 */
	public void setSshKey(String sshKey) {

		this.sshKey = sshKey;
	}

	/**
	 * Specifies the URL of the authentication server (<b>OS_AUTH_URL</b>
	 * openstack property value).
	 * 
	 * @param identityPublicURL
	 *            The URL that specifies the authentication server.
	 */
	public void setDockerHost(String identityPublicURL) {

		this.dockerHost = identityPublicURL;
	}

	/**
	 * Specifies the name of the tenant that can be used by this application (
	 * <b>OS_TENANT_NAME</b> openstack property value).
	 * 
	 * @param tenantName
	 *            The name of the tenant to use.
	 */
	public void setTenantName(String tenantName) {

		this.tenantName = tenantName;
	}

	/**
	 * The login of the user that can be used by this application (
	 * <b>OS_USERNAME</b> openstack property value).
	 * 
	 * @param login
	 *            The string that represents the Openstack login of the user.
	 */
	public void setLogin(String login) {

		this.login = login;
	}

	/**
	 * The password of the user that correspond for the specified username (
	 * <b>OS_PASSWORD</b> openstack property value).
	 * 
	 * @param password
	 *            The string that represents the password of the specified user.
	 */
	public void setPassword(String password) {

		this.password = password;
	}

	// TODO Change this to dockerWrapper !
	synchronized void setOsWrapper(DockerWrapper dockerWrapper) {
		this.dockerWrapper = dockerWrapper;
	}

	// /**
	// * Specifies the image name that should be used to start new Message Queue
	// host.
	// * Either Image name of Image Id has to be specified.
	// *
	// * @param mqImageName
	// * The existing name of the image that can be used in case Message Queue
	// host has to be started.
	// */
	// public synchronized void setMqImageName(String mqImageName) {
	//
	// this.mqImageName = mqImageName;
	// }

	// /**
	// * Specifies the image id that should be used to start new Message Queue
	// host.
	// * Either Image name of Image Id has to be specified.
	// *
	// * @param mqImageId
	// * The existing id of the image that can be used in case Message Queue
	// host has to be started.
	// */
	// public synchronized void setMqImageId(String mqImageId) {
	//
	// this.mqImageId = mqImageId;
	// }

	// /**
	// * Specifies the instance type of the Message Queue host.
	// * This property is necessary to specify only when application is supposed
	// to start Message Queue server
	// * automatically.
	// * Either MqInstanceType or MqInstanceTypeId has to be specified.
	// *
	// * @param newMqInstanceType
	// * The type name (e.g., "m1.tiny") of the message queue host that should
	// be used.
	// */
	// public void setNewMqInstanceType(String newMqInstanceType) {
	//
	// this.newMqInstanceType = newMqInstanceType;
	// }

	// /**
	// * Specifies the instance type id of the Message Queue host.
	// * Either MqInstanceType or MqInstanceTypeId has to be specified.
	// * This property is necessary to specify only when application is supposed
	// to start Message Queue server
	// * automatically.
	// *
	// * @param newMqInstanceTypeId
	// * The type id of the message queue host that should be used.
	// */
	// public synchronized void setNewMqInstanceTypeId(String
	// newMqInstanceTypeId) {
	//
	// this.newMqInstanceTypeId = newMqInstanceTypeId;
	// }

	// ----------------------------------------------------------------------

	/**
	 * Specifies the name of the image that will be used to start new instances
	 * in the Openstack Cloud.
	 * 
	 * @param imageName
	 *            The existing name of the image that new virtual hosts will be
	 *            spawned from.
	 * @return The current instance of <b>DockerCloudPlatformConfiguration</b>
	 *         to continue configuration.
	 */
	public DockerCloudPlatformConfiguration withInstanceImage(String imageName) {

		setImageName(imageName);
		return this;
	}

	public DockerCloudPlatformConfiguration withDockerHost(String dockerHost) {

		setDockerHost(dockerHost);
		return this;
	}

	/**
	 * Specifies the SSH key name that may be used to access the instance
	 * through SSH connection.
	 * 
	 * @param sshKeyName
	 *            The name of existing SSH key that will be attached to the new
	 *            virtual host to allow remote access.
	 * @return The current instance of <b>DockerCloudPlatformConfiguration</b>
	 *         to continue configuration.
	 */
	public DockerCloudPlatformConfiguration withSshKey(String sshKeyName) {
		setSshKey(sshKeyName);
		return this;
	}

	@Override
	public IVirtualHost getVirtualHost(IIdManager idManager) {

		boolean startPerformanceMonitoring = JCloudScaleConfiguration.getConfiguration().common().monitoring()
				.isEnabled();

		return new DockerInstance(this, idManager, startPerformanceMonitoring);
	}

	@Override
	protected AutoCloseable startMessageQueueServer(MessageQueueConfiguration communicationConfiguration)
			throws Exception {

		String mqAddress = communicationConfiguration.getServerAddress();

		if ("localhost".equals(mqAddress) || PlatformSpecificUtil.isThisHostIPAddress(mqAddress)) {
			MessageQueueConfiguration mqConfig = communicationConfiguration.clone();
			mqConfig.setServerAddress("0.0.0.0");
			ActiveMQHelper mqServer = new ActiveMQHelper(mqConfig);
			mqServer.start();
			return mqServer;
		} else {

			String serverName = "MQ_" + UUID.randomUUID().toString();

			throw new RuntimeException(" getDockerWrapper().startNewMessageQeueHost(" + serverName + " , " + mqAddress
					+ ") not implemented !");
		}
	}

	@Override
	public VirtualHostPool getVirtualHostPool() {
		return new VirtualHostPool(this, getMessageQueueConfiguration());
	}

	// it's public only for tests. change test's package?
	public synchronized DockerWrapper getDockerWrapper() {

		if (this.dockerWrapper == null)
			this.dockerWrapper = new DockerWrapper(this);

		return this.dockerWrapper;
	}

	@Override
	public ICloudWrapper getCloudWrapper() {
		return (ICloudWrapper) getDockerWrapper();
	}
}
