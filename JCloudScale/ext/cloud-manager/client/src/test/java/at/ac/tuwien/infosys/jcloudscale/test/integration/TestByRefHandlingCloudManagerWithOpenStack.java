/*
   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.integration;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleShutdown;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.management.CloudManager;
import at.ac.tuwien.infosys.jcloudscale.test.util.CloudManagerConfigurationHelper;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.CloudManagerPlatformConfiguration;

public class TestByRefHandlingCloudManagerWithOpenStack
		extends at.ac.tuwien.infosys.jcloudscale.test.integration.base.TestByRefHandling {
	private static CloudManagerHelper staticInstanceManager;

	@BeforeClass
	public static void setup() throws Exception {
		//
		// Initializing JCloudScale
		//
		JCloudScaleConfiguration cfg = CloudManagerConfigurationHelper
				.createDefaultCloudManagerTestConfigurationForOpenStack()// .withLogging(Level.FINEST)
				.build();

		JCloudScaleClient.setConfiguration(cfg);

		//
		// Starting "static" hosts.
		//
		staticInstanceManager = new CloudManagerHelper(cfg);
		staticInstanceManager.startStaticInstances();

		cs = CloudManager.getInstance();
	}

	@AfterClass
	@JCloudScaleShutdown
	public static void tearDown() throws Exception {
		staticInstanceManager.shudownStaticInstances();
		// Stop the background process as well
		if (JCloudScaleClient.getConfiguration().server().cloudPlatform() instanceof CloudManagerPlatformConfiguration)
			((CloudManagerPlatformConfiguration) JCloudScaleClient.getConfiguration().server().cloudPlatform())
					.getCloudManagerWrapper().shutdown();
	}

	@After
	public void cleanup() throws Exception {
		for (UUID obj : new ArrayList<>(cs.getCloudObjects()))
			cs.destructCloudObject(obj);
	}

	/**
	 * FIXME This test when executed results in the next two tests to fail ! But
	 * If I redeclare the following tests like below, all tests pass ?
	 */
	@Test
	@Override
	public void testClassAsParameterAndResult() throws Exception {
		super.testClassAsParameterAndResult();
	}

	@Test
	@Override
	public void testConfiguredByValueParameter() throws Exception {
		super.testConfiguredByValueParameter();
	}

}
