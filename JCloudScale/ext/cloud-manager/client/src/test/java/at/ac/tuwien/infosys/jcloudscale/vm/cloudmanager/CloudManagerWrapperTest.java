package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;

import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.test.util.CloudManagerConfigurationHelper;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;

public class CloudManagerWrapperTest {

	@Test
	public void testCloudManagerServerStartUsingDockerCP() throws URISyntaxException, IOException {
		//
		// Initializing JCloudScale
		//
		JCloudScaleConfiguration cfg = CloudManagerConfigurationHelper
				.createDefaultCloudManagerTestConfigurationForDocker().withLogging(Level.FINEST)//
				.build();
		JCloudScaleClient.setConfiguration(cfg);

		CloudManagerWrapper wrapper = ((CloudManagerPlatformConfiguration) cfg.server().cloudPlatform())
				.getCloudManagerWrapper();

		//
		// wrapper.startNewHost("1");
		//
		// wrapper.shutdownHost("123.001.001.123");

		// This will shutdown the entire Cloud Manager
		wrapper.shutdown();

		// TODO - Check that there is no server running

	}
}
