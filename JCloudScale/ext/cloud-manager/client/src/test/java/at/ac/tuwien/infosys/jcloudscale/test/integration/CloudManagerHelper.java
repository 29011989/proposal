package at.ac.tuwien.infosys.jcloudscale.test.integration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.CloudManagerPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.CloudManagerWrapper;

public class CloudManagerHelper {
	private static final int STATIC_HOSTS_COUNT = 3;
	private static final long HOSTS_STARTUP_TIMEOUT = 10 * 60 * 1000;

	private List<String> staticHosts = new ArrayList<>();
	private CloudManagerWrapper wrapper;
	private MessageQueueConfiguration mqConfig;
	private CloudPlatformConfiguration cloudPlatformConfiguration;
	private AutoCloseable communicationServer;

	public CloudManagerHelper(JCloudScaleConfiguration config) {
		this.cloudPlatformConfiguration = config.server().cloudPlatform();
		this.mqConfig = config.common().communication();
		// This also start and creates everything
		wrapper = ((CloudManagerPlatformConfiguration) config.server().cloudPlatform()).getCloudManagerWrapper();
	}

	public void startStaticInstances() throws InterruptedException {

		System.out.println("Ensuring Communication Server is running...");
		try {
			communicationServer = this.cloudPlatformConfiguration.ensureCommunicationServerRunning();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to ensure communication server is running.", e);
		}

		//
		// starting static hosts.
		//
		try (IdManager idManager = new IdManager(mqConfig)) {
			int existingHosts = idManager.getRegisteredInstances().size();
			if (existingHosts > 0)
				System.out.println("Detected " + existingHosts + " cloud hosts running already!");

			if (existingHosts < STATIC_HOSTS_COUNT) {
				int newHostsCount = STATIC_HOSTS_COUNT - existingHosts;
				System.out.println("Starting " + newHostsCount + " new static hosts...");
				for (int i = 0; i < newHostsCount; ++i)
					wrapper.startNewHost(null);
			} else
				System.out.println("Using already running hosts.");

			long startTime = System.nanoTime();
			boolean timeout = false;
			while (idManager.getRegisteredInstances().size() < STATIC_HOSTS_COUNT) {
				if (TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) > HOSTS_STARTUP_TIMEOUT) {
					System.out.println("Timeout waiting for hosts to start. Cleaning up environment and aborting.");
					timeout = true;
					break;
				}
				System.out.println("Waiting for hosts to start...");
				Thread.sleep(5000);
			}

			//
			// Saving static hosts ips
			//
			for (UUID hostId : idManager.getRegisteredInstances())
				staticHosts.add(idManager.getIpToId(hostId));

			if (timeout) {
				shudownStaticInstances();
				throw new RuntimeException("Failed to start additional hosts within the configured timeout of "
						+ HOSTS_STARTUP_TIMEOUT + "ms!");
			}

			System.out.printf("Started successfully %s static hosts: %s.%n", staticHosts.size(),
					Arrays.toString(staticHosts.toArray()));
		}
	}

	public void shudownStaticInstances() {

		System.out.printf("Shutting down %s static hosts: %s.%n", staticHosts.size(),
				Arrays.toString(staticHosts.toArray()));
		for (String hostIp : staticHosts) {
			System.out.println("Shutting down host " + hostIp + "...");
			try {
				wrapper.shutdownHost(hostIp);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("Shutting down communication server...");
		if (communicationServer != null)
			try {
				communicationServer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

//	public boolean verifyWithOpenstack(IVirtualHost host, String instanceSize) {
//
//		String ip = host.getIpAddress();
//		String id = wrapper.findOpenStackIdByIP(ip);
//		String size = wrapper.getInstanceSize(id);
//		return instanceSize.equals(size);
//	}
}
