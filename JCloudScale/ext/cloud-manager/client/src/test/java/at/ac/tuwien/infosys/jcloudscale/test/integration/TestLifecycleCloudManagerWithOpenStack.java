/*
   Copyright 2016 Alessio Gambi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.integration;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.annotations.JCloudScaleShutdown;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.management.CloudManager;
import at.ac.tuwien.infosys.jcloudscale.test.util.CloudManagerConfigurationHelper;
import at.ac.tuwien.infosys.jcloudscale.vm.IHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHostPool;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.CloudManagerPlatformConfiguration;

public class TestLifecycleCloudManagerWithOpenStack
		extends at.ac.tuwien.infosys.jcloudscale.test.integration.base.TestLifecycle {

	private static CloudManagerHelper staticInstanceManager;

	@BeforeClass
	public static void setup() throws Exception {
		//
		// Initializing JCloudScale
		//
		JCloudScaleConfiguration cfg = CloudManagerConfigurationHelper
				.createDefaultCloudManagerTestConfigurationForOpenStack()// .withLogging(Level.FINEST)
				.build();

		JCloudScaleClient.setConfiguration(cfg);

		//
		// Starting "static" hosts.
		//
		staticInstanceManager = new CloudManagerHelper(cfg);
		staticInstanceManager.startStaticInstances();

		cs = CloudManager.getInstance();
	}

	@AfterClass
	@JCloudScaleShutdown
	public static void tearDown() throws Exception {
		staticInstanceManager.shudownStaticInstances();
		// Stop the background process as well
		if (JCloudScaleClient.getConfiguration().server().cloudPlatform() instanceof CloudManagerPlatformConfiguration)
			((CloudManagerPlatformConfiguration) JCloudScaleClient.getConfiguration().server().cloudPlatform())
					.getCloudManagerWrapper().shutdown();
	}

	@After
	public void cleanup() throws Exception {
		IVirtualHostPool pool = cs.getHostPool();
		for (IHost host : pool.getHosts())
			pool.shutdownHost(host.getId());
	}

	@Override
	@Test
	public void testObjectRemovedByClosedClientExplicit() throws Exception {
		// that's really not nice at all
		super.testObjectRemovedByClosedClientExplicit();

		// Shutdown the background process as well. This should work out of the
		// box if the actual JVM stops
		// Stop the background process as well
		if (JCloudScaleClient.getConfiguration().server().cloudPlatform() instanceof CloudManagerPlatformConfiguration)
			((CloudManagerPlatformConfiguration) JCloudScaleClient.getConfiguration().server().cloudPlatform())
					.getCloudManagerWrapper().shutdown();

		// At this point there should be nothing using port 1098 !

		TestLifecycleCloudManagerWithOpenStack.setup();
	}

}
