/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.test.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.CloudManagerPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.CloudManagerWrapperTest;

public class CloudManagerConfigurationHelper extends ConfigurationHelper {

	private static final String CLOUD_MANAGER_DOCKER_CONFIG_FILE = "cloud-manager-docker.props";
	private static final String CLOUD_MANAGER_OPENSTACK_CONFIG_FILE = "cloud-manager-openstack.props";

	private static final String SERVER_ADDRESS_CONFIG = "mq.address";

	public static JCloudScaleConfigurationBuilder createDefaultCloudManagerTestConfiguration(String customClasspath,
			String cloudManagerConfigFile, Properties cloudProperties) throws FileNotFoundException, IOException {
		CloudManagerPlatformConfiguration cloud = new CloudManagerPlatformConfiguration();

		// My Local Configurations - TODO Update this one
		Properties cloudManagerProperties = new Properties();
		try (InputStream propertiesStream = ClassLoader.getSystemResourceAsStream(cloudManagerConfigFile)) {
			cloudManagerProperties.load(propertiesStream);
		}

		String serverAddress = cloudManagerProperties.getProperty(SERVER_ADDRESS_CONFIG);

		if (serverAddress == null || serverAddress.length() == 0)
			throw new RuntimeException("Could not read ActiveMQ server address from the property \""
					+ SERVER_ADDRESS_CONFIG + "\" from the config file " + CLOUD_MANAGER_DOCKER_CONFIG_FILE);

		//
		cloud.setClasspath(customClasspath);
		cloud.setCloudProperties(cloudProperties);
		//
		return applyCommonConfiguration(new JCloudScaleConfigurationBuilder(cloud)//
				.withMQServerHostname(serverAddress));
						// Is the policy required ?! No it will fail the Migration and Static field test cases !
						//.with(new SingleHostScalingPolicy());

	}

	public static JCloudScaleConfigurationBuilder createDefaultCloudManagerTestConfigurationForDocker()
			throws URISyntaxException, IOException {
		// Classpath
		// TODO Note that the docker cp MUST INCLUDE THE JCS DOCKER ITSELF !
		java.net.URL url = CloudManagerWrapperTest.class.getResource("/docker-cp.txt");
		String dockerCP = new java.util.Scanner(new File(url.toURI()), "UTF8").useDelimiter("\\Z").next();
		// Cloud Properties
		Properties dockerProperties = new Properties();
		InputStream in = CloudManagerConfigurationHelper.class.getResourceAsStream("/docker.properties");
		dockerProperties.load(in);
		in.close();
		//
		System.out.println("CloudManagerConfigurationHelper.createDefaultCloudManagerTestConfigurationForDocker()\n"
				+ "" + dockerProperties);

		//
		return createDefaultCloudManagerTestConfiguration(dockerCP, CLOUD_MANAGER_DOCKER_CONFIG_FILE, dockerProperties);
	}

	public static JCloudScaleConfigurationBuilder createDefaultCloudManagerTestConfigurationForOpenStack()
			throws URISyntaxException, IOException {
		// Classpath
		// TODO Note that the docker cp MUST INCLUDE THE JCS DOCKER ITSELF !
		java.net.URL url = CloudManagerWrapperTest.class.getResource("/openstack-cp.txt");
		String dockerCP = new java.util.Scanner(new File(url.toURI()), "UTF8").useDelimiter("\\Z").next();
		// Cloud Properties
		Properties openstackProperties = new Properties();
		InputStream in = CloudManagerConfigurationHelper.class.getResourceAsStream("/openstack.properties");
		openstackProperties.load(in);
		in.close();
		//
		return createDefaultCloudManagerTestConfiguration(dockerCP, CLOUD_MANAGER_OPENSTACK_CONFIG_FILE,
				openstackProperties);
	}
}
