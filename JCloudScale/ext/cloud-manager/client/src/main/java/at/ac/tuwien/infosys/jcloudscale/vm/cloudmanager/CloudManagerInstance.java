/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager;

import at.ac.tuwien.infosys.jcloudscale.logging.Logged;
import at.ac.tuwien.infosys.jcloudscale.vm.IIdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalVM;

/**
 * TODO Not sure if here we need to implement something else other that start and stop...
 * FIXME Probably we must
 * @author gambi
 *
 */
@Logged
public class CloudManagerInstance extends LocalVM {

	public CloudManagerInstance(CloudManagerPlatformConfiguration config, IIdManager idManager,
			boolean startPerformanceMonitoring) {
		super(idManager, startPerformanceMonitoring);
		this.config = config;
	}

	@Override
	protected void launchHost(String size) {
		this.instanceSize = size;
		((CloudManagerPlatformConfiguration) config).getCloudManagerWrapper().startNewHost(size);
	}

	@Override
	public void close() {
		super.close();
		if (!isStaticHost())
			((CloudManagerPlatformConfiguration) config).getCloudManagerWrapper().shutdownHost(serverIp);
	}
	
	
	

}
