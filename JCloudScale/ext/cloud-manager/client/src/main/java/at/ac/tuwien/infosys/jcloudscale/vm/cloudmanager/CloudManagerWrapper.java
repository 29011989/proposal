/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.net.Socket;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.rmi.CloudManager;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.rmi.CloudManagerServer;

/**
 * This class just forward whatever command to the server. In essence it works
 * as client. Not sure if this is singleton though. It implements the
 * CloudManager (remote) interface.
 * 
 * @author gambi
 *
 */
public class CloudManagerWrapper {

	// TODO This must provide data for connecting to the background process
	// Plus someone shall start that background process as well, probably the
	// configuration. For the moment use localhost.
	private CloudManagerPlatformConfiguration config = null;
	private Logger log = null;

	// Background process acting as server. We use RMI on localhost to forward
	// commands and get back the results
	private CloudManager server;

	CloudManagerWrapper(CloudManagerPlatformConfiguration config) {
		this.config = config;
		this.log = JCloudScaleConfiguration.getLogger(this);

		// TODO NOT SURE HOW TO SET THIS UP !
		System.setProperty("java.security.policy", "src/main/resources/grant-all.policy");
		if (System.getSecurityManager() == null) {
			log.info("\nCreating local RMI security manager\n");
			System.setSecurityManager(new RMISecurityManager());
		}

		// Check if the required properties are there !?
		Properties properties = config.getCloudProperties();
		if (!properties.containsKey(CloudManagerServer.CLOUD_CONFIGURATION_CLASS)) {
			String msg = "Cloud Configuration Class property is missing";
			System.out.println("CloudManagerWrapper.CloudManagerWrapper() Properties:\n" + properties);
			throw new IllegalArgumentException(msg);
		}

		// return the port of the started JVM. It might happen that more than
		// one RMI registry is running or the port is not available.
		int rmiRegistryPort = startServerJVM();
		// // Maybe here is not started yet ?
		//
		// try {
		// Thread.sleep(1000);
		// } catch (InterruptedException e) {
		// }
		// // Start the client. TODO Do we really need security manager here?
		// //

		try {
			// Note that we cannot use the Standard RMI port beacuse ActiveMQ
			// requires that as well !
			Registry registry = LocateRegistry.getRegistry(rmiRegistryPort);
			server = (CloudManager) registry.lookup(CloudManagerServer.CLOUD_MANAGER_NAME);
			log.info("Cloud Manager Server: " + server);

			server.setup(config.getCloudProperties());

		} catch (Exception e) {
			try {
				System.err.println("CloudManager exception. Shutdown server");
				e.printStackTrace();
				server.shutdown();
			} catch (RemoteException e1) {
				System.err.println("CloudManager Failed to stop server. Exception: " + e1);
				e1.printStackTrace();
			}
			throw new RuntimeException(e);
		}
	}

	private String buildProcessBuilderCommandLine(ProcessBuilder pb) {

		StringBuilder builder = new StringBuilder();

		List<String> cmds = pb.command();
		if (cmds.size() > 0) {
			builder.append(cmds.get(0));
			for (int i = 1; i < cmds.size(); ++i) {
				builder.append(" ");
				builder.append(cmds.get(i));
			}
		}
		return builder.toString();
	}

	protected Process jvmProcess;
	private static final String HEAP_SIZE_COMMAND = "-Xmx";
	private static final String CLASSPATH_COMMAND = "-cp";

	/**
	 * Start the background JVM and register the shutdown hook to kill it
	 * ! </br>
	 * TODO Return the port
	 */
	private int startServerJVM() {
		File workingDir = new File(config.getCloudManagerStartupDirectory());

		try {
			// For even higher platform independency, we can use ant to start
			// new jvm.
			String javaPath = config.getJavaPath();

			ProcessBuilder pb = new ProcessBuilder(/* JAVA executable */javaPath,
					/* Class path of the server */CLASSPATH_COMMAND, config.getClasspath(),
					/* Server startup class */config.getServerStartupClass());

			// adding memory limit parameter
			if (config.getJavaHeapSizeMB() > 0)
				pb.command().add(1, HEAP_SIZE_COMMAND + config.getJavaHeapSizeMB() + "m");

			// add custom JVM parameters as defined by the user
			for (String arg : config.getCustomJVMArgs()) {
				pb.command().add(1, arg);
			}

			pb.directory(workingDir);

			// TODO: Not sure this is needed !
			// redirecting output to the same destination as of our process
			// (so that server will write output to client's console/error
			// stream.)
			pb.redirectOutput(Redirect.INHERIT);
			pb.redirectError(Redirect.INHERIT);

			if (log.isLoggable(Level.FINE))
				log.fine("Starting Cloud Manager with command line " + buildProcessBuilderCommandLine(pb)
						+ " in folder " + pb.directory().getCanonicalPath());

			jvmProcess = pb.start();

			try {
				Thread.sleep(400);
				int exitCode = jvmProcess.exitValue();
				String message = String.format(
						"Cloud Manager JVM failed to launch. Launch Line:%n'%s'%nfrom folder '%s'. Exit code %s",
						buildProcessBuilderCommandLine(pb), pb.directory().getCanonicalPath(), exitCode);

				// TODO Change this to something more specific ?
				throw new RuntimeException(message);
			} catch (IllegalThreadStateException | InterruptedException e1) {
				// ignore - This means it started
				// e1.printStackTrace();
			}

			// TAG: BETA. Why this does not require final ?
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					System.out.println("CloudManagerWrapper. " + jvmProcess);
					if (jvmProcess != null && isCloudManagerRunning()) {
						jvmProcess.destroy();
					}
				}
			});
			log.info("CloudManager Shut Down Hook Attached.");
			System.out.println("CloudManagerWrapper.startServerJVM(). CloudManager Shut Down Hook Attached.");

			// TODO For the moment just return the default port if everything
			// was fine
			return CloudManagerServer.CLOUD_MANAGER_RMI_PORT;

		} catch (IOException e) {
			throw new RuntimeException("Could not start Cloud Manager VM. Error message was: " + e.getMessage());
		}
	}

	private boolean isCloudManagerRunning() {
		try {
			jvmProcess.exitValue();
			return isCloudManagerPortBound();
		} catch (Exception e) {
			return true;
		}
	}

	private boolean isCloudManagerPortBound() {
		Socket s = null;
		try {
			s = new Socket("localhost", CloudManagerServer.CLOUD_MANAGER_RMI_PORT);
			return false;
		} catch (IOException e) {
			return true;
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					throw new RuntimeException("You should handle this error.", e);
				}
			}
		}
	}

	/**
	 * This might take a while so we necessarily must .waitFor() it
	 */
	public void shutdown() {
		try {
			// Notify the server. This will also kill the jvmProcess only if
			// there is nothing more running on it
			server.shutdown();

			// This should be able to kill the entire machinery
			if (jvmProcess != null && isCloudManagerRunning()) {
				jvmProcess.destroy();
				//
				int exitValue = jvmProcess.waitFor();
				//
				System.out.println("CloudManagerWrapper.shutdown() " + exitValue);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// -------------------------------------------------------------------------------

	public void startNewHost(String size) {
		// Do the Remote Call and capture interface
		try {
			server.startNewHost(size);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// TODO IMPLEMENT ME
	public void shutdownHost(String ip) {
		try {
			System.out.println("CloudManagerWrapper.shutdownHost() " + ip);
			server.shutdownHost(ip);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
