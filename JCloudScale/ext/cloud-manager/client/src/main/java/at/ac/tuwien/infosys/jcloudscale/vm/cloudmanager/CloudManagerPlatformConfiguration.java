/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.ActiveMQHelper;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.server.PlatformSpecificUtil;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IIdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.VirtualHostPool;
import at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.rmi.CloudManagerServer;

/**
 * TODO Ideally this one shall use the XML configuration provided to the main
 * app ! TODO How to wrap the conf there ? In other words, this conf shall point
 * to the actual conf to use, and pass that file to the background process at
 * startup. But for the moment we can also start y assuming that the XML
 * Configuration is provided directly to the background process instead...
 * 
 * TODO Customizaton can only be on the socket data (file/port/whatever)
 * 
 * @author gambi
 *
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CloudManagerPlatformConfiguration extends CloudPlatformConfiguration {
	private static final long serialVersionUID = 1L;

	// This one acts as client !
	private transient CloudManagerWrapper cloudManagerWrapper = null;

	public CloudManagerPlatformConfiguration() {

	}

	synchronized void setCloudManagerWrapper(CloudManagerWrapper cloudManagerWrapper) {

		this.cloudManagerWrapper = cloudManagerWrapper;
	}

	// it's public only for tests. change test's package?
	// This is basically a SINGLETON
	public synchronized CloudManagerWrapper getCloudManagerWrapper() {

		if (this.cloudManagerWrapper == null)
			this.cloudManagerWrapper = new CloudManagerWrapper(this);

		return this.cloudManagerWrapper;
	}
	// ----------------------------------------------------------------------

	private String startupDirectory = "";// emplty means use as current app.

	String getCloudManagerStartupDirectory() {
		if (startupDirectory == null || startupDirectory.length() == 0)
			startupDirectory = System.getProperty("java.io.tmpdir");
		return startupDirectory;
	}

	private String javaPath = "";// empty means use same as current app.

	String getJavaPath() {
		if (javaPath == null || javaPath.length() == 0)
			javaPath = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";

		return javaPath;
	}

	private String findCloudManagerServerJar(String[] appClassPath) {

		for (String elem : appClassPath)
			if (elem.contains("jcloudscale.cloud-manager-server"))
				return elem;

		return null;
	}

	private final static Pattern splitLibVersionPattern = Pattern.compile("(.+)\\-([0-9].*)");

	private String getLibWithoutVersion(String filename) {

		Matcher match = splitLibVersionPattern.matcher(filename);
		if (!match.matches())
			return filename;
		else
			return match.group(1);
	}

	// TODO What is this about ?
	private String collectClassPath(Logger log, String jcsJar, String[] appClassPath) {

		final String CLASS_PATH_KEY = "Class-Path";
		final String CLASS_PATH_SEPARATOR = " ";

		Map<String, String> nameToPathMap = new HashMap<>();
		Map<String, List<String>> nameToVersionsMap = new HashMap<>();
		for (String path : appClassPath) {
			File file = new File(path);
			if (file.isDirectory() || !file.exists())
				continue;

			String filename = file.getName();
			nameToPathMap.put(filename, file.getAbsolutePath());

			// coining out library name
			String libNameNoVersion = getLibWithoutVersion(filename);

			if (nameToVersionsMap.containsKey(libNameNoVersion))
				nameToVersionsMap.get(libNameNoVersion).add(file.getAbsolutePath());
			else {
				List<String> elems = new ArrayList<>();
				elems.add(file.getAbsolutePath());
				nameToVersionsMap.put(libNameNoVersion, elems);
			}
		}

		StringBuilder result = new StringBuilder();
		result.append(new File(jcsJar).getAbsolutePath());

		try (JarFile jar = new JarFile(jcsJar)) {
			Manifest mf = jar.getManifest();
			if (mf.getMainAttributes().getValue(CLASS_PATH_KEY) == null) {
				log.warning("\"" + CLASS_PATH_KEY + "\" property is missing from \"" + jcsJar + "\" manifest file.");
				return null;
			}

			for (String entry : mf.getMainAttributes().getValue(CLASS_PATH_KEY).split(CLASS_PATH_SEPARATOR)) {
				String libPath = null;

				// trying to find exact match.
				if (nameToPathMap.containsKey(entry))
					libPath = nameToPathMap.get(entry);
				else {// trying to find name-only match.
					List<String> libs = nameToVersionsMap.get(getLibWithoutVersion(entry));
					if (libs != null && libs.size() > 0) {
						log.warning("Failed to find exact classpath match for \"" + entry + "\". Using \"" + libs.get(0)
								+ "\" instead. If your application indeed needs later version of library, "
								+ "APPLICATION MAY BEHAVE DIFFERENTLY in real cloud environment.");
						libPath = libs.get(0);
					}
				}

				if (libPath == null) {
					log.warning("Failed to find classpath entry \"" + entry + "\" on application classpath. "
							+ "You may have issues starting virtual machines in local testing setup.");
					return null;
				}

				result.append(File.pathSeparator).append(libPath);
			}
		} catch (IOException e) {
			log.warning("Failed to collect classpath elements from " + jcsJar + ": " + e);
			return null;
		}

		return result.toString();
	}

	// TODO FixMe: Point directly to lib folder vs MVN repository ?!
	// This one is tricky. For the moment we refer to a CP provided by the user
	// as configuration parameter
	private String generateServerClassPath() {

		Logger log = JCloudScaleConfiguration.getLogger(this);
		String appClassPath = System.getProperty("java.class.path");
		String resultClassPath = null;

		try {
			if (appClassPath != null && appClassPath.length() > 0) {
				String[] appClassPathElements = appClassPath.split(File.pathSeparator);

				String cloudManagerJar = findCloudManagerServerJar(appClassPathElements);
				if (cloudManagerJar == null)
					log.warning("Failed to find Cloud Manager jar on classpath.");
				else {
					// TODO: Not sure this is OK
					resultClassPath = cloudManagerJar;

					// resultClassPath = collectClassPath(log, cloudManagerJar,
					// appClassPathElements);
					if (resultClassPath != null)
						log.fine("Collected Successfully Cloud Manager Classpath for " + cloudManagerJar);
				}
			}
		} catch (Exception ex) {
			log.warning("Failed to collect Cloud Manager-only classpath for local vm startup: " + ex);
			resultClassPath = null;
		}

		if (resultClassPath == null) {
			log.warning("JCloudScale failed to coin out necessary libraries for Cloud Manager to start. "
					+ "Thus, complete application classpath will be used for local testing.");
			log.warning(
					"For thorough testing, consider changing local platform configuration to specify server classpath manually.");
			resultClassPath = null;
		}

		return resultClassPath;
	}

	private String classpath = "";// empty means use same as current app.

	public void setClasspath(String classpath) {
		this.classpath = classpath;
	}

	// Customization properties for the cloud
	private Properties cloudProperties = new Properties();

	public void setCloudProperties(Properties cloudProperties) {
		this.cloudProperties.clear();
		this.cloudProperties.putAll(cloudProperties);
	}

	public Object addCloudProperty(String key, String value) {
		return this.cloudProperties.put(key, value);
	}

	public Object removeCloudProperty(String key) {
		return this.cloudProperties.remove(key);
	}

	public Properties getCloudProperties() {
		return cloudProperties;
	}

	/*
	 * we expect users to specify it ";"-separated, as it allows us to keep it
	 * platform-independent.
	 */
	String getClasspath() {

		if (classpath == null || classpath.length() == 0)
			throw new RuntimeException("You must define the Cloud Backend classpath !");

		// Append the Actual Cloud Manager Server Jars
		String cloudManagerClassPath = generateServerClassPath();

		classpath = classpath + ((cloudManagerClassPath != null) ? ';' + generateServerClassPath() : " ");
		return classpath.replace(';', File.pathSeparatorChar);
	}

	private String serverStartupClass = CloudManagerServer.class.getName();

	String getServerStartupClass() {
		return serverStartupClass;
	}

	private long javaHeapSizeMB = 0;// zero is "use default"

	public void setJavaHeapSizeMB(long javaHeapSizeMB) {
		this.javaHeapSizeMB = javaHeapSizeMB;
	}

	long getJavaHeapSizeMB() {
		return javaHeapSizeMB;
	}

	private List<String> customJVMArgs = new ArrayList<>();

	public List<String> getCustomJVMArgs() {
		return customJVMArgs;
	}

	public void addCustomJVMArgs(String arg) {
		customJVMArgs.add(arg);
	}

	// ----------------------------------------------------------------------

	@Override
	public IVirtualHost getVirtualHost(IIdManager idManager) {

		boolean startPerformanceMonitoring = JCloudScaleConfiguration.getConfiguration().common().monitoring()
				.isEnabled();

		return new CloudManagerInstance(this, idManager, startPerformanceMonitoring);
	}

	@Override
	protected AutoCloseable startMessageQueueServer(MessageQueueConfiguration communicationConfiguration)
			throws Exception {

		String mqAddress = communicationConfiguration.getServerAddress();

		if ("localhost".equals(mqAddress) || PlatformSpecificUtil.isThisHostIPAddress(mqAddress)) {
			MessageQueueConfiguration mqConfig = communicationConfiguration.clone();
			mqConfig.setServerAddress("0.0.0.0");
			ActiveMQHelper mqServer = new ActiveMQHelper(mqConfig);
			mqServer.start();
			return mqServer;
		} else {

			String serverName = "MQ_" + UUID.randomUUID().toString();

			throw new RuntimeException(" getDockerWrapper().startNewMessageQeueHost(" + serverName + " , " + mqAddress
					+ ") not implemented !");
		}
	}

	@Override
	public VirtualHostPool getVirtualHostPool() {
		return new VirtualHostPool(this, getMessageQueueConfiguration());
	}

}
