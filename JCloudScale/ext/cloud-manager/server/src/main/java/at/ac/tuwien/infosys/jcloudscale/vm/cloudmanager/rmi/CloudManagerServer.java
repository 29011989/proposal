package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.rmi;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnmarshalException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.ICloudWrapperProvider;
import at.ac.tuwien.infosys.jcloudscale.vm.JCloudScaleClient;

/**
 * This is a singleton class that implements the cloud manager server: it waits
 * for commands to start/stop Cloud host on a local socket, sends them to the
 * cloud, gets results back and reply to the client.
 * 
 * TODO Check: http://stackoverflow.com/questions/9307764/localhost-only-rmi
 * 
 * 
 * @author gambi
 *
 */
public class CloudManagerServer implements CloudManager {

	public static final String CLOUD_MANAGER_NAME = "CloudManager";
	public static final String CLOUD_CONFIGURATION_CLASS = "cloudConfigurationClass";
	/*
	 * Non standard port because ActiveMQ requires the standard one !
	 */
	public static final int CLOUD_MANAGER_RMI_PORT = 1098;

	private CloudPlatformConfiguration config = null;

	private final Logger logger = Logger.getLogger(CloudManagerServer.class.getName());

	private Registry registry;

	public CloudManagerServer(Registry registry) throws RemoteException {
		super();
		this.registry = registry;
		//
		setup();
	}

	void setup() throws RemoteException {
		CloudManager stub = (CloudManager) UnicastRemoteObject.exportObject(this, 0);
		registry.rebind(CloudManagerServer.CLOUD_MANAGER_NAME, stub);
		System.out.println("CloudManager Object Bound");
		logger.info("CloudManager Object Bound");
	}

	// TODO Move into constructor ?
	// void setRegistry(Registry registry) {
	// this.registry = registry;
	// }

	// Only or testing
	Registry getRegistry() {
		return registry;
	}

	// Only or testing
	CloudPlatformConfiguration getConfig() {
		return config;
	}

	@Override
	public void startNewHost(String size) throws RemoteException {
		System.out.println("CloudManagerServer.startNewHost() " + size);
		// TODO Get to the wrapper here
		((ICloudWrapperProvider) config).getCloudWrapper().startNewHost(size);
	}

//	@Override
//	public void shutdownHost(UUID hostId) throws RemoteException {
//		System.out.println("CloudManagerServer.shutdownHost() " + hostId);
//		((ICloudWrapperProvider) config).getCloudWrapper().shutdownHost(hostId);
//	}

	@Override
	public void shutdownHost(String ip) throws RemoteException {
		System.out.println("CloudManagerServer.shutdownHost() " + ip);
		((ICloudWrapperProvider) config).getCloudWrapper().shutdownHost(ip);
	}

	// --------------------------------------------------------------------
	@Override
	public void setup(Properties properties) {
		Class<?> configurationClass;
		try {
			configurationClass = Class.forName(properties.getProperty(CLOUD_CONFIGURATION_CLASS));
			Constructor<?> cons = configurationClass.getConstructor(Properties.class);

			config = (CloudPlatformConfiguration) cons.newInstance(properties);

			// TODO Forward additional configuration options here ?

			JCloudScaleConfigurationBuilder builder = new JCloudScaleConfigurationBuilder(config);
			//
			if (properties.containsKey("mq.address"))
				builder.withMQServerHostname(properties.getProperty("mq.address"));
			//
			// FIXME Not sure about this one ! The problem is that logging
			// raises a NPE otherwise !
			JCloudScaleClient.setConfiguration(builder.build());

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	@Override
	public void shutdown() throws RemoteException {
		System.out.println("CloudManagerServer.shutdown()");
		try {
			registry.unbind(CLOUD_MANAGER_NAME);
			UnicastRemoteObject.unexportObject(this, true);
			logger.info("Server shut down gracefully");
		} catch (NotBoundException e) {
			logger.severe("Error shutting down the server - could not unbind the registry ");
			e.printStackTrace();
		} catch (AccessException e) {
			logger.severe("Error shutting down the server - Access Exception");
			e.printStackTrace();
		} catch (UnmarshalException e) {
			logger.severe("Error shutting down the server - UnMarshall Exception");
			e.printStackTrace();
		} catch (RemoteException e) {
			logger.severe("Error shutting down the server - Remote Exception");
			e.printStackTrace();
		}
	}

	// Configurations are passed here somehow ?
	// CloudBackend Configuration
	// Non Standard RMI Port
	// https://docs.oracle.com/javase/tutorial/rmi/implementing.html
	public static void main(String[] args) {
		// TODO NOT SURE HOW TO SET THIS UP !
		ClassLoader cl = CloudManagerServer.class.getClassLoader();
		URL policyURL = cl.getResource("grant-all.policy");
		System.setProperty("java.security.policy", policyURL.toString());
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		// Setup RMI Registry
		Registry registry = null;
		try {
			int port = (args.length > 0) ? Integer.parseInt(args[0]) : CLOUD_MANAGER_RMI_PORT;
			registry = LocateRegistry.createRegistry(port);
			System.out.println("CloudManagerServerRunner.main() Started Registry at " + port);
		} catch (RemoteException re) {
			re.printStackTrace();
			System.exit(1);
		} catch (Throwable e) {
			// TODO: handle exception
			e.printStackTrace();
			System.exit(1);
		}
		// Start the Cloud Manager Server
		try {
			System.out.println("CloudManagerServerRunner.main() Setting up CloudManager");
			// This starts and bounds the object to the RMI Registry
			CloudManagerServer manager = new CloudManagerServer(registry);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}
}
