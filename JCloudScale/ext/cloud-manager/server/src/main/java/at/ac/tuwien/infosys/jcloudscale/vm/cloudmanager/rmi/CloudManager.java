package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Properties;

public interface CloudManager extends Remote {

	// Virtual Host Pool
	public void startNewHost(String size) throws RemoteException;

	// Not sure which one is the correct one tought
//	public void shutdownHost(UUID hostId) throws RemoteException;
	
	
	public void shutdownHost(String ip) throws RemoteException;

	/**
	 * Stop the background process
	 * 
	 * @throws RemoteException
	 */
	public void shutdown() throws RemoteException;

	/**
	 * Setup the cloud manager with a given specification. For the moment, we
	 * rely on the user to provide the right class name and various
	 * configuration parameters as properties.
	 * 
	 * @param properties
	 */
	public void setup(Properties properties) throws RemoteException;

}
