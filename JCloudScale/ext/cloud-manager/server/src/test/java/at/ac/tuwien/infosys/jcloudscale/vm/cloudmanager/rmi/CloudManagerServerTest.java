package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.rmi;

import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CloudManagerServerTest {

	private Registry registry;
	private CloudManagerServer manager;

	@Before
	public void setupRMIRegistry() throws RemoteException {
		// TODO NOT SURE HOW TO SET THIS UP !
		System.setProperty("java.security.policy", "src/main/resources/grant-all.policy");

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}

		// Setup RMI Registry
		int port = 1099;
		registry = LocateRegistry.createRegistry(1099);
		System.out.println("CloudManagerServerRunner.main() Started Registry at " + port);
	}

	@Test
	public void startAndStopServerTest() throws RemoteException, NotBoundException {
		// Start the Cloud Manager Server

		System.out.println("CloudManagerServerRunner.main() Setting up CloudManager");
		manager = new CloudManagerServer(registry);
		//
		CloudManager stub = (CloudManager) UnicastRemoteObject.exportObject(manager, 0);
		registry.rebind(CloudManagerServer.CLOUD_MANAGER_NAME, stub);
		System.out.println("CloudManager bound");

		// Get a reference to the Remote Object
		CloudManager remoteManager = (CloudManager) registry.lookup(CloudManagerServer.CLOUD_MANAGER_NAME);
		System.out.println("CloudManagerServerTest.startAndStopServerTest() " + remoteManager);

		// Call some method on the remote object - TODO assertion
		remoteManager.startNewHost("1");

		// Stop the Cloud Manager Server using the remote object
		System.out.println("CloudManagerServerRunner.main() Shutting down CloudManager");
		remoteManager.shutdown();

		// Check Object is gone
		try {
			registry.lookup(CloudManagerServer.CLOUD_MANAGER_NAME);
			Assert.fail("NotBoundException not raised but expected");
		} catch (NotBoundException e) {
			// OK
		} catch (Exception e) {
			Assert.fail("Wrong exception ! " + e);
		}

		//
		try {
			remoteManager.startNewHost("1");
			Assert.fail("Expected exception not raised !");
		} catch (java.rmi.NoSuchObjectException e) {
			// OK
		} catch (Exception e) {
			Assert.fail("Wrong exception ! " + e);
		}
	}
}
