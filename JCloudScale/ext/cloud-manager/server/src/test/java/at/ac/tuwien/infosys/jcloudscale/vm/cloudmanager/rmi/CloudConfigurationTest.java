package at.ac.tuwien.infosys.jcloudscale.vm.cloudmanager.rmi;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.vm.ICloudWrapper;
import at.ac.tuwien.infosys.jcloudscale.vm.ICloudWrapperProvider;

public class CloudConfigurationTest {

	private static Registry registry;
	// CUT
	private CloudManagerServer manager;

	@BeforeClass
	public static void setupRMIRegistry() throws RemoteException {
		// TODO NOT SURE HOW TO SET THIS UP !
		System.setProperty("java.security.policy", "src/main/resources/grant-all.policy");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}

		// Setup RMI Registry
		int port = 1099;
		registry = LocateRegistry.createRegistry(1099);
		System.out.println("CloudManagerServerRunner.main() Started Registry at " + port);
	}

	@AfterClass
	public static void stopRMIRegistry() {
		// TODO Not sure there is a way to kill the registry
	}

	@Before
	public void setup() throws RemoteException {
		manager = new CloudManagerServer(registry);
	}

	@After
	public void tearDown() throws RemoteException {
		manager.shutdown();
	}

	@Test
	public void testDockerCloudConfiguration() throws NotBoundException, IOException {
		// Get a reference to the Remote Object
		CloudManager remoteManager = (CloudManager) registry.lookup(CloudManagerServer.CLOUD_MANAGER_NAME);
		System.out.println("Cloud Manager " + remoteManager);
		Properties dockerProperties = new Properties();

		InputStream in = getClass().getResourceAsStream("/docker.properties");
		dockerProperties.load(in);
		in.close();
		//
		remoteManager.setup(dockerProperties);
	}

	@Test
	public void testStartDockerContainer() throws NotBoundException, IOException {
		// Get a reference to the Remote Object
		CloudManager remoteManager = (CloudManager) registry.lookup(CloudManagerServer.CLOUD_MANAGER_NAME);
		System.out.println("Cloud Manager " + remoteManager);
		Properties dockerProperties = new Properties();

		InputStream in = getClass().getResourceAsStream("/docker.properties");
		dockerProperties.load(in);
		in.close();
		//
		remoteManager.setup(dockerProperties);
		//
		remoteManager.startNewHost(null);
		//
		// TODO Check the host started
		// Is this ok even if we setup the manager using remoteManager?
		// Shall we reinvoke setup?
		ICloudWrapper wrapper = ((ICloudWrapperProvider) manager.getConfig()).getCloudWrapper();

		List<String> hostsIp = wrapper.listCloudHosts();
		Assert.assertEquals(1, hostsIp.size());

		// Shutdown the container
		for (String ip : hostsIp)
			remoteManager.shutdownHost(ip);

	}
}
