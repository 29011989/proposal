package at.ac.tuwien.infosys.jcloudscale.utility;

public class ExceptionHelper {
	
	public static void throwException(Throwable ex) {
		ExceptionHelper.<RuntimeException>throwAnyException(ex);
	}
	
	@SuppressWarnings("unchecked")
	private static <T extends Throwable> void throwAnyException(Throwable ex) throws T{
		throw (T) ex;
	}
}
