package at.ac.tuwien.infosys.jcloudscale.monitoring.providers;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.UUID;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.CPUEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.NetworkEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.NetworkEvent.InterfaceStats;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.RAMEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.StateEvent;

/**
 * @author RST Monitoring Provider that always works. We need it to be able to
 *         get SOME values anyways.
 */
public class FallbackMonitoringProvider implements IMonitoringProvider {

    protected UUID hostId;
    protected Runtime runtime;
    protected OperatingSystemMXBean mxBean;
    protected String osVersion;
    protected String cpuArchitecture;

    public FallbackMonitoringProvider(UUID hostId) {
        this.hostId = hostId;
        mxBean = ManagementFactory.getOperatingSystemMXBean();
        runtime = Runtime.getRuntime();
        this.osVersion = this.mxBean.getName() + ", " + this.mxBean.getVersion();
        this.cpuArchitecture = System.getProperty("os.arch");
    }

    protected void setBaseEventProperties(StateEvent event) {
        // frankly speaking, these all fields should be set in event
        // constructor.
        event.setHostId(hostId);
        //TODO: if this is a huge overhead,maybe we can switch for incremental ids?
        event.setEventId(UUID.randomUUID());
        event.setTimestamp(System.currentTimeMillis());
        event.setEventProvider(this.getClass().getSimpleName());
    }

    @Override
    public CPUEvent measureCpuEvent() {
        CPUEvent event = new CPUEvent();
        setBaseEventProperties(event);

        event.setOsVersion(this.osVersion);
        event.setCpuArchitecture(this.cpuArchitecture);
        
        event.setCpuCount(this.mxBean.getAvailableProcessors());

        //the system load average; or a negative value if not available.
        double systemLoadAverage = this.mxBean.getSystemLoadAverage();
        event.setSystemLoadAverage(systemLoadAverage < 0 ? MonitoringProviderFactory.UNDEFINED_DOUBLE : systemLoadAverage);

        event.setSystemCpuLoad(MonitoringProviderFactory.UNDEFINED_DOUBLE);
        event.setProcessCpuLoad(MonitoringProviderFactory.UNDEFINED_DOUBLE);
        event.setProcessCpuTime(MonitoringProviderFactory.UNDEFINED_INTEGRAL);

        return event;
    }

    @Override
    public RAMEvent measureRamEvent() {
        RAMEvent event = new RAMEvent();
        setBaseEventProperties(event);

        event.setProcessFree(runtime.freeMemory());
        event.setProcessUsed(runtime.totalMemory() - event.getProcessFree());
        event.setProcessMaxAllowed(runtime.maxMemory());

        event.setSystemCommitted(MonitoringProviderFactory.UNDEFINED_INTEGRAL);
        event.setSystemFree(MonitoringProviderFactory.UNDEFINED_INTEGRAL);
        event.setSystemUsed(MonitoringProviderFactory.UNDEFINED_INTEGRAL);
        event.setSystemTotalRam(MonitoringProviderFactory.UNDEFINED_INTEGRAL);

        event.setSystemFreeSwap(MonitoringProviderFactory.UNDEFINED_INTEGRAL);
        event.setSystemUsedSwap(MonitoringProviderFactory.UNDEFINED_INTEGRAL);
        event.setSystemMaxSwap(MonitoringProviderFactory.UNDEFINED_INTEGRAL);

        return event;
    }

    @Override
    public NetworkEvent measureNetworkEvent() {
        NetworkEvent event = new NetworkEvent();
        setBaseEventProperties(event);
        try {

            for (NetworkInterface iface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                String name = iface.getName();

                long speed = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long recv_bytes = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long recv_packets = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long recv_errs = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long recv_drop = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long trans_bytes = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long trans_packets = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long trans_errs = MonitoringProviderFactory.UNDEFINED_INTEGRAL;
                long trans_drop = MonitoringProviderFactory.UNDEFINED_INTEGRAL;

                event.addInterface(new InterfaceStats(name, speed, recv_bytes, recv_packets, recv_errs, recv_drop,
                        trans_bytes, trans_packets, trans_errs, trans_drop));
            }
        } catch (SocketException ex) {
            throw new RuntimeException("Failed to obtain interface information.", ex);
        }

        return event;
    }

    @Override
    public void close() {
        this.mxBean = null;
        this.runtime = null;
    }

}
