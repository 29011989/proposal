package at.ac.tuwien.infosys.jcloudscale.migration;

import java.util.UUID;

import at.ac.tuwien.infosys.jcloudscale.vm.IHost;
import at.ac.tuwien.infosys.jcloudscale.vm.IHostPool;

public interface IMigrationEnabledHostPool extends IHostPool {
	
	void migrateObject(UUID object, IHost targetHost);
	void migrateObjectAsync(UUID object, IHost targetHost);
	void migrateObjectAsync(UUID object, IHost targetHost, IObjectMigratedCallback callback);
	
    public interface IObjectMigratedCallback
    {
        void migrationFinished();
    }
	
}
