/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.utility;

import java.lang.reflect.Method;

import at.ac.tuwien.infosys.jcloudscale.annotations.Local;
import at.ac.tuwien.infosys.jcloudscale.classLoader.RemoteClassLoaderUtils;
import at.ac.tuwien.infosys.jcloudscale.exception.JCloudScaleException;
import at.ac.tuwien.infosys.jcloudscale.management.references.JCloudScaleReference;
import at.ac.tuwien.infosys.jcloudscale.management.references.JCloudScaleReferenceManager;
import at.ac.tuwien.infosys.jcloudscale.server.ServerCallbackManager;
import net.sf.cglib.core.CodeGenerationException;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class CgLibUtil {
	
	public static Object[] replaceRefsWithProxies(Object[] objectParams, ClassLoader classloader) 
			throws Throwable {
		
		Object[] proxies = new Object[objectParams.length];
		for(int i=0; i<objectParams.length; i++) {
			proxies[i] = replaceRefWithProxy(objectParams[i], classloader);
		}
		
		return proxies; 
	}
	
	public static Object replaceRefWithProxy(Object object, ClassLoader classloader) 
			throws Throwable {
		
		if(object == null)
			return null;
	
		if(object.getClass().equals(JCloudScaleReference.class)) {
			
			JCloudScaleReference ref = (JCloudScaleReference) object;
			
			Class<?> loadedClazz = RemoteClassLoaderUtils.getClass(ref.getReferenceObjectClassName(), classloader);
			
			Object proxy = createReferenceProxy(loadedClazz, ref, classloader);
			
			JCloudScaleReferenceManager.getInstance().registerReference(ref, proxy);
			
			return proxy;
			
		} else {
			return object;
		}
		
	}
	
	private static Object createReferenceProxy(Class<?> templateClass, JCloudScaleReference ref, ClassLoader classloader) throws Throwable {
		
		try {
			return Enhancer.create(templateClass, new JCloudScaleReferenceHandler(ref, classloader));
		} catch(IllegalArgumentException e) {
			// this will happen if no no-arg constructor is present
			throw new JCloudScaleException(
				"Class "+templateClass.getName()+" is used as by-reference parameter but does not contain a no-arg constructor");
		} catch(CodeGenerationException e) {
			// we are not particularly interested in the CodeGenerationException, more in what caused it
			if(e.getCause() != null)
				throw e.getCause();
			else
				throw e;
		}
	    
	}
	
	public static boolean isOverwriteableMethod(Method method) {
		
		// method should not be overridden if it is a finalize method
		// or annotated with @Local
		
		String mName = method.getName();
		if(mName.equals("finalize") && method.getParameterTypes().length == 0)
			return false;
		else {
			return (method.getAnnotation(Local.class) == null);
		}
	}
	
	public static boolean isCGLibEnhancedClass(Class<?> clazz) {
		return (clazz.getName().contains("EnhancerByCGLIB"));
	}

	private static class JCloudScaleReferenceHandler implements MethodInterceptor {
		
		private ClassLoader classloader;
		private JCloudScaleReference ref;
		
		public JCloudScaleReferenceHandler(JCloudScaleReference ref, ClassLoader classloader) {
			this.ref = ref;
			this.classloader = classloader;
		}
		
		@Override
		public Object intercept(Object o, Method method, Object[] params, MethodProxy proxy) throws Throwable {
			
			if(!isOverwriteableMethod(method))
				return proxy.invokeSuper(o, params);
			
			ServerCallbackManager callbackManager = ServerCallbackManager.getInstance();
			Object response = callbackManager.callback(ref, method, params, method.getParameterTypes(), classloader);
			return response;
			
		}

	}
	
}
