package at.ac.tuwien.infosys.jcloudscale.monitoring.providers;

import java.util.UUID;

import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.NetInterfaceStat;
import org.hyperic.sigar.OperatingSystem;
import org.hyperic.sigar.ProcCpu;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.SigarNotImplementedException;
import org.hyperic.sigar.Swap;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.CPUEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.NetworkEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.NetworkEvent.InterfaceStats;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.RAMEvent;

public class SigarMonitoringProvider extends FallbackMonitoringProvider {

    private Sigar sigar;
    private boolean tryLoadAverage = true;
    private int cpuCount;
    private long pid;

    public SigarMonitoringProvider(UUID hostId) {
    	super(hostId);
        try {
            Sigar.load();
            sigar = new Sigar();
            
            // collecting some constant information that does not change over time.
            OperatingSystem osInfo = getOperatingSystemInfo();
            osVersion = String.format("%s, %s (%s)", osInfo.getName(), osInfo.getVersion(), osInfo.getDescription());
            cpuArchitecture = osInfo.getArch();
            cpuCount = countCpus();
            pid = sigar.getPid();
            
            // trying to measure things to ensure that everything indeed works
            measureCpuEvent();
            measureRamEvent();
            measureNetworkEvent();
        } catch (SigarException ex) {
            throw new RuntimeException("Unable to initialize Sigar libraries: " + ex);
        }
    }

    private int countCpus() throws SigarException 
    {
        int cpuCount = 0;
        CpuInfo[] cpuInfos = sigar.getCpuInfoList();
        if (cpuInfos == null || cpuInfos.length == 0)
            throw new RuntimeException("Failed to obtain CPU Info from Sigar.");
//        //The problem is that we receive an instance of the CpuInfo for each core, but with information for whole cpu.
//        for(CpuInfo cpu : cpuInfos)
//            cpuCount+= cpu.getTotalCores();
        
        cpuCount = cpuInfos[0].getTotalCores();
        
        return cpuCount;
    }

    private OperatingSystem getOperatingSystemInfo() throws SigarException 
    {
        OperatingSystem os = OperatingSystem.getInstance();
        os.gather(sigar);
        return os;
    }

    @Override
    public CPUEvent measureCpuEvent() {
        CPUEvent event = super.measureCpuEvent();
        try {
            CpuPerc cpuPerc = sigar.getCpuPerc();
            if (cpuPerc == null)
                throw new RuntimeException("Failed to obtain CPU Percentage usage");

            double loadAverage = MonitoringProviderFactory.UNDEFINED_DOUBLE;
            if (tryLoadAverage) {
                try {
                    double[] loadAverages = sigar.getLoadAverage();
                    if (loadAverages == null || loadAverages.length == 0)
                        throw new RuntimeException("Failed to obtain CPU load averages");

                    loadAverage = loadAverages[0];// TODO: should we average?

                } catch (SigarNotImplementedException ex) {
                    tryLoadAverage = false;
                }
            }
            
            ProcCpu processCpuInfo = sigar.getProcCpu(pid);
            
            event.setCpuCount(this.cpuCount);

            event.setProcessCpuLoad(processCpuInfo.getPercent());
            event.setProcessCpuTime(processCpuInfo.getTotal());
            event.setSystemCpuLoad(cpuPerc.getCombined());
            if(!Double.isNaN(loadAverage))
            	event.setSystemLoadAverage(loadAverage);

            return event;
        } catch (SigarException ex) {
            throw new RuntimeException("Failed to obtain CPU measurements.", ex);
        }
    }

    @Override
    public RAMEvent measureRamEvent() {
        RAMEvent event = super.measureRamEvent();
        try {
            Mem memory = sigar.getMem();
            Swap swap = sigar.getSwap();

            if (memory == null || swap == null)
                throw new RuntimeException("Failed to RAM measurements.");

            event.setSystemFree(memory.getFree());
            event.setSystemUsed(memory.getUsed());
            event.setSystemCommitted(memory.getTotal());
            event.setSystemTotalRam(memory.getRam()*1024*1024);//the actual value is in MB.
            
            event.setSystemFreeSwap(swap.getFree());
            event.setSystemMaxSwap(swap.getTotal());
            event.setSystemUsedSwap(swap.getUsed());

            return event;
        } catch (SigarException ex) {
            throw new RuntimeException("Failed to obtain RAM measurements.", ex);
        }
    }

    @Override
    public NetworkEvent measureNetworkEvent() {
        NetworkEvent event = new NetworkEvent();
        setBaseEventProperties(event);

        try {
            String[] nics = sigar.getNetInterfaceList();
            for (String nic : nics) {
                final NetInterfaceStat nicstat = sigar.getNetInterfaceStat(nic);

                event.addInterface(new InterfaceStats(nic, nicstat.getSpeed(), nicstat.getRxBytes(), nicstat
                        .getRxPackets(), nicstat.getRxErrors(), nicstat.getRxDropped(), nicstat.getTxBytes(), nicstat
                        .getTxPackets(), nicstat.getTxErrors(), nicstat.getTxDropped()));
            }

            return event;
        } catch (SigarException ex) {
            throw new RuntimeException("Failed to obtain Network measurements.", ex);
        }
    }

    @Override
    public void close() {
        if (sigar != null) {
            sigar.close();
            sigar = null;
        }
        super.close();
    }

}
