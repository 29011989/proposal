/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.management.references.JCloudScaleReference;
import at.ac.tuwien.infosys.jcloudscale.management.references.JCloudScaleReferenceManager;
import at.ac.tuwien.infosys.jcloudscale.server.CustomCLObjectInputStream;

public class SerializationUtil {
	
	public static Object[] getObjectArrayFromBytes(byte[] bytes, ClassLoader classLoader) throws IOException, ClassNotFoundException 
	{
		try(ObjectInputStream objectstream = new CustomCLObjectInputStream(new ByteArrayInputStream(bytes), classLoader))
		{
			return (Object[]) objectstream.readObject();
		}
	}
	
	public static Object getObjectFromBytes(byte[] bytes, ClassLoader classLoader) throws IOException, ClassNotFoundException 
	{
		try(ObjectInputStream objectstream = new CustomCLObjectInputStream(new ByteArrayInputStream(bytes), classLoader))
		{
			return objectstream.readObject();
		}
	}
	
	public static byte[] serializeToByteArray(Object object) throws IOException 
	{
		try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ObjectOutput out = new ObjectOutputStream(bos))
		{
		    out.writeObject(object);
		    return bos.toByteArray();
		}
	}
	
	public static byte[] serializeToByteArray(Object[] objects) throws IOException {
		
		try(ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
		ObjectOutput out = new ObjectOutputStream(bos);)
		{
			out.writeObject(objects);
		    return bos.toByteArray();
		}
	}
	
	public static String base64Encode(byte[] bytes) {
		return DatatypeConverter.printBase64Binary(bytes);
	}
	
	public static byte[] base64Decode(String base64) {
		return DatatypeConverter.parseBase64Binary(base64);
	} 
	
	public static String serializeToString(Object obj) throws IOException
	{
		if(obj == null || !obj.getClass().isAnnotationPresent(XmlRootElement.class))
			return null;
		
		try(StringWriter writer = new StringWriter())
		{
			try
			{
				JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
				ctx.createMarshaller().marshal(obj, writer);
				return writer.toString();
			}
			catch(JAXBException ex)
			{//let's just ignore.
			}
		}
		
		return null;
	}
	
	public static Object deserialize(String data, String className)
	{
		if(className == null || className.length() == 0)
			return null;
		
		if(data != null && data.length() > 0)
		{			
			try(StringReader reader = new StringReader(data))
			{
				try
				{
					JAXBContext ctx = JAXBContext.newInstance(Class.forName(className));
					Unmarshaller um = ctx.createUnmarshaller();
					return um.unmarshal(reader);
				}
				catch(JAXBException | ClassNotFoundException ex)
				{//let's just ignore.
				}
			}
		} 
		else
		{
			try 
			{
				return Class.forName(className).newInstance();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) 
			{
			}
		}
		
		return null;
	}

	public static String paramsToString(Object... params) {

		if (params == null || params.length == 0)
			return "<no args>";

		StringBuilder builder = new StringBuilder();

		builder.append("{").append(paramToString(params[0])).append("}");

		for (int i = 1; i < params.length; ++i) {
			builder.append(", ").append("{").append(paramToString(params[i])).append("}");
		}

		return builder.toString();
	}

	private static String paramToString(Object param) {
		// checking for nulls
		if (param == null)
			return "null";

		Class<?> cls = param.getClass();

		// checking for primitives
		if (cls.isPrimitive() || ReflectionUtil.WRAPPER_TYPES.contains(cls))
			return param.toString();

		// checking for string
		if (cls.equals(String.class))
			return "\"" + param + "\"";

		// checking for arrays
		if (cls.isArray()) {
			return paramArrayToString(param);
		}

		// checking for collections
		if (Iterable.class.isAssignableFrom(cls)) {
			return paramIterableToString((Iterable<?>)param);
		}
		
		// checking for not escaped proxy object
		if(ReflectionUtil.isCGLibEnhancedClass(cls)){
			JCloudScaleReference ref = JCloudScaleReferenceManager.getInstance().getReferenceByProxy(param);
			if(ref == null)
				return String.format("A proxy object of type \"%s\"", cls.getSuperclass());
			else
			{
				return paramReferenceToString(ref);
			}
		}

		// checking for reference object
		if (cls.equals(JCloudScaleReference.class)) {
			return paramReferenceToString((JCloudScaleReference) param);
		}

		// getting object's toString
		String paramString = "";
		try {
			paramString = param.toString();
		} catch (Exception ex) {
			paramString = "\"toString\" call failed: " + ex.getMessage();
		}

		if (paramString.contains(cls.getSimpleName()))
			return paramString;

		return String.format("%s:\"%s\"", cls.getName(), paramString);
	}
	
	private static String paramReferenceToString(JCloudScaleReference reference){
		UUID clientId = JCloudScaleConfiguration.getConfiguration().common().clientID();

		return String.format("ref %s to an instance of \"%s\" located on %s", reference.getReferenceObjectId(),
				reference.getReferenceObjectClassName(), clientId.equals(reference.getReferencingHostId())
						? "client" : "cloud host " + reference.getReferencingHostId());
	}
	
	private static String paramArrayToString(Object param){
		int length = Array.getLength(param);
		if (length == 0)
			return "[]";

		StringBuilder result = new StringBuilder().append("[");
		result.append(paramToString(Array.get(param, 0)));
		for (int i = 1; i < length; ++i) {
			result.append(", ").append(paramToString(Array.get(param, i)));
		}

		return result.append("]").toString();
	}
	
	private static String paramIterableToString(Iterable<?> param){
		StringBuilder result = new StringBuilder().append("[");
		boolean firstElement = true;
		
		for (Object subParam : param) {
			if (firstElement)
				firstElement = false;
			else
				result.append(",");

			result.append(paramToString(subParam));
		}
		return result.append("]").toString();
	}
}
