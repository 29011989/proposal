/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.classLoader;

import java.io.Closeable;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import at.ac.tuwien.infosys.jcloudscale.exception.JCloudScaleException;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractClassLoaderConfiguration implements Serializable, Cloneable
{
    private static final long serialVersionUID = 1L;
    
    /**
     * The instance of custom classloader that should be used on the 
     * client-side to resolve server requests.
     */
    protected transient ClassLoader customClassloader = null;  
    
    
    /**
     * Sets the custom client-side classloader that should be used to 
     * resolve classes and resources requested from the server hosts.
     * By default, classloader that loaded JCloudScale classes is used.
     * @param classLoader
     */
    public void setClientClassLoader(ClassLoader classLoader)
    {
        this.customClassloader = classLoader;
    }
    
    /**
     * Gets the custom client-side classloader that is used by JCloudScale to 
     * resolve classes and resources requested from the server hosts within 
     * the client application. 
     * @return The custom classloader that was specified priorly. Otherwise, <b>null</b>
     */
    public ClassLoader getClientClassLoader()
    {
        return this.customClassloader;
    }

    /**
     * Creates JCloudScale ClassLoader that tries to load classes necessary 
     * for cloud objects from ClassProvider.
     * @return An instance of classloader that can be used on server to load 
     * classes and resources from client.
     */
    public abstract ClassLoader createClassLoader();

    /**
     * Creates an instance of Class Provider that connects to communication channels
     * and serves requests of JCloudScale ClassLoaders from cloud hosts.
     * @return An Closeable instance of Class Provider.
     */
    public abstract Closeable createClassProvider();
    
    
    
    /**
     * Specifies local classloader to use in order to collect/load classes on client-side.
     * By default, classloader that loaded JCloudScale classes is used.
     * @param classLoader Client-side classloader that should be used to locate classes and resources required on server-side.
     * @return The current instance of <b>CachingClassLoaderConfiguration</b> to continue configuration.
     */
    public AbstractClassLoaderConfiguration with(ClassLoader classLoader)
    {
        setClientClassLoader(classLoader);
        return this;
    }
    

    @Override
    public AbstractClassLoaderConfiguration clone()
    {
        try {
            return (AbstractClassLoaderConfiguration) super.clone();
        } catch (CloneNotSupportedException e)
        {
            throw new JCloudScaleException(e, "Failed to clone configuration");
        }
    }
}
