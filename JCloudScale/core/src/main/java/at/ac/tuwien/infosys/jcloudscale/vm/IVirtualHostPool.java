package at.ac.tuwien.infosys.jcloudscale.vm;

import java.io.Closeable;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import at.ac.tuwien.infosys.jcloudscale.migration.IMigrationEnabledHostPool;

public interface IVirtualHostPool extends IMigrationEnabledHostPool, Closeable 
{
	
	public UUID deployCloudObject(IVirtualHost host, ClientCloudObject cloudObject, Object[] args, Class<?>[] paramTypes);
	
	public void destroyCloudObject(UUID cloudObjectId);
	
	public int countCloudObjects();
	
	public Set<UUID> getCloudObjects();
	
	public ReentrantReadWriteLock getCOLock(UUID cloudObjectId);
	
	public IVirtualHost findManagingHost(UUID cloudObjectId);
	
	public Collection<IVirtualHost> getVirtualHosts();
	
	public void shutdownHost(UUID hostId);//TODO: needed only for UIConnector
	
	@Override
	public void close();

}
