package at.ac.tuwien.infosys.jcloudscale.management.references;

import java.lang.reflect.Member;

public interface IClassPassingController {

	/**
	 * Allows controlling if the value should be passed by value or by-reference in this context.
	 * 
	 * @param context
	 *            The current invocation context
	 * @param value
	 *            The current value that needs to be passed
	 * @param paramIndex
	 *            An index of the parameter. If the index is negative, this is the return value.
	 * @param currentDecision
	 *            The current decision with this value.
	 * @return <b>true</b> if the value should be passed by value. Otherwise, -- <b>false</b>.
	 */
	public boolean passByValue(Member context, Object value, int paramIndex, boolean currentDecision);
}
