package at.ac.tuwien.infosys.jcloudscale.messaging;

import java.io.Closeable;
import java.util.UUID;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.naming.NamingException;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.MessageObject;
import at.ac.tuwien.infosys.jcloudscale.utility.CancellationToken;

public interface IMQWrapper extends Closeable
{
	public void disconnect() throws JMSException, NamingException;
	
	@Override
	public void close();
	
	public boolean configurationEquals(MessageQueueConfiguration newConfiguration);
	
	public void createQueueProducer(String destName) throws NamingException, JMSException;
	
	public void createTopicProducer(String destName) throws NamingException, JMSException;
	
	public void createQueueConsumer(String destName) throws NamingException, JMSException;
	
	public void createTopicConsumer(String destName, String messageSelector) throws NamingException, JMSException;
	
	public void createTopicConsumer(String destName) throws NamingException, JMSException;
	
	public void registerListener(MessageListener listener) throws JMSException;
	
	public MessageObject requestResponseToCSHost(MessageObject obj, UUID correlationId, UUID hostId) throws JMSException, TimeoutException;
	
	public MessageObject requestResponseToCSHost(MessageObject obj, UUID correlationId, UUID hostId, CancellationToken cancelToken) throws JMSException, TimeoutException;
	
	public MessageObject requestResponse(MessageObject obj, UUID correlationId) throws JMSException, TimeoutException;
	
	public MessageObject requestResponse(MessageObject obj, UUID correlationId, CancellationToken cancelToken) throws JMSException, TimeoutException;
	
	public void oneway(MessageObject obj) throws JMSException;
	
	public void oneway(MessageObject obj, UUID correlationId) throws JMSException;
	
	public void onewayToCSHost(MessageObject obj, UUID correlationId, UUID hostId) throws JMSException;
	
	public void respond(MessageObject obj, Destination dest, UUID correlationId) throws JMSException;
	
	public void respond(MessageObject obj, Destination dest) throws JMSException;
	
	
}
