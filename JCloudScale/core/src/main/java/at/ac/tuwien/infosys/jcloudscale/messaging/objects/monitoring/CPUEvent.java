/*
 * Copyright 2013 Philipp Leitner Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring;

public class CPUEvent extends StateEvent {
    private static final long serialVersionUID = 87108;

    private String osVersion;
    private int cpuCount;
    private String cpuArchitecture;

    private double systemLoadAverage;

    private double systemCpuLoad;
    private double processCpuLoad;
    private long processCpuTime;

    /**
     * Gets the name and version of the operating system.
     * @return the string representing the version of the operating system
     *         running on the host that produced this event.
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * Sets the operating system version property of the CPUEvent.
     * @param osVersion
     */
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    /**
     * Gets the count of CPUs installed on the host that produced this event.
     * @return The positive value that represents the amount of CPUs (cores) on
     *         the host. If this data is not available, -- negative value.
     */
    public int getCpuCount() {
        return cpuCount;
    }

    /**
     * Sets the cpuCount property of the CPUEvent.
     * @param cpuCount
     */
    public void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }

    /**
     * Gets the CPU architecture (x32,x64...) of the host that produced this
     * event.
     * @return The string that specifies the CPU architecture or empty string if
     *         this data is not available.
     */
    public String getCpuArchitecture() {
        return cpuArchitecture;
    }

    /**
     * Sets the cpuArchitecture property of the CPUEvent.
     * @param cpuArchitecture
     */
    public void setCpuArchitecture(String cpuArchitecture) {
        this.cpuArchitecture = cpuArchitecture;
    }

    /**
     * Gets the System Load Average over the last execution minute.
     * @return The double value between 0 and n (where n is amount of cores on
     *         the system) that represents the recent system load average. NaN
     *         if this value is not available.
     */
    public double getSystemLoadAverage() {
        return systemLoadAverage;
    }

    /**
     * Sets the systemLoadAverage property of the CPUEvent
     * @param systemLoadAverage
     */
    public void setSystemLoadAverage(double systemLoadAverage) {
        this.systemLoadAverage = systemLoadAverage;
    }

    /**
     * Gets the current host CPU load of the host that produced this event.
     * @return The double value between 0 and n (where n is amount of cores on
     *         the system) that represents the overall system load. NaN if this
     *         value is not available.
     */
    public double getSystemCpuLoad() {
        return systemCpuLoad;
    }

    /**
     * Sets the systemCpuLoad property of the CPUEvent.
     * @param systemCpuLoad
     */
    public void setSystemCpuLoad(double systemCpuLoad) {
        this.systemCpuLoad = systemCpuLoad;
    }

    /**
     * Gets the current CPU load that is caused by this java process on the host
     * that produced this event.
     * @return The double value between 0 and n (where n is amount of cores on
     *         the system) that represents the cpu load caused by this
     *         particular java process. NaN if this value is not available.
     */
    public double getProcessCpuLoad() {
        return processCpuLoad;
    }

    /**
     * Sets the ProcessCpuLoad property of the CPUEvent.
     * @param processCpuLoad
     */
    public void setProcessCpuLoad(double processCpuLoad) {
        this.processCpuLoad = processCpuLoad;
    }

    /**
     * Gets the amount of milliseconds for which CPU was used by this process.
     * @return The amount of milliseconds that CPU was used by this process. Negative value if it is not available.
     */
    public long getProcessCpuTime() {
        return processCpuTime;
    }

    /**
     * Sets the processCpuTime property of the CPUEvent.
     * @param processCpuTime
     */
    public void setProcessCpuTime(long processCpuTime) {
        this.processCpuTime = processCpuTime;
    }

    @Override
    public String toString() {
        return String.format("CPUEvent %s:  %s/%s with %d CPUs.  Process: %.2f%%, %dmsec. System: %.2f%%. SLA: %.2f%%",
                super.toString(), osVersion, cpuArchitecture, cpuCount, processCpuLoad * 100, processCpuTime,
                systemCpuLoad * 100, systemLoadAverage);
    }
}
