package at.ac.tuwien.infosys.jcloudscale.vm;

public interface ICloudWrapperProvider {

	public ICloudWrapper getCloudWrapper();
}
