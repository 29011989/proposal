package at.ac.tuwien.infosys.jcloudscale.configuration.distribution;

import java.io.IOException;
import java.util.UUID;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.MessageObject;

class ConfigurationDeliveryObject extends MessageObject
{
	private static final long serialVersionUID = -3761557206063869693L;
	
	String configurationVersion;
	UUID clientId;
	private byte[] serializedConfiguration;
	
	public ConfigurationDeliveryObject(JCloudScaleConfiguration cfg) throws IOException
	{
		this.configurationVersion = cfg.getVersion();
		this.clientId = cfg.common().clientID();
		//we have to clone configuration here to avoid modification to working configuration 
		//(we are modifying configuration during serialization).
		this.serializedConfiguration = cfg.clone().serialize();
	}
	
	public JCloudScaleConfiguration getConfiguration() throws ClassNotFoundException, IOException
	{
		return JCloudScaleConfiguration.deserialize(serializedConfiguration);
	}
}
