package at.ac.tuwien.infosys.jcloudscale.vm;

import java.util.List;

/**
 * Utility interface for CloudWrappers classes.
 * 
 * @author gambi
 *
 */
public interface ICloudWrapper {

	public void startNewHost(String size);

	// Is this needed then or it is just for the sake of Docker ?
	// public void shutdownHost(UUID hostId);

	public void shutdownHost(String ip);

	/**
	 * Returns the list of IP for the VMs in the cloud. TODO We assume that the
	 * VMs are only managed by us ?
	 * 
	 * @return
	 */
	public List<String> listCloudHosts();

}
