package at.ac.tuwien.infosys.jcloudscale.management.references;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.server.AbstractJCloudScaleServerRunner;
import at.ac.tuwien.infosys.jcloudscale.utility.ReflectionUtil;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ClassPassingConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	private Set<String> byValueClasses = new HashSet<>();
	private String classPassingControllerClass = null;

	private transient IClassPassingController classPassingController = null;

	public void addByValueClass(Class<?> cls) {

		if (cls == null)
			return;

		byValueClasses.add(cls.getName());
	}

	public boolean isConfiguredAsByValueClass(String className) {

		if (className == null)
			return false;

		return byValueClasses.contains(className);
	}
	
	public void setClassPassingController(Class<? extends IClassPassingController> classPassingControllerCls){
		if(classPassingControllerCls == null)
			this.classPassingControllerClass = null;
		
		this.classPassingControllerClass = classPassingControllerCls.getName();
		this.classPassingController = null;
	}

	public boolean hasClassPassingController() {

		return classPassingControllerClass != null && classPassingControllerClass.length() > 0;
	}

	public boolean hasByValueClasses() {

		return byValueClasses != null && byValueClasses.size() > 0;
	}

	@SuppressWarnings("unchecked")
	IClassPassingController getClassPassingController() {

		// returning controller if it is created or not defined.
		if (classPassingController != null || !hasClassPassingController())
			return classPassingController;

		// creating class passing controller.
		synchronized (this) {
			try {
				Class<IClassPassingController> cls = null;
				if (JCloudScaleConfiguration.isServerContext()) {
					UUID clientId = JCloudScaleConfiguration.getConfiguration().common().clientID();
					cls = AbstractJCloudScaleServerRunner.getInstance()
							.loadClientClass(clientId, this.classPassingControllerClass);
				} else
					cls = (Class<IClassPassingController>) Class.forName(this.classPassingControllerClass);

				this.classPassingController = ReflectionUtil.newInstance(cls);
			} catch (Exception ex) {
				JCloudScaleConfiguration.getLogger(this).warning(
						"Failed to construct Class Passing Controller " + this.classPassingControllerClass + ": " + ex);
			}

			return classPassingController;
		}
	}
}
