/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.management.references;

import java.io.Closeable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.naming.NamingException;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.IMQWrapper;
import at.ac.tuwien.infosys.jcloudscale.server.AbstractJCloudScaleServerRunner;
import at.ac.tuwien.infosys.jcloudscale.utility.ReferenceHashmap;
import at.ac.tuwien.infosys.jcloudscale.utility.ReflectionUtil;
import at.ac.tuwien.infosys.jcloudscale.utility.SerializationUtil;

public class JCloudScaleReferenceManager implements Closeable {

	private static JCloudScaleReferenceManager instance = null;

	private Hashtable<UUID, Object> references = new Hashtable<>();
	private Map<Object, UUID> referencesReverse = new ReferenceHashmap<>();
	private Map<Object, JCloudScaleReference> objectToReference = new ReferenceHashmap<>();

	private JCloudScaleReferenceCallbackListener callback;
	private IMQWrapper mq;

	private UUID thisHostId;

	private final Logger distributionLogger;

	private JCloudScaleReferenceManager() {

		this.distributionLogger = JCloudScaleConfiguration.getDistributionTraceLogger();

		JCloudScaleConfiguration config = JCloudScaleConfiguration.getConfiguration();

		// if we are on server-side, we send to the fixed client UUID from
		// config
		// on client-side, we need to answer the correct host UUID
		if (JCloudScaleConfiguration.isServerContext()) {
			// XXX AbstractJCloudScaleServerRunner
			// thisHostId = JCloudScaleServerRunner.getInstance().getId();
			thisHostId = AbstractJCloudScaleServerRunner.getInstance().getId();
		} else {
			thisHostId = config.common().clientID();
		}

		try {
			mq = JCloudScaleConfiguration.createMQWrapper(config);
			mq.createTopicProducer(config.server().getCallbackResponseQueueName());

			// if we are on server-side, we register for callbacks to our server
			// ID
			// on clide-side we use the fixed client UUID from config
			mq.createTopicConsumer(config.server().getCallbackRequestQueueName(), "CS_HostId = '" + thisHostId + "'");

			callback = new JCloudScaleReferenceCallbackListener();
			mq.registerListener(callback);
		} catch (NamingException | JMSException e) {
			e.printStackTrace();
		}
	}

	public synchronized static JCloudScaleReferenceManager getInstance() {

		if (instance == null)
			instance = new JCloudScaleReferenceManager();
		return instance;
	}

	public Object[] processArguments(Constructor<?> constructor, Object[] origArguments) {

		boolean[] byRef = ReflectionUtil.findByRefParams(constructor);
		byRef = checkByRefDecisionsWithConfiguration(constructor, origArguments, byRef);
		return replaceRefs(byRef, origArguments);
	}

	public Object[] processArguments(Method method, Object[] origArguments) {

		boolean[] byRef = ReflectionUtil.findByRefParams(method, origArguments);
		byRef = checkByRefDecisionsWithConfiguration(method, origArguments, byRef);
		return replaceRefs(byRef, origArguments);
	}

	public Object processReturn(Method method, Object origReturn) {

		boolean byRef = ReflectionUtil.isByRefReturn(method, origReturn);
		byRef = checkByRefDecisionWithConfiguration(method, origReturn, byRef);
		if (byRef) {
			return createReference(origReturn);
		} else {
			return origReturn;
		}
	}

	public Object processField(Field field, Object fieldValue) {

		boolean byRef = ReflectionUtil.isByRef(field);
		byRef = checkByRefDecisionWithConfiguration(field, fieldValue, byRef);
		if (byRef) {
			return createReference(fieldValue);
		} else {
			return fieldValue;
		}
	}

	public Object getReference(UUID id) {

		if (!references.containsKey(id))
			return null;
		else
			return references.get(id);
	}

	public JCloudScaleReference getReferenceByProxy(Object proxy) {
		if (proxy == null)
			return null;

		return this.objectToReference.get(proxy);
	}

	public void registerReference(JCloudScaleReference reference, Object proxy) {
		this.objectToReference.put(proxy, reference);
	}

	private boolean[] checkByRefDecisionsWithConfiguration(Member member, Object[] args, boolean[] currentDecisions) {

		if (args == null)
			return currentDecisions;

		ClassPassingConfiguration config = JCloudScaleConfiguration.getConfiguration().common().classPassing();

		if (config.hasByValueClasses()) {

			for (int i = 0; i < args.length; ++i) {
				if (args[i] == null)
					continue;

				if (currentDecisions[i] && config.isConfiguredAsByValueClass(args[i].getClass().getName()))
					currentDecisions[i] = false;
			}
		}

		IClassPassingController classPassingController = config.getClassPassingController();

		if (classPassingController != null) {
			for (int i = 0; i < args.length; ++i) {
				if (args[i] == null)
					continue;
				try {
					//
					// we have to not it on entrance and exit as the question
					// here is "is it by reference?
					// but "is it by value?" in method.
					//
					currentDecisions[i] = !classPassingController.passByValue(member, args[i], i, !currentDecisions[i]);
				} catch (Exception ex) {
					JCloudScaleConfiguration.getLogger(this).warning(
							"Exception on checking class passing with user-defined class checker controller: " + ex);
				}
			}
		}

		return currentDecisions;
	}

	private boolean checkByRefDecisionWithConfiguration(Member member, Object arg, boolean currentDecision) {

		if (arg == null)
			return currentDecision;

		ClassPassingConfiguration config = JCloudScaleConfiguration.getConfiguration().common().classPassing();

		if (currentDecision && config.hasByValueClasses())
			if (config.isConfiguredAsByValueClass(arg.getClass().getName()))
				return false;

		IClassPassingController classPassingController = config.getClassPassingController();

		if (classPassingController != null) {
			try {
				//
				// we have to not it on entrance and exit as the question here
				// is "is it by reference?
				// but "is it by value?" in method.
				//
				return !classPassingController.passByValue(member, arg, -1, !currentDecision);
			} catch (Exception ex) {
				JCloudScaleConfiguration.getLogger(this).warning(
						"Exception on checking class passing with user-defined class checker controller: " + ex);
			}
		}

		return currentDecision;
	}

	private Object[] replaceRefs(boolean[] byRef, Object[] origArguments) {

		// now replace all byref params with actual references
		Object[] args = new Object[byRef.length];
		for (int i = 0; i < byRef.length; i++) {
			args[i] = byRef[i] ? createReference(origArguments[i]) : origArguments[i];
		}
		return args;
	}

	private JCloudScaleReference createReference(Object value) {
		if (value == null)
			return null;
		boolean isManaged = isObjectManaged(value);
		UUID refId = isManaged ? getReferenceId(value) : addReference(value);

		if (this.distributionLogger.isLoggable(Level.INFO)) {
			this.distributionLogger.info(String.format("%s reference to %s with id %s",
					isManaged ? "Re-using a" : "Creating a new", SerializationUtil.paramsToString(value), refId));
		}

		JCloudScaleReference reference = new JCloudScaleReference(refId, thisHostId, value);

		objectToReference.put(value, reference);

		return reference;
	}

	private boolean isObjectManaged(Object obj) {

		return referencesReverse.containsKey(obj);
	}

	private UUID addReference(Object obj) {

		UUID id = UUID.randomUUID();
		references.put(id, obj);
		referencesReverse.put(obj, id);
		return id;
	}

	private UUID getReferenceId(Object object) {

		if (!referencesReverse.containsKey(object))
			return null;
		else
			return referencesReverse.get(object);
	}

	@Override
	public void close() {

		references.clear();
		referencesReverse.clear();
		callback.close();
		mq.close();
	}

	public synchronized static void closeInstance() {

		if (instance != null) {
			instance.close();
			instance = null;
		}
	}
}
