package at.ac.tuwien.infosys.jcloudscale.vm;


/**
 * Declares the state of the Cloud Object.
 */
public enum CloudObjectState {
	
	/**
	 * The object is idle and no methods executions are running on it.
	 */
	IDLE, 
	
	/**
	 * The object is active and some methods are executing right now. 
	 */
	EXECUTING,
	
	/**
	 * The object is migrating right now. 
	 */
	MIGRATING,
	
	/**
	 * The object is destroyed and cannot be used any more. 
	 */
	DESTRUCTED
	
}
