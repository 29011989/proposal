/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.logging;

import java.io.PrintStream;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.naming.NamingException;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.IMQWrapper;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.RedirectedOutputObject;

public class SysoutputReceiver {

	private IMQWrapper mq;
	private final Object syncRoot = new Object();
	private static final String messagePattern = "--%s-- ";
	// private boolean err_initialized = false;
	// private boolean out_initialized = false;
	private UUID lastOutputSource = null;
	private boolean isOutputStarted = false;

	private boolean isErrorStarted = false;
	private UUID lastErrorSource = null;

	public SysoutputReceiver() {

		try {
			mq = JCloudScaleConfiguration.createMQWrapper();
			mq.createQueueConsumer(JCloudScaleConfiguration.getConfiguration().server().logging()
					.getOutputRedirectQueue());
			mq.registerListener(new ServerOutputListener());

		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	public void close() {

		try {
			mq.disconnect();
		} catch (JMSException | NamingException e) {
			e.printStackTrace();
		}
	}

	private class ServerOutputListener implements MessageListener {

		@Override
		public void onMessage(Message message) {

			if (message instanceof ObjectMessage) {

				ObjectMessage m = (ObjectMessage) message;

				try {

					if (!(m.getObject() instanceof RedirectedOutputObject))
						return;

					// // throw away old messages
					// if(m.getJMSTimestamp() < System.currentTimeMillis() - OUTPUT_TIMEOUT)
					// return;
					RedirectedOutputObject output = (RedirectedOutputObject) m.getObject();
					synchronized (syncRoot) {

						// selecting output stream.
						PrintStream outputStream = output.isErr() ? System.err : System.out;
						boolean isLineContinues = output.isErr() ? isErrorStarted : isOutputStarted;
						UUID lastSource = output.isErr() ? lastErrorSource : lastOutputSource;

						//
						// we decide if we have to prepend with message source
						//
						if (!isLineContinues || output.getId() != null && !output.getId().equals(lastSource)) {
							String prepending = getPrefix(output.getId(), output.getSource());
							
							if (isLineContinues) {
								// if we had started before, we need to go for the next line first,
								// because we're in the case of new source.
								prepending = System.lineSeparator() + prepending;
							}
							
							outputStream.print(prepending);
							lastSource = output.getId();
						}

						//
						// we format and print the message itself.
						//
						String text = output.getText();
						isLineContinues = !text.endsWith(output.getLineSeparator());

						if (!isLineContinues){
							text = text.substring(0, text.length() - output.getLineSeparator().length());
						}
						
						text = text.replace(output.getLineSeparator(),
								System.lineSeparator() + getPrefix(output.getId(), output.getSource()));

						if (!isLineContinues)
							text += System.lineSeparator();
						
						outputStream.print(text);

						//
						// saving back stream status.
						//
						if (output.isErr()) {
							isErrorStarted = isLineContinues;
							lastErrorSource = lastSource;
						} else {
							isOutputStarted = isLineContinues;
							lastOutputSource = lastSource;
						}
					}
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		}

		private String getPrefix(UUID id, String source) {

			return String.format(messagePattern, source);
		}

		// private String convertLineEndings(String origSeparator, String string) {
		// return string.replaceAll(origSeparator, System.lineSeparator());
		// }
	}
}
