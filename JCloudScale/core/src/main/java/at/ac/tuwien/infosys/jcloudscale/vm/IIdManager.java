package at.ac.tuwien.infosys.jcloudscale.vm;

import java.util.UUID;

public interface IIdManager {
	public UUID getFreeId(boolean staticInstanceOk);
	
	public UUID waitForId();
	
	public void releaseId(UUID id);
	
	 /**
     * Remove this id from the Id manager. This should be called when
     * we requested to destroy a host. Basically a faster way to remove
     * a host before the id times out/
     * @param id
     *            The id to be removed
     */
    public void removeId(UUID id);
	
	/**
     * Checks if this ID belongs to a static or dynamic host
     * @param serverId
     *            The id to check
     * @return True if this ID belongs to a static host, false otherwise.
     */
	public boolean isStaticId(UUID serverId);

	public String getIpToId(UUID id);
}
