/*
 * Copyright 2013 Philipp Leitner Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring;

import java.util.Date;
import java.util.UUID;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.MessageObject;

public abstract class Event extends MessageObject {

    private static final long serialVersionUID = 123456;

    protected long timestamp;
    protected UUID eventId;

    /**
     * Gets the time stamp in milliseconds.
     * @return the difference, measured in milliseconds, between the current
     *         time and midnight, January 1, 1970 UTC.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp property of the event
     * @param timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets the id if the event.
     * @return The UUID that represents the unique id of this event.
     */
    public UUID getEventId() {
        return eventId;
    }

    /**
     * Sets the id property of the event.
     * @param eventId
     */
    public void setEventId(UUID eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return new Date(timestamp) + "(" + eventId + ")";
    }

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
		result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (eventId == null) {
			if (other.eventId != null)
				return false;
		} else
			if (!eventId.equals(other.eventId))
				return false;
		if (timestamp != other.timestamp)
			return false;
		return true;
	}
}
