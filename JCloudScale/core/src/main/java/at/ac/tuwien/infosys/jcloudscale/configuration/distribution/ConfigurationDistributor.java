/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.configuration.distribution;

import java.util.UUID;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;

class ConfigurationDistributor
{
	final static String configurationDeliveryTopic = "CS_ConfigurationDelivery";
	
	//TODO: should we create some helper method to not copy this string everywhere?
	static String createSelector(UUID id) 
	{
		return "JMSCorrelationID = '"+id+"'";
	}
	
	static JCloudScaleConfiguration deserializeConfiguration(ConfigurationDeliveryObject response) throws Exception 
	{
		if(JCloudScaleConfiguration.isVersionCompatible(response.configurationVersion))
		{	//version is compatible, let's try to deserialize this configuration.
			try
			{
				return response.getConfiguration();
			}
			catch(Exception ex)
			{
				throw new Exception("Exception occured trying to deserialize configuration: "+ex, ex);
			}
		}
		else 
			throw new Exception("Configuration version is incompatible. Received \""+response.configurationVersion+"\", Expected \""+JCloudScaleConfiguration.CS_VERSION+"\"");
	}
}
