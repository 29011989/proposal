/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.server.aspects.eventing;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import javax.jms.JMSException;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hyperic.sigar.SigarException;

import at.ac.tuwien.infosys.jcloudscale.configuration.IConfigurationChangedListener;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.CPUEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.NetworkEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.RAMEvent;
import at.ac.tuwien.infosys.jcloudscale.monitoring.MonitoringConfiguration;
import at.ac.tuwien.infosys.jcloudscale.monitoring.providers.IMonitoringProvider;
import at.ac.tuwien.infosys.jcloudscale.monitoring.providers.MonitoringProviderFactory;
import at.ac.tuwien.infosys.jcloudscale.server.AbstractJCloudScaleServerRunner;

/**
 * 
 * This is really just implemented as an aspect to keep consistency with all
 * other event sources - it is not intercepting anything, just creating a
 * separate scheduler which triggers state info periodically
 * 
 * @author philipp
 * 
 */
@Aspect
public class StateEventAspect extends EventAspect implements IConfigurationChangedListener
{
	private Timer scheduler;
	private UUID hostId;
	private Object lock = new Object();
	private IMonitoringProvider monitoringProvider;

	// XXX AbstractJCloudScaleServerRunner
	@After("this(server) && execution(protected void at.ac.tuwien.infosys.jcloudscale.server.AbstractJCloudScaleServerRunner.run())")
	public void startStateEventScheduler(AbstractJCloudScaleServerRunner server) {

		log.fine("Initializing state events");

		this.hostId = server.getId();

		if (JCloudScaleConfiguration.getConfiguration().common().monitoring().isEnabled()) {
			initializeScheduler(JCloudScaleConfiguration.getConfiguration());
		}

		server.registerConfigurationChangeListner(this);
	}

	private void initializeScheduler(JCloudScaleConfiguration cfg)
	{
		synchronized (lock)
		{
			if (scheduler != null)
				scheduler.cancel();

			monitoringProvider = MonitoringProviderFactory.selectMonitoringProvider(hostId);
			if(monitoringProvider == null)
			{
			    log.severe("Failed to use any Monitoring Provider. Monitoring information will not be provided.");
			    return;
			}
			else
			    log.fine("Monitoring Provider "+monitoringProvider.getClass().getName()+" will be used.");
			
			scheduler = new Timer();
			long schedulingInterval = cfg.common().monitoring().getSystemEventsInterval();
			scheduler.scheduleAtFixedRate(new StateEventTask(), 0, schedulingInterval);

			log.info(String.format("Starting to send state events from %s with interval %sms.",
			        monitoringProvider.getClass().getSimpleName(), schedulingInterval));
		}
	}

	// XXX AbstractJCloudScaleServerRunner
	@Before("execution(public void at.ac.tuwien.infosys.jcloudscale.server.AbstractJCloudScaleServerRunner.shutdown())")
	public void stopStateEventScheduler()
	{
		synchronized (lock)
		{
			if (scheduler != null)
			{
				scheduler.cancel();

				if(monitoringProvider != null)
				{
				    monitoringProvider.close();
				    monitoringProvider = null;
				}
				log.info("Stopped sending state events");
			}
		}
	}

	@Override
	public void onConfigurationChange(JCloudScaleConfiguration newConfiguration)
	{
		MonitoringConfiguration oldMonitoring = JCloudScaleConfiguration.getConfiguration().common()
				.monitoring();
		MonitoringConfiguration newMonitoring = newConfiguration.common().monitoring();

		if (oldMonitoring.isEnabled() != newMonitoring.isEnabled() ||
				oldMonitoring.getSystemEventsInterval() != newMonitoring.getSystemEventsInterval())
			initializeScheduler(newConfiguration);
	}

	private class StateEventTask extends TimerTask {

		@Override
		public void run()
		{

			log.finest("Running StateEventTask");

			synchronized (lock)
			{
				MonitoringConfiguration cfg = JCloudScaleConfiguration.getConfiguration().common()
						.monitoring();
				if (cfg.triggerCpuEvents())
				{
					try {
						sendCPUEvent();
					} catch (Exception e) {
						e.printStackTrace();
						log.severe("Error while triggering CPUEvent: " + e.getMessage());
					}
				}

				if (cfg.triggerRamEvents())
				{
					try {
						sendRAMEvent();
					} catch (Exception e) {
						e.printStackTrace();
						log.severe("Error while triggering RAMEvent: " + e.getMessage());
					}
				}

				if (cfg.triggerNetworkEvents())
				{
					try {
						sendNetworkEvent();
					} catch (Exception e) {
						e.printStackTrace();
						log.severe("Error while triggering NetworkEvent: " + e.getMessage());
					}
				}
			}
		}

		private void sendCPUEvent() throws JMSException, SigarException {

			log.finer("Sending CPU event");
			
			CPUEvent event = monitoringProvider.measureCpuEvent();
			
			getMqHelper().sendEvent(event);
			
			log.finer("Successfully sent CPU event");
		}

		private void sendRAMEvent() throws JMSException, SigarException {

			log.finer("Sending RAM event");

			RAMEvent event = monitoringProvider.measureRamEvent();

			getMqHelper().sendEvent(event);
			
			log.finer("Successfully sent RAM event");

		}

		private void sendNetworkEvent() throws JMSException, SigarException {

			log.finer("Sending Network event");

			NetworkEvent event = monitoringProvider.measureNetworkEvent();

			getMqHelper().sendEvent(event);
			
			log.finer("Successfully sent Network event");
		}
	}
}
