/*
 * Copyright 2013 Philipp Leitner Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring;

public class RAMEvent extends StateEvent {
    private static final long serialVersionUID = 87112;

    private long processUsed;
    private long processFree;
    private long processMaxAllowed;

    private long systemUsed;
    private long systemFree;
    private long systemRam;
    private long systemCommitted;

    private long systemUsedSwap;
    private long systemFreeSwap;
    private long systemMaxSwap;

    /**
     * Gets the amount of memory actually used by the current java process on host that produced this event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getProcessUsed() {
        return processUsed;
    }

    /**
     * Sets the processUsed property of the RAMEvent
     * @param processUsed
     */
    public void setProcessUsed(long processUsed) {
        this.processUsed = processUsed;
    }

    /**
     * Gets the amount of memory acquired by the process, but currently free. 
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getProcessFree() {
        return processFree;
    }

    /**
     * Gets the total amount of currently allocated memory for the process on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getProcessAllocated() {
        if(this.processFree < 0 || this.processUsed < 0)
            return Math.min(this.processFree, this.processUsed);
        
        return this.processFree + this.processUsed;
    }

    /**
     * Sets the processFree property of the RAMEvent
     * @param processFree
     */
    public void setProcessFree(long processFree) {
        this.processFree = processFree;
    }

    /**
     * Gets the maximally allowed amount of memory to allocate for the java process on the host that produced event. 
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getProcessMaxAllowed() {
        return processMaxAllowed;
    }

    /**
     * Sets the processMaxAllowed property of the RAMEvent
     * @param processMaxAllowed
     */
    public void setProcessMaxAllowed(long processMaxAllowed) {
        this.processMaxAllowed = processMaxAllowed;
    }

    /**
     * Gets the amount of memory that is currently used on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getSystemUsed() {
        return systemUsed;
    }

    /**
     * Sets the systemUsed property of the RAMEvent
     * @param systemUsed
     */
    public void setSystemUsed(long systemUsed) {
        this.systemUsed = systemUsed;
    }

    /**
     * Gets the total amount of installed RAM on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getSystemTotalRam() {
        return this.systemRam;
    }
    
    /**
     * Sets the systemRAM property of the RAMEvent
     * @param systemRam
     */
    public void setSystemTotalRam(long systemRam) {
        this.systemRam = systemRam;
    }


    /**
     * Gets the amount of free memory on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getSystemFree() {
        return systemFree;
    }

    /**
     * Sets the systemFree property of the RAMEvent
     * @param systemFree
     */
    public void setSystemFree(long systemFree) {
        this.systemFree = systemFree;
    }

    /**
     * Gets the amount of memory currently committed (available for usage) on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable. 
     */
    public long getSystemCommitted() {
        return systemCommitted;
    }

    /**
     * Sets the systemComitted property of the RAMEvent
     * @param systemCommitted
     */
    public void setSystemCommitted(long systemCommitted) {
        this.systemCommitted = systemCommitted;
    }

    /**
     * Gets the amount of swap memory that is used on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getSystemUsedSwap() {
        return systemUsedSwap;
    }

    /**
     * Sets the systemUsedSwap property of the RAMEvent
     * @param systemUsedSwap
     */
    public void setSystemUsedSwap(long systemUsedSwap) {
        this.systemUsedSwap = systemUsedSwap;
    }

    /**
     * Gets the amount of swap memory that is allocated, but currently free on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getSystemFreeSwap() {
        return systemFreeSwap;
    }

    /**
     * Sets the systemFreeSwap property of the RAMEvent
     * @param systemFreeSwap
     */
    public void setSystemFreeSwap(long systemFreeSwap) {
        this.systemFreeSwap = systemFreeSwap;
    }

    /**
     * Gets the maximal amount of swap memory that can be allocated on the host that produced event.
     * @return The long value that represents the amount of memory in bytes or negative value if the actual value is unavailable.
     */
    public long getSystemMaxSwap() {
        return systemMaxSwap;
    }

    /**
     * Sets the systemMaxSwap property of the RAMEvent
     * @param systemMaxSwap
     */
    public void setSystemMaxSwap(long systemMaxSwap) {
        this.systemMaxSwap = systemMaxSwap;
    }

    @Override
    public String toString() {
        return String.format("RAMEvent %s: PROC: Free:%d, Used:%d, Max:%d; SYS: Free:%d, Used:%d Commit:%d,RAM:%d; SWAP: Free:%d, Used:%d, Max:%d.",super.toString(),
                processFree, processUsed, processMaxAllowed,
                systemFree, systemUsed, systemCommitted, systemRam,
                systemFreeSwap, systemUsedSwap, systemMaxSwap);
    }
}
