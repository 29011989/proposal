package at.ac.tuwien.infosys.jcloudscale.configuration.distribution;

import java.io.Closeable;
import java.io.IOException;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.naming.NamingException;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.IMQWrapper;
import at.ac.tuwien.infosys.jcloudscale.server.AbstractJCloudScaleServerRunner;

public class ServerConfigurationListener implements Closeable, MessageListener 
{
	private IMQWrapper mq;
	public ServerConfigurationListener(UUID id) throws NamingException, JMSException
	{
		mq = JCloudScaleConfiguration.createMQWrapper();
		mq.createTopicConsumer(ConfigurationDistributor.configurationDeliveryTopic, ConfigurationDistributor.createSelector(id));
		mq.createTopicProducer(ConfigurationDistributor.configurationDeliveryTopic);
		mq.registerListener(this);
	}
	
	@Override
	public void onMessage(Message msg) 
	{
		if(msg == null || !(msg instanceof ObjectMessage))
			return;
		
		try 
		{
			ConfigurationDeliveryObject configuationDelvery = (ConfigurationDeliveryObject)((ObjectMessage)msg).getObject();
			
			JCloudScaleConfiguration cfg;
			try 
			{
				cfg = ConfigurationDistributor.deserializeConfiguration(configuationDelvery);
				
				//all working services that are interested in configuration replacement will react on that.
				// XXX AbstractJCloudScaleServerRunner
				// JCloudScaleServerRunner.getInstance().setConfiguration(cfg);
				if(AbstractJCloudScaleServerRunner.getInstance() != null)
					AbstractJCloudScaleServerRunner.getInstance().setConfiguration(cfg);
				
				mq.oneway(new ConfigurationDeliveryResponseObject(true, ""), configuationDelvery.clientId);
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				
				mq.oneway(new ConfigurationDeliveryResponseObject(false, e.getMessage()), configuationDelvery.clientId);
			}
		} 
		catch (JMSException e) 
		{
			e.printStackTrace();//hardly can do anything else here... send message to client if we have configuration already?
		}
	}

	@Override
	public void close() throws IOException 
	{
		if(mq != null)
		{
			mq.close();
			mq = null;
		}
	}
}

