package at.ac.tuwien.infosys.jcloudscale.configuration.distribution;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.MessageObject;

class ConfigurationDeliveryResponseObject extends MessageObject
{
	private static final long serialVersionUID = -1470029851256020709L;
	boolean isConfigurationAccepted;
	String errorMessage;
	
	public ConfigurationDeliveryResponseObject(boolean isConfigurationAccepted, String errorMessage)
	{
		this.isConfigurationAccepted = isConfigurationAccepted;
		this.errorMessage = errorMessage;
	}
}
