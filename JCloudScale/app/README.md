Eclipse complains that com.sun.management.OperatingSystemMXBean is not a valid public API on Mac.

According to: http://stackoverflow.com/questions/25222811/access-restriction-the-type-application-is-not-api-restriction-on-required-l

You can configure your eclipse project to avoid that error:

*Background: Eclipse access restrictions

Eclipse has a mechanism called access restrictions to prevent you from accidentally using classes which Eclipse thinks are not part of the public API. Usually, Eclipse is right about that, in both senses: We usually do not want to use something which is not part of the public API. And Eclipse is usually right about what is and what isn't part of the public API.

*Problem

Now, there can be situations, where you want to use non-public API. And there can be situations, where Eclipse is not really right (that's what happened to me, I just wanted to use javax.smartcardio). In that case, we get this error in Eclipse.

*Solution

The solution is to change the access restrictions. Go to the properties of your Java project, i.e. by selecting "Properties" from the context menu of the project in the "Package Explorer". There, go to "Java Build Path", tab "Libraries". There, expand the library entry, select "Access rules", "Edit..." and "Add..." a "Resolution: Accessible" with a corresponding rule pattern.

Use com/sun/management/**
