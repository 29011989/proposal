<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<artifactId>jcloudscale.app</artifactId>
	<name>JCloudScale for Application</name>
	<description>JCloudScale Client Application Module</description>

	<parent>
		<groupId>jcloudscale</groupId>
		<artifactId>jcloudscale</artifactId>
		<version>0.5.0-SNAPSHOT</version>
	</parent>

	<properties>
		<testsJVMArgumensLine>-Xmx2048m</testsJVMArgumensLine>
		<libraryDirectory>lib</libraryDirectory>
		<classLoaderCacheDir>classLoaderCache</classLoaderCacheDir>
		<libraryDirectoryPath>${project.build.directory}/${libraryDirectory}</libraryDirectoryPath>
		<serverMainClass>at.ac.tuwien.infosys.jcloudscale.server.JCloudScaleServerRunner</serverMainClass>
		<resources.jcloudscale>src/main/resources/META-INF</resources.jcloudscale>
		<resources.tests>src/test/resources/META-INF</resources.tests>
		<testsTempFolder>tests-temp</testsTempFolder>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>

		<!-- Core & Server component -->
		<dependency>
			<groupId>jcloudscale</groupId>
			<artifactId>jcloudscale.core</artifactId>
			<version>0.5.0-SNAPSHOT</version>
		</dependency>

		<!-- TESTS DEPENDENCIES -->

		<!-- testing -->
		<dependency>
			<scope>test</scope>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
		</dependency>

		<dependency>
			<scope>test</scope>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-io</artifactId>
			<version>1.3.2</version>
		</dependency>

		<dependency>
			<scope>test</scope>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>4.0.1.RELEASE</version>
		</dependency>

	</dependencies>

	<!-- Configuration -->
	<build>
		<plugins>
			<!-- specify the java version for code compiler plugin -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.1</version>
				<configuration>
					<source>${javaVersion}</source>
					<target>${javaVersion}</target>
				</configuration>
			</plugin>

			<!-- Findbugs Configuration -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>2.5.3</version>
				<configuration>
					<findbugsXmlOutput>true</findbugsXmlOutput>
					<findbugsXmlWithMessages>true</findbugsXmlWithMessages>
					<xmlOutput>true</xmlOutput>
					<excludeFilterFile>findbugsIgnore.xml</excludeFilterFile>
					<effort>Max</effort>
					<threshold>Low</threshold>
				</configuration>
			</plugin>

			<!-- Specifying that aspectJ should weave aspects in compile-time -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>aspectj-maven-plugin</artifactId>
				<version>1.6</version>
				<configuration>
					<source>${javaVersion}</source>
					<target>${javaVersion}</target>
					<complianceLevel>${javaVersion}</complianceLevel>
					<verbose>true</verbose>
					<weaveWithAspectsInMainSourceFolder>true</weaveWithAspectsInMainSourceFolder>
					<!-- <excludes> <exclude>**/StateEventAspect.java</exclude> </excludes> -->
					<aspectLibraries>
						<aspectLibrary>
							<groupId>jcloudscale</groupId>
							<artifactId>jcloudscale.core</artifactId>
						</aspectLibrary>
					</aspectLibraries>
				</configuration>
				<executions>
					<execution>
						<configuration>
							<XnoInline>true</XnoInline>
						</configuration>
						<goals>
							<goal>compile</goal>
							<goal>test-compile</goal>
						</goals>
					</execution>
				</executions>
				<dependencies>
					<dependency>
						<groupId>org.aspectj</groupId>
						<artifactId>aspectjrt</artifactId>
						<version>${aspectj.version}</version>
					</dependency>
					<dependency>
						<groupId>org.aspectj</groupId>
						<artifactId>aspectjtools</artifactId>
						<version>${aspectj.version}</version>
					</dependency>
				</dependencies>
			</plugin>

			<!-- skip unit test run, tests has to be executed during integration-test -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.10</version>
				<configuration>
					<skip>true</skip>
				</configuration>
				<executions>
					<execution>
						<id>surefire-it</id>
						<phase>integration-test</phase>
						<goals>
							<goal>test</goal>
						</goals>
						<configuration>
							<argLine>${testsJVMArgumensLine}</argLine>
							<skip>false</skip>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Copy all dependencies to lib folder in target folder. -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.5</version>
				<executions>
					<execution>
						<id>copy-dependencies</id>
						<phase>package</phase>
						<goals>
							<goal>copy-dependencies</goal>
						</goals>
						<configuration>
							<outputDirectory>${libraryDirectoryPath}</outputDirectory>
							<overWriteReleases>false</overWriteReleases>
							<overWriteSnapshots>false</overWriteSnapshots>
							<overWriteIfNewer>true</overWriteIfNewer>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Building runnable jars for project and tests. -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<archive>
						<index>true</index>
						<manifest>
							<addClasspath>true</addClasspath>
							<!-- <classpathPrefix>${libraryDirectory}</classpathPrefix> -->
							<mainClass>${serverMainClass}</mainClass>
						</manifest>
					</archive>
					<goal>
					</goal>
				</configuration>

				<!-- Build and include test jar into artifacts. -->
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>

				<!-- <executions> Build tests also to lib dir -->
				<!-- <execution> -->
				<!-- <id>test-jar</id> -->
				<!-- <phase>package</phase> -->
				<!-- <goals> -->
				<!-- <goal>test-jar</goal> -->
				<!-- </goals> -->
				<!-- <configuration> -->
				<!-- <outputDirectory>${libraryDirectoryPath}</outputDirectory> -->
				<!-- </configuration> -->
				<!-- </execution> -->
				<!-- </executions> -->
			</plugin>

			<!-- Generates jar file with project source. -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.2.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- Generates jar file with project javadocs. -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.9.1</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- <plugin> <artifactId>maven-assembly-plugin</artifactId> <configuration> 
				<archive> <manifest> <mainClass>${serverMainClass}</mainClass> </manifest> 
				</archive> <descriptorRefs> <descriptorRef>jar-with-dependencies</descriptorRef> 
				</descriptorRefs> </configuration> <executions> <execution> <id>jcloudscale-with-dependencies</id> 
				<phase>package</phase> <goals> <goal>single</goal> </goals> </execution> 
				</executions> </plugin> -->

		</plugins>
	</build>
</project>
