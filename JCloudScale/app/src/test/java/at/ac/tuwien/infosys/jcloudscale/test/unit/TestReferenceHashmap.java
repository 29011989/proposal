package at.ac.tuwien.infosys.jcloudscale.test.unit;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import at.ac.tuwien.infosys.jcloudscale.utility.ReferenceHashmap;

public class TestReferenceHashmap {
	
	private ReferenceHashmap<BrokenKey, BrokenValue> map;
	
	@Before
	public void setup() {
		map = new ReferenceHashmap<>();
	}
	
	@Test
	public void testSize() {
		
		BrokenKey key = new BrokenKey(); 
		
		assertEquals(0, map.size());
		map.put(key, new BrokenValue());
		assertEquals(1, map.size());
		map.put(new BrokenKey(), new BrokenValue());
		assertEquals(2, map.size());
		map.remove(key);
		assertEquals(1, map.size());
		map.clear();
		assertEquals(0, map.size());
		
		
	}
	
	@Test
	public void testIsEmpty() {
		
		assertTrue(map.isEmpty());
		map.put(new BrokenKey(), new BrokenValue());
		assertFalse(map.isEmpty());
		map.clear();
		assertTrue(map.isEmpty());
		
	}
	
	@Test
	public void testContainsKey() {
		
		BrokenKey key = new BrokenKey(); 
		
		assertEquals(0, map.size());
		map.put(key, new BrokenValue());
		assertEquals(1, map.size());
		map.put(new BrokenKey(), new BrokenValue());
		assertEquals(2, map.size());
		assertTrue(map.containsKey(key));
		
	}
	
	@Test
	public void testContainsValue() {
		
		BrokenKey key = new BrokenKey();
		BrokenValue value = new BrokenValue();
		
		assertEquals(0, map.size());
		map.put(key, value);
		assertEquals(1, map.size());
		map.put(new BrokenKey(), new BrokenValue());
		assertEquals(2, map.size());
		assertTrue(map.containsValue(value));
		
	}
	
	@Test
	public void testGetAndPut() {
		
		BrokenKey key = new BrokenKey();
		BrokenValue value = new BrokenValue();
		
		assertEquals(0, map.size());
		map.put(key, value);
		assertEquals(1, map.size());
		map.put(new BrokenKey(), new BrokenValue());
		assertEquals(2, map.size());
		assertTrue(map.get(key) == value);
		
	}
	
	@Test(expected = RuntimeException.class)
	public void testFailing() {
		
		BrokenKey key = new BrokenKey();
		BrokenValue value = new BrokenValue();
		
		HashMap<BrokenKey, BrokenValue> oldMap =
				new HashMap<>();
				
		oldMap.put(key , value);
		
	}
	
	
	private static class BrokenKey {
		
		@Override
		public int hashCode() {
			throw new RuntimeException("hashCode called");
		}
		
		@Override
		public boolean equals(Object other) {
			throw new RuntimeException("equals called");
		}
		
	}
	
	private static class BrokenValue {
		
		@Override
		public int hashCode() {
			throw new RuntimeException("hashCode called");
		}
		
		@Override
		public boolean equals(Object other) {
			throw new RuntimeException("equals called");
		}
		
	}
	
}

