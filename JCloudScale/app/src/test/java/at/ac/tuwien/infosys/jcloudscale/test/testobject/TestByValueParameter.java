package at.ac.tuwien.infosys.jcloudscale.test.testobject;

import java.io.Serializable;

import at.ac.tuwien.infosys.jcloudscale.annotations.ByValueParameter;

@ByValueParameter
public class TestByValueParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name = "";
	private int id = 0;

	public TestByValueParameter(String name, int id) {

		this.name = name;
		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public int getId() {

		return id;
	}
}
