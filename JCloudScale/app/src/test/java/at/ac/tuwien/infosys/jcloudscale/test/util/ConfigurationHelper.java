/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package at.ac.tuwien.infosys.jcloudscale.test.util;

import java.io.File;
import java.util.logging.Level;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfigurationBuilder;
import at.ac.tuwien.infosys.jcloudscale.policy.sample.HostPerObjectScalingPolicy;
import at.ac.tuwien.infosys.jcloudscale.test.testevents.EventDrivenPolicyTestEvent;
import at.ac.tuwien.infosys.jcloudscale.test.testobject.MyCustomEvent;
import at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalCloudPlatformConfiguration;

public class ConfigurationHelper
{
    protected static JCloudScaleConfigurationBuilder applyCommonConfiguration(JCloudScaleConfigurationBuilder builder)
    {
        builder .with(new HostPerObjectScalingPolicy())
                .withLogging(Level.INFO)
                .withDistributionTraceLogger(true)
                .withMonitoring(true)
                .withInvocationTimeout(2*60*1000)//2 minutes
                .withMonitoringEvents(MyCustomEvent.class, EventDrivenPolicyTestEvent.class);
        
        builder.build().common().setKeepAliveIntervalInSec(15);
        
        return builder;
    }

    public static JCloudScaleConfigurationBuilder createDefaultTestConfiguration()
    {
    	return applyCommonConfiguration(new JCloudScaleConfigurationBuilder(
    			new LocalCloudPlatformConfiguration()
    				.withStartupDirectory("target/")
    				.withClasspath(String.format("lib/*%1$slib/", File.pathSeparatorChar))
    				.withCustomJVMArgs("-Djava.library.path=test-classes")));
    }
}
