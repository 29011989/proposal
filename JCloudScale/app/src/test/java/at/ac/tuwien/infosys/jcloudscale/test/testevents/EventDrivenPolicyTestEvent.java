package at.ac.tuwien.infosys.jcloudscale.test.testevents;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.Event;

public class EventDrivenPolicyTestEvent extends Event {
	
	private static final long serialVersionUID = 1L;
	
	private int value;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
}
