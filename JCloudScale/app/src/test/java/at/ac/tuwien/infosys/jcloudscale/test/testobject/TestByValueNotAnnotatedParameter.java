package at.ac.tuwien.infosys.jcloudscale.test.testobject;

import java.io.Serializable;
import java.lang.reflect.Member;

import at.ac.tuwien.infosys.jcloudscale.management.references.IClassPassingController;

public class TestByValueNotAnnotatedParameter implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	public TestByValueNotAnnotatedParameter(String name){
		this.name = name;
	}

	public String getName() {
	
		return name;
	}

	public void setName(String name) {
	
		this.name = name;
	}
	
	public static class ClassPassingController implements IClassPassingController{
		@Override
		public boolean passByValue(Member context, Object value, int paramIndex, boolean currentDecision) {
			if(value == null || !value.getClass().equals(TestByValueNotAnnotatedParameter.class))
				return currentDecision;
			
			// TestByValueNotAnnotatedParameter has to go always by-Value!
			return true;
		}
		
	}
}
