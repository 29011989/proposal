package at.ac.tuwien.infosys.jcloudscale.test.testobject;

import at.ac.tuwien.infosys.jcloudscale.annotations.CloudObject;
import at.ac.tuwien.infosys.jcloudscale.annotations.DestructCloudObject;

@CloudObject
public class ComplexObjectParameterPassing {

	public void passDoubleHere(Object doubleValue)
	{
		((Double)doubleValue).doubleValue();
	}
	
	public Object getDoubleFromHere()
	{
		return Math.PI;
	}
	
	public Object getAndPassDoubleHere(Object doubleValue)
	{
		return ((Double)doubleValue)*2;
	}
	
	public Object getAndPassByValueParameterHere(Object param)
	{
		TestByValueParameter byValParam = (TestByValueParameter)param;
		byValParam.setName(byValParam.getName()+"_modified");
		return byValParam;
	}
	
	@DestructCloudObject
	public void cleanup(){}
}
