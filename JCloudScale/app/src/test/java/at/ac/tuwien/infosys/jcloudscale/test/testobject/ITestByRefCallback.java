package at.ac.tuwien.infosys.jcloudscale.test.testobject;

public interface ITestByRefCallback {

	public int getInvocationCount();
	
	public void invoke();
	
	public int invokeWithReturn();
	
	public int invokeWithCount(int count);
	
	public Object invokeWithCountBoxed(Object count);
}
