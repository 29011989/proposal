/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.test.testobject;

import at.ac.tuwien.infosys.jcloudscale.annotations.ByValueParameter;
import at.ac.tuwien.infosys.jcloudscale.annotations.CloudObject;


/**
 * Check if by placing the annotation on the constructor this tests work
 * @author gambi
 *
 */
@CloudObject
public class ByRefCloudObjectWithAnnotation extends ByRefCloudObject {

	public ByRefCloudObjectWithAnnotation() {
		super();
	}

	public ByRefCloudObjectWithAnnotation(String name) {
		super(name);
	}

	public ByRefCloudObjectWithAnnotation(@ByValueParameter TestByValueNotAnnotatedParameter param) {
		super(param);
	}

	public ByRefCloudObjectWithAnnotation(String name, TestByValueParameter param) {
		super(name, param);
	}

	public ByRefCloudObjectWithAnnotation(TestByRefParameter test) {
		super(test);
	}

}
