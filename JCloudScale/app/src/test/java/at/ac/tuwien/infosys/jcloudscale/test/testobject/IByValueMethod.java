package at.ac.tuwien.infosys.jcloudscale.test.testobject;

import at.ac.tuwien.infosys.jcloudscale.annotations.ByValueParameter;

public interface IByValueMethod {
	public @ByValueParameter TestByValueParameter process(@ByValueParameter TestByValueParameter param);
}
