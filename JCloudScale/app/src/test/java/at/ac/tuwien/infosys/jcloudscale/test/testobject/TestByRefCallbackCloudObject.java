package at.ac.tuwien.infosys.jcloudscale.test.testobject;

import at.ac.tuwien.infosys.jcloudscale.annotations.CloudObject;

@CloudObject
public class TestByRefCallbackCloudObject {

	private TestByRefCallback callback;
	
	private ITestByRefCallback interfaceCallback; 
	
	public TestByRefCallbackCloudObject(TestByRefCallback callback)
	{
		this.callback = callback;
	}
	
	public void setCallback(ITestByRefCallback callback)
	{
		this.interfaceCallback = callback;
	}
	
	
	public void performInvocations()
	{
		this.callback.invoke();
		
		int result = this.callback.invokeWithReturn();
		
		if(this.callback.invokeWithReturn() != result + 1)
			throw new RuntimeException("Failed to increase the counter!");
		
		if(this.callback.invokeWithCount(2) != result + 3)
			throw new RuntimeException("Failed to increase the counter again!");
		
		if(((Integer)this.callback.invokeWithCountBoxed(2)).intValue() != result + 5)
			throw new RuntimeException("Failed to increase the counter on the last attempt!");
	}
	
	public void performInvocationsOnInterface()
	{
		this.interfaceCallback.invoke();
		
		int result = this.interfaceCallback.invokeWithReturn();
		
		if(this.interfaceCallback.invokeWithReturn() != result + 1)
			throw new RuntimeException("Failed to increase the counter!");
		
		if(this.interfaceCallback.invokeWithCount(2) != result + 3)
			throw new RuntimeException("Failed to increase the counter again!");
		
		if(((Integer)this.interfaceCallback.invokeWithCountBoxed(2)).intValue() != result + 5)
			throw new RuntimeException("Failed to increase the counter on the last attempt!");
	}
	
	public ITestByRefCallback performInvocationsOnPassedInterface(ITestByRefCallback callback)
	{
		callback.invoke();
		
		int result = callback.invokeWithReturn();
		
		if(callback.invokeWithReturn() != result + 1)
			throw new RuntimeException("Failed to increase the counter!");
		
		if(callback.invokeWithCount(2) != result + 3)
			throw new RuntimeException("Failed to increase the counter again!");
		
		if(((Integer)callback.invokeWithCountBoxed(2)).intValue() != result + 5)
			throw new RuntimeException("Failed to increase the counter on the last attempt!");
		
		return callback;
	}
	
	public Class<?> moveClass(Class<?> param){
		return param;
	}
}
