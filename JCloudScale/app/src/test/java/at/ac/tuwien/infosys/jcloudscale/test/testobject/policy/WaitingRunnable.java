package at.ac.tuwien.infosys.jcloudscale.test.testobject.policy;

import at.ac.tuwien.infosys.jcloudscale.annotations.CloudObject;
import at.ac.tuwien.infosys.jcloudscale.annotations.DestructCloudObject;

import java.io.Closeable;
import java.io.IOException;

/**
 * @author Gregor Schauer
 */
@CloudObject
public class WaitingRunnable implements Runnable, Closeable {
	boolean closed;

	public void start() {
		new Thread() {
			@Override
			public void run() {
				WaitingRunnable.this.run();
			}
		}.start();
	}
	
	@Override
	public void run() {
		try {
			while (!closed) {
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			// Who cares?
		}
	}

	@DestructCloudObject
	@Override
	public void close() throws IOException {
		closed = true;
		Thread.currentThread().interrupt();
	}
}
