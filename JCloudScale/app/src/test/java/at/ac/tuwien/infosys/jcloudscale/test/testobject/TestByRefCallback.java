package at.ac.tuwien.infosys.jcloudscale.test.testobject;

public class TestByRefCallback implements ITestByRefCallback {

	private int invocationCount = 0;
	
	public TestByRefCallback(){}
	
	public TestByRefCallback(int initialCount)
	{
		this.invocationCount = initialCount;
	}

	@Override
	public int getInvocationCount() {
	
		return invocationCount;
	}
	
	public void setInvocationCount(int invocationCount) {
	
		this.invocationCount = invocationCount;
	}
	
	@Override
	public void invoke()
	{
		this.invocationCount++;
	}
	
	@Override
	public int invokeWithReturn()
	{
		return ++this.invocationCount;
	}
	
	@Override
	public int invokeWithCount(int count)
	{
		this.invocationCount+=count;
		return this.invocationCount;
	}
	
	@Override
	public Object invokeWithCountBoxed(Object count)
	{
		return invokeWithCount((Integer)count);
	}
	
}
