package at.ac.tuwien.infosys.jcloudscale.test.testobject.policy;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Gregor Schauer
 */
public class CalculatingRunnable implements Runnable, Closeable {
	Map<Long, Double> results = new TreeMap<>();
	int counter;
	
	@Override
	public void run() {
		for (counter = 1; counter > 0; counter++) {
			Long now = System.currentTimeMillis();
			results.put(now, Math.pow(counter, counter));
		}
	}

	@Override
	public void close() throws IOException {
		counter = Integer.MIN_VALUE;
	}
}
