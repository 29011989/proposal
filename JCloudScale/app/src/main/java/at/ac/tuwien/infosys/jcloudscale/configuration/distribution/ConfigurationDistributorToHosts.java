/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.configuration.distribution;

import java.io.IOException;
import java.util.UUID;

import javax.jms.JMSException;
import javax.naming.NamingException;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.exception.JCloudScaleException;
import at.ac.tuwien.infosys.jcloudscale.messaging.IMQWrapper;
import at.ac.tuwien.infosys.jcloudscale.messaging.TimeoutException;
import at.ac.tuwien.infosys.jcloudscale.server.PlatformSpecificUtil;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.localVm.LocalCloudPlatformConfiguration;

public class ConfigurationDistributorToHosts
{
	public static synchronized void sendConfigurationToHost(JCloudScaleConfiguration cfg, UUID serverId) throws NamingException, JMSException, TimeoutException, IOException 
	{
		try(IMQWrapper mq = JCloudScaleConfiguration.createMQWrapper())
		{
			mq.createTopicConsumer(ConfigurationDistributor.configurationDeliveryTopic, 
							ConfigurationDistributor.createSelector(cfg.common().clientID()));
			mq.createTopicProducer(ConfigurationDistributor.configurationDeliveryTopic);
			
			// checking configuration. 
			CloudPlatformConfiguration cloudPlatform = cfg.server().cloudPlatform();
			if(!(cloudPlatform instanceof LocalCloudPlatformConfiguration))
			{
				// ensuring that remote hosts will connect to correct message queue, and not "localhost".
				if("localhost".equals(cfg.common().communication().getServerAddress()))
				{
					cfg = cfg.clone();
					String correctServerAddress = PlatformSpecificUtil.findBestIP();
					cfg.common().communication().setServerAddress(correctServerAddress);
					
					JCloudScaleConfiguration.getLogger(ConfigurationDistributorToHosts.class)
					.info(String.format("Detected \"localhost\" MQ address in %s configuration. Replaced MQ address to %s", 
								cfg.server().cloudPlatform().getClass().getName(), correctServerAddress));
				}
			}
			
			// sending configuration and awaiting reply.
			ConfigurationDeliveryResponseObject response = 
					(ConfigurationDeliveryResponseObject)mq
					.requestResponse(new ConfigurationDeliveryObject(cfg), serverId);
			
			if(!response.isConfigurationAccepted)
				throw new JCloudScaleException("Configuration was not accepted by server "+serverId+": "+response.errorMessage);
		}
	}
}
