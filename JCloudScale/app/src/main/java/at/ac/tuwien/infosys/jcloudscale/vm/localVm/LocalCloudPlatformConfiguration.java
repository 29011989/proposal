/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.vm.localVm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.messaging.ActiveMQHelper;
import at.ac.tuwien.infosys.jcloudscale.messaging.MessageQueueConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.CloudPlatformConfiguration;
import at.ac.tuwien.infosys.jcloudscale.vm.IIdManager;
import at.ac.tuwien.infosys.jcloudscale.vm.IVirtualHost;
import at.ac.tuwien.infosys.jcloudscale.vm.VirtualHostPool;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LocalCloudPlatformConfiguration extends CloudPlatformConfiguration 
{
	private static final long serialVersionUID = 1L;
	
	private String serverStartupClass = "at.ac.tuwien.infosys.jcloudscale.server.JCloudScaleServerRunner";
	private String javaPath = "";//empty means use same as current app.
	private String classpath = "";//empty means use same as current app.
	private String startupDirectory = "";//emplty means use as current app.
	private boolean isServerMode = false;
	private long javaHeapSizeMB = 0;//zero is "use default"
	private List<String> customJVMArgs = new ArrayList<>();
	
	public List<String> getCustomJVMArgs() {
		return customJVMArgs;
	}

	public void addCustomJVMArgs(String arg) {
		customJVMArgs.add(arg);
	}

	public void setJavaPath(String javaPath) {
		this.javaPath = javaPath;
	}

	public void setClasspath(String classpath) {
		this.classpath = classpath;
	}

	public void setStartupDirectory(String startupDirectory) {
		this.startupDirectory = startupDirectory;
	}

	public void setServerMode(boolean isServerMode) {
		this.isServerMode = isServerMode;
	}

	public void setJavaHeapSizeMB(long javaHeapSizeMB) {
		this.javaHeapSizeMB = javaHeapSizeMB;
	}
	
	public void setServerStartupClass(String serverStartupClass) {
		this.serverStartupClass = serverStartupClass;
	}

	String getJavaPath() 
	{
		if(javaPath == null || javaPath.length() == 0)
			javaPath = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
		
		return javaPath;
	}

	String getClasspath() 
	{
		if(classpath == null || classpath.length() == 0)
			classpath = generateServerClassPath();
		return classpath.replace(';', File.pathSeparatorChar);//we expect users to specify it ";"-separated, as it allows us to keep it platform-independent.
	}

	String getStartupDirectory() 
	{
		if(startupDirectory == null || startupDirectory.length() == 0)
		        startupDirectory = System.getProperty("java.io.tmpdir");
		return startupDirectory;
	}

	boolean isServerMode() {
		return isServerMode;
	}

	long getJavaHeapSizeMB() {
		return javaHeapSizeMB;
	}
	
	String getServerStartupClass() {
		return serverStartupClass;
	}
	
	//---------------------------------------------------------------------------
	
	/**
	 * Specifies the startup and working directory of the new JVMs that are representing cloud hosts.
	 * @param path The valid path to the existing directory in the local file system.
	 * @return The current instance of <b>LocalCloudPlatformConfiguration</b> to continue configuration.
	 */
	public LocalCloudPlatformConfiguration withStartupDirectory(String path)
	{
		setStartupDirectory(path);
		return this;
	}
	
	/**
	 * Specifies the class path of the new JVM to start with. java-classpath specification rules are used.
	 * Dependencies can be separated by ";" on any platform. Class path specified has to contain all necessary 
	 * dependencies of the application that has to be started. 
	 * @param classpath The ";" separated set of paths that will be provided to java on JVM startup.
	 * @return The current instance of <b>LocalCloudPlatformConfiguration</b> to continue configuration.
	 */
	public LocalCloudPlatformConfiguration withClasspath(String classpath)
	{
		setClasspath(classpath);
		return this;
	}
	
	/**
	 * Specifies the collection of custom JVM arguments that need to be provided on JVM startup.
	 * @param parameters The collection of separate parameters passed to JVM on startup. 
	 * @return The current instance of <b>LocalCloudPlatformConfiguration</b> to continue configuration.
	 */
	public LocalCloudPlatformConfiguration withCustomJVMArgs(String... parameters)
	{
		if(parameters != null)
			for(String param : parameters)
				addCustomJVMArgs(param);
		
		return this;
	}
	
	/**
	 * Specifies the path to the java executable that can be used to start JVM.
	 * @param javaPath The absolute path to the java executable to start new JVM.
	 * @return The current instance of <b>LocalCloudPlatformConfiguration</b> to continue configuration.
	 */
	public LocalCloudPlatformConfiguration withJavaPath(String javaPath)
	{
		setJavaPath(javaPath);
		return this;
	}
	
	/**
	 * Specifies whether the JVM should start in server mode.
	 * @param isServerMode <b>true</b> if server mode should be enabled. Otherwise, <b>false</b>.
	 * @return The current instance of <b>LocalCloudPlatformConfiguration</b> to continue configuration.
	 */
	public LocalCloudPlatformConfiguration withServerMode(boolean isServerMode)
	{
		setServerMode(isServerMode);
		return this;
	}
	
	/**
	 * Specifies the amount of maximal java heap size to allocate for new JVM.
	 * @param javaHeapSizeMB The positive <b>long</b> that specifies the amount of megabytes to limit java heap
	 * size of new JVM. 
	 * @return The current instance of <b>LocalCloudPlatformConfiguration</b> to continue configuration.
	 */
	public LocalCloudPlatformConfiguration withJavaHeapSizeInMB(long javaHeapSizeMB)
	{
		setJavaHeapSizeMB(javaHeapSizeMB);
		return this;
	}
	
	/**
	 * Specifies the entry point of the new JVM.
	 * @param serverStartupClass The full name of the class that contains main method and will be used to start application.
	 * @return The current instance of <b>LocalCloudPlatformConfiguration</b> to continue configuration.
	 */
	public LocalCloudPlatformConfiguration withServerStartupClass(String serverStartupClass)
	{
		setServerStartupClass(serverStartupClass);
		return this;
	}
	
	//---------------------------------------------------------------------------
	
	@Override
	public IVirtualHost getVirtualHost(IIdManager idManager) 
	{
		boolean startPerformanceMonitoring = JCloudScaleConfiguration.getConfiguration()
										.common().monitoring().isEnabled();
		
		return new LocalVM(this, idManager, startPerformanceMonitoring);
	}

	@Override
	public VirtualHostPool getVirtualHostPool() {
		return new VirtualHostPool(this, getMessageQueueConfiguration());
	}
	
	@Override
	protected AutoCloseable startMessageQueueServer(MessageQueueConfiguration communicationConfiguration) throws Exception 
	{
		ActiveMQHelper mqServer = new ActiveMQHelper(communicationConfiguration);
		mqServer.start();
		return mqServer;
	}

	private String generateServerClassPath() {

		Logger log = JCloudScaleConfiguration.getLogger(this);
		String appClassPath = System.getProperty("java.class.path");
		String resultClassPath = null;

		try {
			if (appClassPath != null && appClassPath.length() > 0) {
				String[] appClassPathElements = appClassPath.split(File.pathSeparator);

				String jcsJar = findJCloudScaleJar(appClassPathElements);
				if (jcsJar == null)
					log.warning("Failed to find JCloudScale jar on classpath.");
				else {
					resultClassPath = collectClassPath(log, jcsJar, appClassPathElements);
					if (resultClassPath != null)
						log.fine("Collected Successfully JCloudScale Classpath for " + jcsJar);
				}
			}
		} catch (Exception ex) {
			log.warning("Failed to collect JCloudScale-only classpath for local vm startup: " + ex);
		}

		if (resultClassPath == null) {
			log.warning("JCloudScale failed to coin out necessary libraries for server to start. "
					+ "Thus, complete application classpath will be used for local testing.");
			log.warning("For thorough testing, consider changing local platform configuration to specify server classpath manually.");
			resultClassPath = appClassPath;
		}

		return resultClassPath;
	}

	private String findJCloudScaleJar(String[] appClassPath) {

		for (String elem : appClassPath)
			if (elem.contains("jcloudscale.core-"))
				return elem;

		return null;
	}

	private String collectClassPath(Logger log, String jcsJar, String[] appClassPath) {

		final String CLASS_PATH_KEY = "Class-Path";
		final String CLASS_PATH_SEPARATOR = " ";

		Map<String, String> nameToPathMap = new HashMap<>();
		Map<String, List<String>> nameToVersionsMap = new HashMap<>();
		for (String path : appClassPath) {
			File file = new File(path);
			if (file.isDirectory() || !file.exists())
				continue;

			String filename = file.getName();
			nameToPathMap.put(filename, file.getAbsolutePath());

			// coining out library name
			String libNameNoVersion = getLibWithoutVersion(filename);

			if (nameToVersionsMap.containsKey(libNameNoVersion))
				nameToVersionsMap.get(libNameNoVersion).add(file.getAbsolutePath());
			else {
				List<String> elems = new ArrayList<>();
				elems.add(file.getAbsolutePath());
				nameToVersionsMap.put(libNameNoVersion, elems);
			}
		}

		StringBuilder result = new StringBuilder();
		result.append(new File(jcsJar).getAbsolutePath());

		try (JarFile jar = new JarFile(jcsJar)) {
			Manifest mf = jar.getManifest();
			if (mf.getMainAttributes().getValue(CLASS_PATH_KEY) == null) {
				log.warning("\"" + CLASS_PATH_KEY + "\" property is missing from \"" + jcsJar + 
						"\" manifest file.");
				return null;
			}

			for (String entry : mf.getMainAttributes().getValue(CLASS_PATH_KEY).split(CLASS_PATH_SEPARATOR)) {
				String libPath = null;

				// trying to find exact match.
				if (nameToPathMap.containsKey(entry))
					libPath = nameToPathMap.get(entry);
				else {// trying to find name-only match.
					List<String> libs = nameToVersionsMap.get(getLibWithoutVersion(entry));
					if (libs != null && libs.size() > 0) {
						log.warning("Failed to find exact classpath match for \""
								+ entry	+ "\". Using \""+ libs.get(0)
								+ "\" instead. If your application indeed needs later version of library, "
								+ "APPLICATION MAY BEHAVE DIFFERENTLY in real cloud environment.");
						libPath = libs.get(0);
					}
				}

				if (libPath == null) {
					log.warning("Failed to find classpath entry \"" + entry + "\" on application classpath. "
							+ "You may have issues starting virtual machines in local testing setup.");
					return null;
				}

				result.append(File.pathSeparator).append(libPath);
			}
		} catch (IOException e) {
			log.warning("Failed to collect classpath elements from " + jcsJar+": "+e);
			return null;
		}

		return result.toString();
	}

	private final static Pattern splitLibVersionPattern = Pattern.compile("(.+)\\-([0-9].*)");

	private String getLibWithoutVersion(String filename) {

		Matcher match = splitLibVersionPattern.matcher(filename);
		if (!match.matches())
			return filename;
		else
			return match.group(1);
	}
}
