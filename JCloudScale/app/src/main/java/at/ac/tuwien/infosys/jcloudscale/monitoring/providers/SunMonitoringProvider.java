package at.ac.tuwien.infosys.jcloudscale.monitoring.providers;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.CPUEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.RAMEvent;

@SuppressWarnings("restriction")
public class SunMonitoringProvider extends FallbackMonitoringProvider {

    private com.sun.management.OperatingSystemMXBean sunBean;

    public SunMonitoringProvider(UUID hostId) {
        super(hostId);

        if (!(mxBean instanceof com.sun.management.OperatingSystemMXBean))
            throw new RuntimeException(
                    "Failed to construct SunMonitoringProvider: OperatingSystemMXBean is not a com.sun.management.OperatingSystemMXBean");

        this.sunBean = (com.sun.management.OperatingSystemMXBean) mxBean;

        // we need to perform the first call to getProcessCpuLoad because first
        // call is always -1.
        // (http://pic.dhe.ibm.com/infocenter/java7sdk/v7r0/topic/com.ibm.java.api.70.doc/com.ibm.lang.management/com/ibm/lang/management/OperatingSystemMXBean.html)
        this.sunBean.getProcessCpuLoad();
        this.sunBean.getSystemCpuLoad();
    }

    @Override
    public CPUEvent measureCpuEvent() {
        CPUEvent event = super.measureCpuEvent();

        // value between 0 and 1.0, or a negative value in the case of an error;
        // -1.0 for first call.
        event.setProcessCpuLoad(this.sunBean.getProcessCpuLoad() * this.runtime.availableProcessors());

        // process cpu time in 100 ns units.
        long processCpuTimeMs = TimeUnit.NANOSECONDS.toMillis(this.sunBean.getProcessCpuTime());
        event.setProcessCpuTime(processCpuTimeMs);

        // value between 0 and 1.0, or a negative value in the case of an error;
        // -1.0 for first call.
        event.setSystemCpuLoad(this.sunBean.getSystemCpuLoad());

        return event;
    }

    @Override
    public RAMEvent measureRamEvent() {
        RAMEvent event = super.measureRamEvent();

        // this is just process committed virtual memory: the amount of virtual
        // memory that is guaranteed to be available to the running process.
        // event.setSystemCommitted(sunBean.getCommittedVirtualMemorySize());
        
        event.setSystemFree(sunBean.getFreePhysicalMemorySize());
        event.setSystemTotalRam(sunBean.getTotalPhysicalMemorySize());
        event.setSystemUsed(sunBean.getTotalPhysicalMemorySize() - event.getSystemFree());

        event.setSystemFreeSwap(sunBean.getFreeSwapSpaceSize());
        event.setSystemUsedSwap(sunBean.getTotalSwapSpaceSize() - sunBean.getFreeSwapSpaceSize());
        event.setSystemMaxSwap(sunBean.getTotalSwapSpaceSize());

        return event;
    }

    @Override
    public void close() {
        sunBean = null;
        super.close();
    }

}
