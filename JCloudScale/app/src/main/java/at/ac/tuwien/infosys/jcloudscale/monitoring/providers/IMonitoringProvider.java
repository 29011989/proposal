package at.ac.tuwien.infosys.jcloudscale.monitoring.providers;

import java.io.Closeable;

import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.CPUEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.NetworkEvent;
import at.ac.tuwien.infosys.jcloudscale.messaging.objects.monitoring.RAMEvent;

public interface IMonitoringProvider extends Closeable {
    
    public CPUEvent measureCpuEvent();
    
    public RAMEvent measureRamEvent();
    
    public NetworkEvent measureNetworkEvent();
    
    @Override
    public void close();
}
