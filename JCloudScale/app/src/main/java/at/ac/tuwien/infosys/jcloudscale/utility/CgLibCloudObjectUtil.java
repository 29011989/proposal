/*
   Copyright 2013 Philipp Leitner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package at.ac.tuwien.infosys.jcloudscale.utility;

import java.lang.reflect.Method;
import java.util.UUID;

import at.ac.tuwien.infosys.jcloudscale.annotations.DestructCloudObject;
import at.ac.tuwien.infosys.jcloudscale.api.CloudObjects;
import at.ac.tuwien.infosys.jcloudscale.aspects.CloudObjectAspect;
import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;
import at.ac.tuwien.infosys.jcloudscale.exception.JCloudScaleException;
import at.ac.tuwien.infosys.jcloudscale.management.CloudManager;
import at.ac.tuwien.infosys.jcloudscale.management.references.JCloudScaleReferenceManager;
import net.sf.cglib.core.CodeGenerationException;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class CgLibCloudObjectUtil {

	public static Object replaceCOWithProxy(Class<?> className, Object[] parameters, Class<?>[] parameterTypes) {
		
		Enhancer enhancer = new Enhancer();
		enhancer.setCallback(new JCloudScaleCloudObjectHandler());
		enhancer.setSuperclass(className);
		
		try {
			if(parameters != null && parameterTypes.length > 0)
				return enhancer.create(parameterTypes, parameters);
			else
				return enhancer.create();
		} catch(CodeGenerationException e) {
			// we are not particularly interested in the CodeGenerationException, more in what caused it
			if(e.getCause() != null)
				ExceptionHelper.throwException(e.getCause());
			else
				ExceptionHelper.throwException(e);
			return null;// we won't reach here, just that JRE doesn't know that previous statement always throws.
		}
	}

	private static class JCloudScaleCloudObjectHandler implements MethodInterceptor {

		@Override
		public Object intercept(Object o, Method method, Object[] params, MethodProxy proxy) throws Throwable {
			
			if(!CgLibUtil.isOverwriteableMethod(method))
				return proxy.invokeSuper(o, params);
			
			Object result = null;

			UUID id = CloudObjects.getId(o);
			
			if(id == null)
			{
				JCloudScaleConfiguration.getLogger(CgLibUtil.class)
						.warning("Invocation of the method "+method.getName()+" on the not-deployed or unknown object! " +
									"Invocation will be performed locally and side effects will not be transferred to the server!");
				
				return proxy.invokeSuper(o, params);
			}
			
			if (CloudObjects.isDestroyed(id))
				throw new JCloudScaleException("Method invocation on already destroyed CloudObject!");
			
			Object[] processedParams = JCloudScaleReferenceManager.getInstance().processArguments(method, params);
			
			result = CloudManager.getInstance().invokeCloudObject(id, method,
					processedParams, method.getParameterTypes());
			
			// TODO: this should better be implemented as a pointcut, but this is easier for now :D 
			if(ReflectionUtil.isAnnotationPresent(method, DestructCloudObject.class)) {
				new CloudObjectAspect().destructCloudObject(o);
			}
			
			return result;
		}
	}
}
