package at.ac.tuwien.infosys.jcloudscale.monitoring.providers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.jcloudscale.configuration.JCloudScaleConfiguration;

public class MonitoringProviderFactory {
    
    static final int UNDEFINED_INTEGRAL = -1;
    static final String UNDEFINED_STRING = "N/A";
    static final double UNDEFINED_DOUBLE = Double.NaN;
    
    private MonitoringProviderFactory() {
    }

    public static IMonitoringProvider selectMonitoringProvider(UUID hostId) {
        Logger log = JCloudScaleConfiguration.getLogger(MonitoringProviderFactory.class);

        try {
            Class<?>[] monitoringProviders = new Class<?>[] { SigarMonitoringProvider.class,
                    SunMonitoringProvider.class, FallbackMonitoringProvider.class };

            for (Class<?> monitoringProviderClass : monitoringProviders) {
                try {
                    IMonitoringProvider monitoringProvider = createMonitoringProvider(monitoringProviderClass, hostId);
                    log.fine("Monitoring Provider " + monitoringProvider + "was successfully constructed.");
                    return monitoringProvider;
                } catch (Exception ex) {
                    log.warning(String.format("Failed to construct %s : %s", monitoringProviderClass, ex));
                }
            }

            log.warning("Failed to construct all of configured monitoring providers.");
            return null;

        } catch (Exception ex) {
            log.severe("Failed to construct MonitoringProvider. Monitoring data will not be collected." + ex);
            ex.printStackTrace();
            return null;
        }
    }

    private static IMonitoringProvider createMonitoringProvider(Class<?> monitoringProviderClass, UUID hostId) {
        if (!IMonitoringProvider.class.isAssignableFrom(monitoringProviderClass))
            throw new RuntimeException("Class " + monitoringProviderClass
                    + " does not implement IMonitoringProvider interface!");

        try {
            @SuppressWarnings("unchecked")
            Constructor<IMonitoringProvider> constructor = (Constructor<IMonitoringProvider>) monitoringProviderClass
                    .getConstructor(UUID.class);

            if (!constructor.isAccessible())
                constructor.setAccessible(true);

            return constructor.newInstance(hostId);

        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException e) {
            throw new RuntimeException("Failed to instantiate monitoring Provider object", e);
        } catch (InstantiationException | InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause == null)
                throw new RuntimeException("Failed to instantiate monitoring Provider object", e);

            if (cause instanceof RuntimeException)
                throw (RuntimeException) cause;

            throw new RuntimeException(cause.getMessage(), cause);
        }
    }
}
